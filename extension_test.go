package spirv_test

import (
	"reflect"
	"testing"

	"gitlab.com/zephyrtronium/spirv"
)

type (
	// testExtInst is an extension instruction with no operands.
	testExtInst struct{}
	// testExtInstOp is an extension instruction with one operand.
	testExtInstOp struct {
		X spirv.Id
	}
	// testExtInstParm is an extension instruction with an operand which has
	// a parameter.
	testExtInstParm struct {
		X testExtParm
	}
	// testExtParm is the parameter of a testExtInstParm.
	testExtParm interface {
		spirv.ValueEnum
		isTestExtParm()
	}
	// testExtParmOpt is a testExtParm.
	testExtParmOpt struct {
		Y spirv.Id
	}
)

func (op *testExtInst) String() string                             { return "TestOpExtInst" }
func (op *testExtInst) Extension() string                          { return testExtSetName }
func (op *testExtInst) Opcode() spirv.LiteralExtInstInteger        { return 1 }
func (op *testExtInst) Setup(p []uint32) error                     { return nil }
func (op *testExtInst) Operands(p []spirv.Operand) []spirv.Operand { return p }
func (op *testExtInst) Encode(m *spirv.Module, n spirv.Instruction, p []uint32) []uint32 {
	return append(p, 1)
}
func (op *testExtInst) Scan(m *spirv.Module, n spirv.Instruction, p []uint32) (int, error) {
	return 1, nil
}
func (op *testExtInst) Parameters(p []spirv.Operand) []spirv.Operand { return p }

func (op *testExtInstOp) String() string                             { return "TestOpExtInstOp" }
func (op *testExtInstOp) Extension() string                          { return testExtSetName }
func (op *testExtInstOp) Opcode() spirv.LiteralExtInstInteger        { return 2 }
func (op *testExtInstOp) Setup(p []uint32) error                     { return nil }
func (op *testExtInstOp) Operands(p []spirv.Operand) []spirv.Operand { return append(p, &op.X) }
func (op *testExtInstOp) Encode(m *spirv.Module, n spirv.Instruction, p []uint32) []uint32 {
	return append(p, 2)
}
func (op *testExtInstOp) Scan(m *spirv.Module, n spirv.Instruction, p []uint32) (int, error) {
	return 1, nil
}
func (op *testExtInstOp) Parameters(p []spirv.Operand) []spirv.Operand { return append(p, &op.X) }

func (op *testExtInstParm) String() string                      { return "TestOpExtInstParm" }
func (op *testExtInstParm) Extension() string                   { return testExtSetName }
func (op *testExtInstParm) Opcode() spirv.LiteralExtInstInteger { return 3 }
func (op *testExtInstParm) Setup(p []uint32) error {
	if len(p) < 1 {
		return &spirv.ShortInstructionError{Operand: "X"}
	}
	switch p[0] {
	case 1:
		op.X = new(testExtParmOpt)
	default:
		return &spirv.InvalidEnumerantError{Enum: "testExtParmOpt", Value: p[1]}
	}
	return nil
}
func (op *testExtInstParm) Operands(p []spirv.Operand) []spirv.Operand { return append(p, op.X) }
func (op *testExtInstParm) Encode(m *spirv.Module, n spirv.Instruction, p []uint32) []uint32 {
	return append(p, 3)
}
func (op *testExtInstParm) Scan(m *spirv.Module, n spirv.Instruction, p []uint32) (int, error) {
	return 1, nil
}
func (op *testExtInstParm) Parameters(p []spirv.Operand) []spirv.Operand { return append(p, op.X) }

func (op *testExtParmOpt) Encode(m *spirv.Module, n spirv.Instruction, p []uint32) []uint32 {
	return append(p, 1)
}
func (op *testExtParmOpt) Scan(m *spirv.Module, n spirv.Instruction, p []uint32) (int, error) {
	return 1, nil
}
func (op *testExtParmOpt) Parameters(p []spirv.Operand) []spirv.Operand { return append(p, &op.Y) }
func (op *testExtParmOpt) EnumWord() uint32                             { return 1 }
func (op *testExtParmOpt) String() string                               { return "TestExtParmOpt" }
func (op *testExtParmOpt) isTestExtParm()                               {}

func testExtSet(op spirv.LiteralExtInstInteger) spirv.ExtInst {
	switch op {
	case 1:
		return new(testExtInst)
	case 2:
		return new(testExtInstOp)
	case 3:
		return new(testExtInstParm)
	default:
		return nil
	}
}

const testExtSetName = "spirv.zephyrtronium.100"

// To simplify things, just register the set at the start.
func init() {
	spirv.Register(testExtSetName, testExtSet)
}

func TestRegisterDuplicate(t *testing.T) {
	defer func() {
		if err := recover(); err == nil {
			t.Error("duplicate registration allowed")
		}
	}()
	spirv.Register(testExtSetName, testExtSet)
}

func TestEncodeOpExtInst(t *testing.T) {
	cases := []struct {
		name string
		in   spirv.OpExtInst
		out  []uint32
	}{
		{
			"raw-niladic",
			spirv.OpExtInst{
				ResultType:  spirv.TypeRef(1),
				Result:      21,
				Set:         spirv.Ref(3),
				Instruction: &spirv.RawExtInst{Op: 101},
			},
			[]uint32{12 | 5<<16, 1, 21, 3, 101},
		},
		{
			"raw-operands",
			spirv.OpExtInst{
				ResultType:  spirv.TypeRef(1),
				Result:      22,
				Set:         spirv.Ref(3),
				Instruction: &spirv.RawExtInst{Op: 102, Words: []spirv.Id{201, 202}},
			},
			[]uint32{12 | 7<<16, 1, 22, 3, 102, 201, 202},
		},
		{
			"typed-niladic",
			spirv.OpExtInst{
				ResultType:  spirv.TypeRef(1),
				Result:      23,
				Set:         spirv.Ref(2),
				Instruction: &testExtInst{},
			},
			[]uint32{12 | 5<<16, 1, 23, 2, 1},
		},
		{
			"typed-operand",
			spirv.OpExtInst{
				ResultType:  spirv.TypeRef(1),
				Result:      24,
				Set:         spirv.Ref(2),
				Instruction: &testExtInstOp{X: 50},
			},
			[]uint32{12 | 6<<16, 1, 24, 2, 2, 50},
		},
		{
			"typed-param",
			spirv.OpExtInst{
				ResultType:  spirv.TypeRef(1),
				Result:      25,
				Set:         spirv.Ref(2),
				Instruction: &testExtInstParm{X: &testExtParmOpt{Y: 51}},
			},
			[]uint32{12 | 7<<16, 1, 25, 2, 3, 1, 51},
		},
	}
	var m spirv.Module
	insts := []spirv.Instruction{
		&spirv.OpTypeBool{Result: 1},
		&spirv.OpExtInstImport{Result: 2, Name: testExtSetName},
		&spirv.OpExtInstImport{Result: 3, Name: "fake.ext.100"},
	}
	for _, inst := range insts {
		m.Add(inst)
	}

	var scr spirv.Scratch
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			err := scr.Encodele(&m, &c.in)
			if err != nil {
				t.Errorf("couldn't encode %#v: %v", c.in, err)
			}
			if !reflect.DeepEqual(scr.D, c.out) {
				t.Errorf("wrong encoding:\nwant %08x\ngot  %08x", c.out, scr.D)
			}
		})
	}
}

func TestDecodeOpExtInst(t *testing.T) {
	const fakename = "fake.ext.100"
	cases := []struct {
		name string
		in   []uint32
		out  spirv.OpExtInst
		err  error
	}{
		{
			name: "raw-niladic",
			in:   []uint32{12 | 5<<16, 1, 21, 3, 101},
			out: spirv.OpExtInst{
				ResultType:  spirv.TypeRef(1),
				Result:      21,
				Set:         spirv.Ref(3),
				Instruction: &spirv.RawExtInst{ExtName: fakename, Op: 101, Words: make([]spirv.Id, 0)},
			},
		},
		{
			name: "raw-operands",
			in:   []uint32{12 | 7<<16, 1, 22, 3, 102, 201, 202},
			out: spirv.OpExtInst{
				ResultType:  spirv.TypeRef(1),
				Result:      22,
				Set:         spirv.Ref(3),
				Instruction: &spirv.RawExtInst{ExtName: fakename, Op: 102, Words: []spirv.Id{201, 202}},
			},
		},
		{
			name: "typed-niladic",
			in:   []uint32{12 | 5<<16, 1, 23, 2, 1},
			out: spirv.OpExtInst{
				ResultType:  spirv.TypeRef(1),
				Result:      23,
				Set:         spirv.Ref(2),
				Instruction: &testExtInst{},
			},
		},
		{
			name: "typed-operand",
			in:   []uint32{12 | 6<<16, 1, 24, 2, 2, 50},
			out: spirv.OpExtInst{
				ResultType:  spirv.TypeRef(1),
				Result:      24,
				Set:         spirv.Ref(2),
				Instruction: &testExtInstOp{X: 50},
			},
		},
		{
			name: "typed-param",
			in:   []uint32{12 | 7<<16, 1, 25, 2, 3, 1, 51},
			out: spirv.OpExtInst{
				ResultType:  spirv.TypeRef(1),
				Result:      25,
				Set:         spirv.Ref(2),
				Instruction: &testExtInstParm{X: &testExtParmOpt{Y: 51}},
			},
		},
		{
			name: "short",
			in:   []uint32{12 | 3<<16, 1, 26},
			err: &spirv.ShortInstructionError{
				Inst:    new(spirv.OpExtInst),
				Operand: "Set",
			},
		},
		{
			name: "no-ext",
			in:   []uint32{12 | 5<<16, 1, 27, 4, 1},
			err: &spirv.NoExtensionError{
				Inst: new(spirv.OpExtInst),
				Set:  spirv.Ref(4),
			},
		},
		{
			name: "no-extinst",
			in:   []uint32{12 | 5<<16, 1, 28, 2, 1024},
			err: &spirv.NoExtensionInstructionError{
				Inst:   new(spirv.OpExtInst),
				Set:    testExtSetName,
				Opcode: 1024,
			},
		},
	}
	var m spirv.Module
	insts := []spirv.Instruction{
		&spirv.OpTypeBool{Result: 1},
		&spirv.OpExtInstImport{Result: 2, Name: testExtSetName},
		&spirv.OpExtInstImport{Result: 3, Name: fakename},
	}
	for _, inst := range insts {
		m.Add(inst)
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			var scr spirv.Scratch
			var inst spirv.OpExtInst
			err := scr.ScanExtInst(&m, &inst, c.in)
			if !eqerrs(t, err, c.err) {
				t.Errorf("wrong error:\nwant %#v\ngot  %#v", c.err, err)
			}
			if err == nil && !reflect.DeepEqual(c.out, inst) {
				t.Errorf("wrong decode result:\nwant %#v\ngot  %#v", c.out, inst)
			}
		})
	}
}
