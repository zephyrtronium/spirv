package amdshadertrinaryminmaxop

// Code generated by spirv/generate; DO NOT EDIT

const (
	OpFMin3AMD = 1
	OpUMin3AMD = 2
	OpSMin3AMD = 3
	OpFMax3AMD = 4
	OpUMax3AMD = 5
	OpSMax3AMD = 6
	OpFMid3AMD = 7
	OpUMid3AMD = 8
	OpSMid3AMD = 9
)
