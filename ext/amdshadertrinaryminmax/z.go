package amdshadertrinaryminmax

// Code generated by spirv/generate; DO NOT EDIT

import "gitlab.com/zephyrtronium/spirv"

// OpFMin3AMD is an extension instruction.
//
// Requires extensions: [SPV_AMD_shader_trinary_minmax]
type OpFMin3AMD struct {
	X spirv.IdRef
	Y spirv.IdRef
	Z spirv.IdRef
}

var _ spirv.ExtInst = (*OpFMin3AMD)(nil)

func (*OpFMin3AMD) String() string {
	return "FMin3AMD"
}

func (*OpFMin3AMD) Extension() string {
	return "spv-amd-shader-trinary-minmax"
}

func (*OpFMin3AMD) Opcode() spirv.LiteralExtInstInteger {
	return 1
}

func (op *OpFMin3AMD) Setup(p []uint32) error {
	k := 0
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "X",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Y",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Z",
		}
	}
	k++
	return nil
}

func (op *OpFMin3AMD) Encode(m *spirv.Module, n spirv.Instruction, p []uint32) []uint32 {
	return append(p, 1)
}

func (op *OpFMin3AMD) Scan(m *spirv.Module, n spirv.Instruction, p []uint32) (int, error) {
	return 0, nil
}

func (op *OpFMin3AMD) Parameters(p []spirv.Operand) []spirv.Operand {
	p = append(p, &op.X)
	p = append(p, &op.Y)
	p = append(p, &op.Z)
	return p
}

// OpUMin3AMD is an extension instruction.
//
// Requires extensions: [SPV_AMD_shader_trinary_minmax]
type OpUMin3AMD struct {
	X spirv.IdRef
	Y spirv.IdRef
	Z spirv.IdRef
}

var _ spirv.ExtInst = (*OpUMin3AMD)(nil)

func (*OpUMin3AMD) String() string {
	return "UMin3AMD"
}

func (*OpUMin3AMD) Extension() string {
	return "spv-amd-shader-trinary-minmax"
}

func (*OpUMin3AMD) Opcode() spirv.LiteralExtInstInteger {
	return 2
}

func (op *OpUMin3AMD) Setup(p []uint32) error {
	k := 0
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "X",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Y",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Z",
		}
	}
	k++
	return nil
}

func (op *OpUMin3AMD) Encode(m *spirv.Module, n spirv.Instruction, p []uint32) []uint32 {
	return append(p, 2)
}

func (op *OpUMin3AMD) Scan(m *spirv.Module, n spirv.Instruction, p []uint32) (int, error) {
	return 0, nil
}

func (op *OpUMin3AMD) Parameters(p []spirv.Operand) []spirv.Operand {
	p = append(p, &op.X)
	p = append(p, &op.Y)
	p = append(p, &op.Z)
	return p
}

// OpSMin3AMD is an extension instruction.
//
// Requires extensions: [SPV_AMD_shader_trinary_minmax]
type OpSMin3AMD struct {
	X spirv.IdRef
	Y spirv.IdRef
	Z spirv.IdRef
}

var _ spirv.ExtInst = (*OpSMin3AMD)(nil)

func (*OpSMin3AMD) String() string {
	return "SMin3AMD"
}

func (*OpSMin3AMD) Extension() string {
	return "spv-amd-shader-trinary-minmax"
}

func (*OpSMin3AMD) Opcode() spirv.LiteralExtInstInteger {
	return 3
}

func (op *OpSMin3AMD) Setup(p []uint32) error {
	k := 0
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "X",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Y",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Z",
		}
	}
	k++
	return nil
}

func (op *OpSMin3AMD) Encode(m *spirv.Module, n spirv.Instruction, p []uint32) []uint32 {
	return append(p, 3)
}

func (op *OpSMin3AMD) Scan(m *spirv.Module, n spirv.Instruction, p []uint32) (int, error) {
	return 0, nil
}

func (op *OpSMin3AMD) Parameters(p []spirv.Operand) []spirv.Operand {
	p = append(p, &op.X)
	p = append(p, &op.Y)
	p = append(p, &op.Z)
	return p
}

// OpFMax3AMD is an extension instruction.
//
// Requires extensions: [SPV_AMD_shader_trinary_minmax]
type OpFMax3AMD struct {
	X spirv.IdRef
	Y spirv.IdRef
	Z spirv.IdRef
}

var _ spirv.ExtInst = (*OpFMax3AMD)(nil)

func (*OpFMax3AMD) String() string {
	return "FMax3AMD"
}

func (*OpFMax3AMD) Extension() string {
	return "spv-amd-shader-trinary-minmax"
}

func (*OpFMax3AMD) Opcode() spirv.LiteralExtInstInteger {
	return 4
}

func (op *OpFMax3AMD) Setup(p []uint32) error {
	k := 0
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "X",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Y",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Z",
		}
	}
	k++
	return nil
}

func (op *OpFMax3AMD) Encode(m *spirv.Module, n spirv.Instruction, p []uint32) []uint32 {
	return append(p, 4)
}

func (op *OpFMax3AMD) Scan(m *spirv.Module, n spirv.Instruction, p []uint32) (int, error) {
	return 0, nil
}

func (op *OpFMax3AMD) Parameters(p []spirv.Operand) []spirv.Operand {
	p = append(p, &op.X)
	p = append(p, &op.Y)
	p = append(p, &op.Z)
	return p
}

// OpUMax3AMD is an extension instruction.
//
// Requires extensions: [SPV_AMD_shader_trinary_minmax]
type OpUMax3AMD struct {
	X spirv.IdRef
	Y spirv.IdRef
	Z spirv.IdRef
}

var _ spirv.ExtInst = (*OpUMax3AMD)(nil)

func (*OpUMax3AMD) String() string {
	return "UMax3AMD"
}

func (*OpUMax3AMD) Extension() string {
	return "spv-amd-shader-trinary-minmax"
}

func (*OpUMax3AMD) Opcode() spirv.LiteralExtInstInteger {
	return 5
}

func (op *OpUMax3AMD) Setup(p []uint32) error {
	k := 0
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "X",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Y",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Z",
		}
	}
	k++
	return nil
}

func (op *OpUMax3AMD) Encode(m *spirv.Module, n spirv.Instruction, p []uint32) []uint32 {
	return append(p, 5)
}

func (op *OpUMax3AMD) Scan(m *spirv.Module, n spirv.Instruction, p []uint32) (int, error) {
	return 0, nil
}

func (op *OpUMax3AMD) Parameters(p []spirv.Operand) []spirv.Operand {
	p = append(p, &op.X)
	p = append(p, &op.Y)
	p = append(p, &op.Z)
	return p
}

// OpSMax3AMD is an extension instruction.
//
// Requires extensions: [SPV_AMD_shader_trinary_minmax]
type OpSMax3AMD struct {
	X spirv.IdRef
	Y spirv.IdRef
	Z spirv.IdRef
}

var _ spirv.ExtInst = (*OpSMax3AMD)(nil)

func (*OpSMax3AMD) String() string {
	return "SMax3AMD"
}

func (*OpSMax3AMD) Extension() string {
	return "spv-amd-shader-trinary-minmax"
}

func (*OpSMax3AMD) Opcode() spirv.LiteralExtInstInteger {
	return 6
}

func (op *OpSMax3AMD) Setup(p []uint32) error {
	k := 0
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "X",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Y",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Z",
		}
	}
	k++
	return nil
}

func (op *OpSMax3AMD) Encode(m *spirv.Module, n spirv.Instruction, p []uint32) []uint32 {
	return append(p, 6)
}

func (op *OpSMax3AMD) Scan(m *spirv.Module, n spirv.Instruction, p []uint32) (int, error) {
	return 0, nil
}

func (op *OpSMax3AMD) Parameters(p []spirv.Operand) []spirv.Operand {
	p = append(p, &op.X)
	p = append(p, &op.Y)
	p = append(p, &op.Z)
	return p
}

// OpFMid3AMD is an extension instruction.
//
// Requires extensions: [SPV_AMD_shader_trinary_minmax]
type OpFMid3AMD struct {
	X spirv.IdRef
	Y spirv.IdRef
	Z spirv.IdRef
}

var _ spirv.ExtInst = (*OpFMid3AMD)(nil)

func (*OpFMid3AMD) String() string {
	return "FMid3AMD"
}

func (*OpFMid3AMD) Extension() string {
	return "spv-amd-shader-trinary-minmax"
}

func (*OpFMid3AMD) Opcode() spirv.LiteralExtInstInteger {
	return 7
}

func (op *OpFMid3AMD) Setup(p []uint32) error {
	k := 0
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "X",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Y",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Z",
		}
	}
	k++
	return nil
}

func (op *OpFMid3AMD) Encode(m *spirv.Module, n spirv.Instruction, p []uint32) []uint32 {
	return append(p, 7)
}

func (op *OpFMid3AMD) Scan(m *spirv.Module, n spirv.Instruction, p []uint32) (int, error) {
	return 0, nil
}

func (op *OpFMid3AMD) Parameters(p []spirv.Operand) []spirv.Operand {
	p = append(p, &op.X)
	p = append(p, &op.Y)
	p = append(p, &op.Z)
	return p
}

// OpUMid3AMD is an extension instruction.
//
// Requires extensions: [SPV_AMD_shader_trinary_minmax]
type OpUMid3AMD struct {
	X spirv.IdRef
	Y spirv.IdRef
	Z spirv.IdRef
}

var _ spirv.ExtInst = (*OpUMid3AMD)(nil)

func (*OpUMid3AMD) String() string {
	return "UMid3AMD"
}

func (*OpUMid3AMD) Extension() string {
	return "spv-amd-shader-trinary-minmax"
}

func (*OpUMid3AMD) Opcode() spirv.LiteralExtInstInteger {
	return 8
}

func (op *OpUMid3AMD) Setup(p []uint32) error {
	k := 0
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "X",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Y",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Z",
		}
	}
	k++
	return nil
}

func (op *OpUMid3AMD) Encode(m *spirv.Module, n spirv.Instruction, p []uint32) []uint32 {
	return append(p, 8)
}

func (op *OpUMid3AMD) Scan(m *spirv.Module, n spirv.Instruction, p []uint32) (int, error) {
	return 0, nil
}

func (op *OpUMid3AMD) Parameters(p []spirv.Operand) []spirv.Operand {
	p = append(p, &op.X)
	p = append(p, &op.Y)
	p = append(p, &op.Z)
	return p
}

// OpSMid3AMD is an extension instruction.
//
// Requires extensions: [SPV_AMD_shader_trinary_minmax]
type OpSMid3AMD struct {
	X spirv.IdRef
	Y spirv.IdRef
	Z spirv.IdRef
}

var _ spirv.ExtInst = (*OpSMid3AMD)(nil)

func (*OpSMid3AMD) String() string {
	return "SMid3AMD"
}

func (*OpSMid3AMD) Extension() string {
	return "spv-amd-shader-trinary-minmax"
}

func (*OpSMid3AMD) Opcode() spirv.LiteralExtInstInteger {
	return 9
}

func (op *OpSMid3AMD) Setup(p []uint32) error {
	k := 0
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "X",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Y",
		}
	}
	k++
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			Operand: "Z",
		}
	}
	k++
	return nil
}

func (op *OpSMid3AMD) Encode(m *spirv.Module, n spirv.Instruction, p []uint32) []uint32 {
	return append(p, 9)
}

func (op *OpSMid3AMD) Scan(m *spirv.Module, n spirv.Instruction, p []uint32) (int, error) {
	return 0, nil
}

func (op *OpSMid3AMD) Parameters(p []spirv.Operand) []spirv.Operand {
	p = append(p, &op.X)
	p = append(p, &op.Y)
	p = append(p, &op.Z)
	return p
}
