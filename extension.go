package spirv

import (
	"strconv"
	"strings"
)

// ExtInst is an extension instruction.
type ExtInst interface {
	// String returns the name of the instruction.
	String() string
	// Extension returns the name of the extension instruction set to which
	// the instruction belongs.
	Extension() string
	// Opcode returns the opcode of the instruction within the extension
	// instruction set.
	Opcode() LiteralExtInstInteger
	// Setup initializes the instruction's enumeration operands according to
	// the values in p, the words constituting the operands of the OpExtInst
	// containing the extension instruction. A subsequent call to Operands
	// allows each operand to be scanned separately.
	Setup(p []uint32) error
	// An ExtInst is always an operand to OpExtInst. The Parameters method
	// appends the extension instruction's operands to its argument.
	Operand
}

// ExtensionSet is a function which creates blank extension instructions for
// all instructions in an extension instruction set. The returned instruction
// is set up by the module decoder using its Setup and Operands methods and so
// does not need to be returned in any particular state.
//
// An ExtensionSet should return nil if and only if called with an opcode that
// is not in the extension instruction set.
type ExtensionSet func(op LiteralExtInstInteger) ExtInst

var extensions = map[string]ExtensionSet{}

// Register registers an extension instruction factory to implement an
// extension instruction set for the module decoder. Panics if a set has
// already been registered with the same name.
func Register(name string, set ExtensionSet) {
	name = strings.ToLower(name)
	if extensions[name] != nil {
		panic("spirv: multiple registrations of " + name)
	}
	extensions[name] = set
}

// RawExtInst holds the operands of an OpExtInst without interpretation.
//
// The module decoder uses RawExtInst as the Instruction for an OpExtInst which
// refers to an extension instruction set which has been imported in the module
// but which has not been registered for the module decoder.
//
// A RawExtInst can be included as the ExtInst operand of an OpExtInst to
// encode arbitrary operands to the instruction.
type RawExtInst struct {
	// ExtName is the name of the extension to which this instruction refers.
	// The decoder includes this for debugging purposes; it is not used for
	// encoding.
	ExtName string
	// Op is the extension instruction opcode.
	Op LiteralExtInstInteger
	// Words are the raw operands of the extension instruction.
	Words []Id
}

var _ ExtInst = (*RawExtInst)(nil)

func (op *RawExtInst) String() string {
	return ""
}

func (op *RawExtInst) Extension() string {
	return op.ExtName
}

func (op *RawExtInst) Opcode() LiteralExtInstInteger {
	return op.Op
}

func (op *RawExtInst) Setup(p []uint32) error {
	op.Words = make([]Id, len(p))
	return nil
}

func (op *RawExtInst) Encode(m *Module, n Instruction, p []uint32) []uint32 {
	return p
}

func (op *RawExtInst) Scan(m *Module, n Instruction, p []uint32) (int, error) {
	return 0, nil
}

func (op *RawExtInst) Parameters(p []Operand) []Operand {
	p = append(p, &op.Op)
	for i := range op.Words {
		p = append(p, &op.Words[i])
	}
	return p
}

// OpExtInst is in class Extension.
//
// OpExtInst is the mechanism implementing extension instructions. The module
// decoder has a special case to decode it in connection with registered
// extension instruction sets.
type OpExtInst struct {
	ResultType  IdResultType
	Result      Id
	Set         IdRef
	Instruction ExtInst
}

var _ Instruction = (*OpExtInst)(nil)

func (op *OpExtInst) String() string {
	return "OpExtInst"
}

func (op *OpExtInst) Opcode() uint32 {
	return 12
}

func (op *OpExtInst) ResultID() IdRef {
	return op.Result.Ref()
}

// Setup sets up the instruction with a RawExtInst as the Instruction.
//
// The module decoder implements a special case for OpExtInst in order to
// connect extension instruction sets.
func (op *OpExtInst) Setup(p []uint32) error {
	switch len(p) {
	case 0, 1, 2, 3:
		return &ShortInstructionError{Inst: op, Operand: "Set"}
	case 4:
		return &ShortInstructionError{Inst: op, Operand: "Instruction"}
	}
	op.Instruction = new(RawExtInst)
	return nil
}

func (op *OpExtInst) Operands(p []Operand) []Operand {
	p = append(p, &op.ResultType, &op.Result, &op.Set, op.Instruction)
	return p
}

// NoExtensionError is an error returned when the module decoder encounters an
// OpExtInst with a Set referencing an <id> which does not correspond to the
// result of an OpExtInstImport.
type NoExtensionError struct {
	// InstPos and WordPos are the number of instructions and words,
	// respectively, preceding the instruction.
	InstPos, WordPos int
	// Inst is the OpExtInst with the invalid reference.
	Inst *OpExtInst
	// Set is the invalid IdRef.
	Set IdRef
}

func (err *NoExtensionError) Error() string {
	i := strconv.Itoa(err.InstPos)
	w := strconv.Itoa(err.WordPos)
	return "inst " + i + " at word " + w + ": " + "no OpExtInstImport with result <id> " + strconv.FormatUint(uint64(err.Set.id), 10)
}

func (err *NoExtensionError) Position() (insts, words int) {
	return err.InstPos, err.WordPos
}

func (err *NoExtensionError) setPosition(insts, words int) {
	err.InstPos, err.WordPos = insts, words
}

var _ Error = (*NoExtensionError)(nil)

// NoExtensionInstructionError is an error returned when the module decoder
// encounters an OpExtInst with an opcode that the registered extension
// instruction set does not recognize.
type NoExtensionInstructionError struct {
	// InstPos and WordPos are the number of instructions and words,
	// respectively, preceding the instruction.
	InstPos, WordPos int
	// Inst is the OpExtInst with the unrecognized opcode.
	Inst *OpExtInst
	// Set is the name of the extension instruction set which did not recognize
	// the opcode.
	Set string
	// Opcode is the unrecognized opcode.
	Opcode LiteralExtInstInteger
}

func (err *NoExtensionInstructionError) Error() string {
	i := strconv.Itoa(err.InstPos)
	w := strconv.Itoa(err.WordPos)
	return "inst " + i + " at word " + w + ": " + "no opcode " + strconv.FormatUint(uint64(err.Opcode), 10) + " in " + err.Set
}

func (err *NoExtensionInstructionError) Position() (insts, words int) {
	return err.InstPos, err.WordPos
}

func (err *NoExtensionInstructionError) setPosition(insts, words int) {
	err.InstPos, err.WordPos = insts, words
}

var _ Error = (*NoExtensionInstructionError)(nil)
