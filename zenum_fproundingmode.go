// Copyright (c) 2014-2020 The Khronos Group Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and/or associated documentation files (the "Materials"),
// to deal in the Materials without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Materials, and to permit persons to whom the
// Materials are furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Materials.
//
// MODIFICATIONS TO THIS FILE MAY MEAN IT NO LONGER ACCURATELY REFLECTS KHRONOS
// STANDARDS. THE UNMODIFIED, NORMATIVE VERSIONS OF KHRONOS SPECIFICATIONS AND
// HEADER INFORMATION ARE LOCATED AT https://www.khronos.org/registry/
//
// THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM,OUT OF OR IN CONNECTION WITH THE MATERIALS OR THE USE OR OTHER DEALINGS
// IN THE MATERIALS.

package spirv

// Code generated by spirv/generate; DO NOT EDIT

// FPRoundingMode is a ValueEnum.
type FPRoundingMode interface {
	ValueEnum
	isFPRoundingMode()
}

// FPRoundingModeEnumerant creates a blank FPRoundingMode containing the enumerant
// corresponding to val. If there is no such enumerant, the result is nil.
func FPRoundingModeEnumerant(val uint32) FPRoundingMode {
	switch val {
	case 0:
		return FPRoundingModeRTE
	case 1:
		return FPRoundingModeRTZ
	case 2:
		return FPRoundingModeRTP
	case 3:
		return FPRoundingModeRTN
	default:
		return nil
	}
}

type _FPRoundingModeRTE struct{}

func (_FPRoundingModeRTE) Encode(m *Module, n Instruction, p []uint32) []uint32   { return append(p, 0) }
func (_FPRoundingModeRTE) Scan(m *Module, n Instruction, p []uint32) (int, error) { return 1, nil }
func (_FPRoundingModeRTE) Parameters(p []Operand) []Operand                       { return p }
func (_FPRoundingModeRTE) EnumWord() uint32                                       { return 0 }
func (_FPRoundingModeRTE) String() string                                         { return "RTE" }
func (_FPRoundingModeRTE) isFPRoundingMode()                                      {}

// FPRoundingModeRTE is an enumerant of FPRoundingMode.
var FPRoundingModeRTE FPRoundingMode = _FPRoundingModeRTE{}

type _FPRoundingModeRTZ struct{}

func (_FPRoundingModeRTZ) Encode(m *Module, n Instruction, p []uint32) []uint32   { return append(p, 1) }
func (_FPRoundingModeRTZ) Scan(m *Module, n Instruction, p []uint32) (int, error) { return 1, nil }
func (_FPRoundingModeRTZ) Parameters(p []Operand) []Operand                       { return p }
func (_FPRoundingModeRTZ) EnumWord() uint32                                       { return 1 }
func (_FPRoundingModeRTZ) String() string                                         { return "RTZ" }
func (_FPRoundingModeRTZ) isFPRoundingMode()                                      {}

// FPRoundingModeRTZ is an enumerant of FPRoundingMode.
var FPRoundingModeRTZ FPRoundingMode = _FPRoundingModeRTZ{}

type _FPRoundingModeRTP struct{}

func (_FPRoundingModeRTP) Encode(m *Module, n Instruction, p []uint32) []uint32   { return append(p, 2) }
func (_FPRoundingModeRTP) Scan(m *Module, n Instruction, p []uint32) (int, error) { return 1, nil }
func (_FPRoundingModeRTP) Parameters(p []Operand) []Operand                       { return p }
func (_FPRoundingModeRTP) EnumWord() uint32                                       { return 2 }
func (_FPRoundingModeRTP) String() string                                         { return "RTP" }
func (_FPRoundingModeRTP) isFPRoundingMode()                                      {}

// FPRoundingModeRTP is an enumerant of FPRoundingMode.
var FPRoundingModeRTP FPRoundingMode = _FPRoundingModeRTP{}

type _FPRoundingModeRTN struct{}

func (_FPRoundingModeRTN) Encode(m *Module, n Instruction, p []uint32) []uint32   { return append(p, 3) }
func (_FPRoundingModeRTN) Scan(m *Module, n Instruction, p []uint32) (int, error) { return 1, nil }
func (_FPRoundingModeRTN) Parameters(p []Operand) []Operand                       { return p }
func (_FPRoundingModeRTN) EnumWord() uint32                                       { return 3 }
func (_FPRoundingModeRTN) String() string                                         { return "RTN" }
func (_FPRoundingModeRTN) isFPRoundingMode()                                      {}

// FPRoundingModeRTN is an enumerant of FPRoundingMode.
var FPRoundingModeRTN FPRoundingMode = _FPRoundingModeRTN{}
