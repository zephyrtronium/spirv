package spirv

// Export definitions for use in package spirv_test.

func (m *Module) Capabilities() []Capability   { return m.capabilities }
func (m *Module) Extensions() []LiteralString  { return m.extensions }
func (m *Module) Imports() []*OpExtInstImport  { return m.imports }
func (m *Module) MemModel() *OpMemoryModel     { return m.memModel }
func (m *Module) EntryPoints() []*OpEntryPoint { return m.entryPoints }
func (m *Module) ExecModes() []Instruction     { return m.execModes }
func (m *Module) Strings() []Instruction       { return m.strings }
func (m *Module) Names() []Instruction         { return m.names }
func (m *Module) Processes() []LiteralString   { return m.processes }
func (m *Module) Annotations() []Instruction   { return m.annotations }
func (m *Module) Globals() []Instruction       { return m.globl }
func (m *Module) FuncDecls() []Instruction     { return m.funcDecls }
func (m *Module) Funcs() []Instruction         { return m.funcs }
func (m *Module) LitSize() map[Id]int32        { return m.litSize }
func (m *Module) RawVersion() (maj, min uint8) { return m.maj, m.min }

func (l LiteralContextDependentNumber) Raw() uint64 { return l.b }

func InstForOpcode(op uint32) Instruction { return instForOpcode(op) }

type Scratch = scratch

// Ref is a shortcut to obtain a reference to an arbitrary <id>.
func Ref(id Id) IdRef {
	return id.Ref()
}

// Refs is a shortcut to obtain references to a list of <id>s.
func Refs(ids ...Id) []IdRef {
	r := make([]IdRef, len(ids))
	for i, id := range ids {
		r[i] = id.Ref()
	}
	return r
}

// TypeRef is a shortcut to obtain an arbitrary type <id>.
func TypeRef(id Id) IdResultType {
	return IdResultType(id.Ref())
}
