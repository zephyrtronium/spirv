// Copyright (c) 2014-2020 The Khronos Group Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and/or associated documentation files (the "Materials"),
// to deal in the Materials without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Materials, and to permit persons to whom the
// Materials are furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Materials.
//
// MODIFICATIONS TO THIS FILE MAY MEAN IT NO LONGER ACCURATELY REFLECTS KHRONOS
// STANDARDS. THE UNMODIFIED, NORMATIVE VERSIONS OF KHRONOS SPECIFICATIONS AND
// HEADER INFORMATION ARE LOCATED AT https://www.khronos.org/registry/
//
// THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM,OUT OF OR IN CONNECTION WITH THE MATERIALS OR THE USE OR OTHER DEALINGS
// IN THE MATERIALS.

package spirv

// Code generated by spirv/generate; DO NOT EDIT

// KernelEnqueueFlags is a ValueEnum.
type KernelEnqueueFlags interface {
	ValueEnum
	isKernelEnqueueFlags()
}

// KernelEnqueueFlagsEnumerant creates a blank KernelEnqueueFlags containing the enumerant
// corresponding to val. If there is no such enumerant, the result is nil.
func KernelEnqueueFlagsEnumerant(val uint32) KernelEnqueueFlags {
	switch val {
	case 0:
		return KernelEnqueueFlagsNoWait
	case 1:
		return KernelEnqueueFlagsWaitKernel
	case 2:
		return KernelEnqueueFlagsWaitWorkGroup
	default:
		return nil
	}
}

type _KernelEnqueueFlagsNoWait struct{}

func (_KernelEnqueueFlagsNoWait) Encode(m *Module, n Instruction, p []uint32) []uint32 {
	return append(p, 0)
}
func (_KernelEnqueueFlagsNoWait) Scan(m *Module, n Instruction, p []uint32) (int, error) {
	return 1, nil
}
func (_KernelEnqueueFlagsNoWait) Parameters(p []Operand) []Operand { return p }
func (_KernelEnqueueFlagsNoWait) EnumWord() uint32                 { return 0 }
func (_KernelEnqueueFlagsNoWait) String() string                   { return "NoWait" }
func (_KernelEnqueueFlagsNoWait) isKernelEnqueueFlags()            {}

// KernelEnqueueFlagsNoWait is an enumerant of KernelEnqueueFlags.
//
// Enabling capabilities: [Kernel]
var KernelEnqueueFlagsNoWait KernelEnqueueFlags = _KernelEnqueueFlagsNoWait{}

type _KernelEnqueueFlagsWaitKernel struct{}

func (_KernelEnqueueFlagsWaitKernel) Encode(m *Module, n Instruction, p []uint32) []uint32 {
	return append(p, 1)
}
func (_KernelEnqueueFlagsWaitKernel) Scan(m *Module, n Instruction, p []uint32) (int, error) {
	return 1, nil
}
func (_KernelEnqueueFlagsWaitKernel) Parameters(p []Operand) []Operand { return p }
func (_KernelEnqueueFlagsWaitKernel) EnumWord() uint32                 { return 1 }
func (_KernelEnqueueFlagsWaitKernel) String() string                   { return "WaitKernel" }
func (_KernelEnqueueFlagsWaitKernel) isKernelEnqueueFlags()            {}

// KernelEnqueueFlagsWaitKernel is an enumerant of KernelEnqueueFlags.
//
// Enabling capabilities: [Kernel]
var KernelEnqueueFlagsWaitKernel KernelEnqueueFlags = _KernelEnqueueFlagsWaitKernel{}

type _KernelEnqueueFlagsWaitWorkGroup struct{}

func (_KernelEnqueueFlagsWaitWorkGroup) Encode(m *Module, n Instruction, p []uint32) []uint32 {
	return append(p, 2)
}
func (_KernelEnqueueFlagsWaitWorkGroup) Scan(m *Module, n Instruction, p []uint32) (int, error) {
	return 1, nil
}
func (_KernelEnqueueFlagsWaitWorkGroup) Parameters(p []Operand) []Operand { return p }
func (_KernelEnqueueFlagsWaitWorkGroup) EnumWord() uint32                 { return 2 }
func (_KernelEnqueueFlagsWaitWorkGroup) String() string                   { return "WaitWorkGroup" }
func (_KernelEnqueueFlagsWaitWorkGroup) isKernelEnqueueFlags()            {}

// KernelEnqueueFlagsWaitWorkGroup is an enumerant of KernelEnqueueFlags.
//
// Enabling capabilities: [Kernel]
var KernelEnqueueFlagsWaitWorkGroup KernelEnqueueFlags = _KernelEnqueueFlagsWaitWorkGroup{}
