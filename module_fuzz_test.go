//go:build go1.18
// +build go1.18

package spirv_test

import (
	"bytes"
	"io"
	"testing"

	"gitlab.com/zephyrtronium/spirv"
)

func FuzzModule(f *testing.F) {
	var scr spirv.Scratch
	scr.D = []uint32{spirv.Magic, spirv.VersionWord, 0, 1, 0}
	hdr := append(([]byte)(nil), scr.Convert()...)
	f.Add(hdr)
	f.Fuzz(func(t *testing.T, b []byte) {
		// Trim the fuzz data to words.
		b = b[:len(b)-len(b)%4]
		m, err := spirv.Read(bytes.NewReader(b))
		if err != nil {
			return
		}
		// If m decodes successfully, then it should encode successfully.
		// Note that we don't expect m to round-trip, because instructions are
		// likely to be recorded in the module in a different order – we only
		// expect *valid* modules to round-trip.
		m.WriteTo(io.Discard)
	})
}
