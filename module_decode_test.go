package spirv_test

import (
	"bytes"
	"encoding/binary"
	"io"
	"reflect"
	"testing"

	"gitlab.com/zephyrtronium/spirv"
	"gitlab.com/zephyrtronium/spirv/spirvop"
)

var spirvMagicLE, spirvMagicBE [4]byte

func init() {
	binary.LittleEndian.PutUint32(spirvMagicLE[:], spirv.Magic)
	binary.BigEndian.PutUint32(spirvMagicBE[:], spirv.Magic)
}

func TestReadHeader(t *testing.T) {
	const (
		kmag = iota
		kver
		kgen
		kbnd
		kres
	)
	cases := []struct {
		name     string
		words    []uint32
		maj, min uint8
		bound    spirv.Id
		gm       uint32
		errs     map[binary.ByteOrder]error
	}{
		{
			name:  "ok",
			words: []uint32{kmag: spirv.Magic, kver: spirv.VersionWord, kgen: 0xffff, kbnd: 2, kres: 0},
			maj:   spirv.MajorVersion, min: spirv.MinorVersion,
			bound: 2,
			gm:    0xffff,
		},
		{
			name:  "magic",
			words: []uint32{kmag: spirv.Magic + 1, kver: spirv.VersionWord, kgen: 0xffff, kbnd: 2, kres: 0},
			bound: 1,
			errs: map[binary.ByteOrder]error{
				binary.LittleEndian: &spirv.InvalidHeaderError{
					Magic: [4]byte{0x04, 0x02, 0x23, 0x07},
				},
				binary.BigEndian: &spirv.InvalidHeaderError{
					Magic: [4]byte{0x07, 0x23, 0x02, 0x04},
				},
			},
		},
		{
			name:  "version",
			words: []uint32{kmag: spirv.Magic, kver: 0x01000006, kgen: 0xffff, kbnd: 2, kres: 0},
			bound: 1,
			errs: map[binary.ByteOrder]error{
				binary.LittleEndian: &spirv.InvalidHeaderError{
					Magic:          spirvMagicLE,
					Version:        0x01000006,
					GeneratorMagic: 0xffff,
					Bound:          2,
					Reserved:       0,
				},
				binary.BigEndian: &spirv.InvalidHeaderError{
					Magic:          spirvMagicBE,
					Version:        0x01000006,
					GeneratorMagic: 0xffff,
					Bound:          2,
					Reserved:       0,
				},
			},
		},
		{
			name:  "bound",
			words: []uint32{kmag: spirv.Magic, kver: spirv.VersionWord, kgen: 0xffff, kbnd: 0, kres: 0},
			bound: 1,
			errs: map[binary.ByteOrder]error{
				binary.LittleEndian: &spirv.InvalidHeaderError{
					Magic:          spirvMagicLE,
					Version:        spirv.VersionWord,
					GeneratorMagic: 0xffff,
					Bound:          0,
					Reserved:       0,
				},
				binary.BigEndian: &spirv.InvalidHeaderError{
					Magic:          spirvMagicBE,
					Version:        spirv.VersionWord,
					GeneratorMagic: 0xffff,
					Bound:          0,
					Reserved:       0,
				},
			},
		},
		{
			name:  "reserved",
			words: []uint32{kmag: spirv.Magic, kver: spirv.VersionWord, kgen: 0xffff, kbnd: 2, kres: 1},
			bound: 1,
			errs: map[binary.ByteOrder]error{
				binary.LittleEndian: &spirv.InvalidHeaderError{
					Magic:          spirvMagicLE,
					Version:        spirv.VersionWord,
					GeneratorMagic: 0xffff,
					Bound:          2,
					Reserved:       1,
				},
				binary.BigEndian: &spirv.InvalidHeaderError{
					Magic:          spirvMagicBE,
					Version:        spirv.VersionWord,
					GeneratorMagic: 0xffff,
					Bound:          2,
					Reserved:       1,
				},
			},
		},
		{
			name:  "short",
			words: []uint32{kmag: spirv.Magic, kver: spirv.VersionWord, kgen: 0xffff, kbnd: 2},
			bound: 1,
			errs: map[binary.ByteOrder]error{
				binary.LittleEndian: io.ErrUnexpectedEOF,
				binary.BigEndian:    io.ErrUnexpectedEOF,
			},
		},
	}
	orders := []binary.ByteOrder{binary.LittleEndian, binary.BigEndian}

	for _, order := range orders {
		for _, c := range cases {
			t.Run(c.name+order.String(), func(t *testing.T) {
				var m spirv.Module
				var b bytes.Buffer
				var scr spirv.Scratch
				binary.Write(&b, order, c.words)
				o, err := scr.ReadHeader(&b, &m)
				if o != order && (c.words[0] == spirv.Magic || o != nil) {
					// TODO(zeph): check whether this is any error from spirv
					if _, ok := err.(*spirv.InvalidHeaderError); ok {
						t.Errorf("wrong byte order: want %v, got %v", order, o)
					}
				}
				if maj, min := m.RawVersion(); maj != c.maj || min != c.min {
					t.Errorf("wrong version: want %d.%d, got %d.%d", c.maj, c.min, maj, min)
				}
				if bnd := m.Bound(); bnd != c.bound {
					t.Errorf("wrong bound: want %x, got %x", c.bound, bnd)
				}
				if m.Magic != c.gm {
					t.Errorf("wrong magic number: want %x, got %x", c.gm, m.Magic)
				}
				if !eqerrs(t, err, c.errs[order]) {
					t.Errorf("wrong error: want %#v, got %#v", c.errs[order], err)
				}
			})
		}
	}
}

func TestReadInst(t *testing.T) {
	first := func(op, len uint32) uint32 { return op | len<<16 }
	cases := []struct {
		name  string
		words []uint32
		want  []uint32
		op    uint32
		err   error
	}{
		{
			name:  "one",
			words: []uint32{first(317, 1)},
			want:  []uint32{first(317, 1)},
			op:    317,
		},
		{
			name:  "several",
			words: []uint32{first(1, 3), 1, 2},
			want:  []uint32{first(1, 3), 1, 2},
			op:    1,
		},
		{
			name:  "long",
			words: append([]uint32{first(2, 0xffff)}, make([]uint32, 0xfffe)...),
			want:  append([]uint32{first(2, 0xffff)}, make([]uint32, 0xfffe)...),
			op:    2,
		},
		{
			name:  "extra",
			words: []uint32{first(1, 3), 1, 2, 0, 0, 0, 0},
			want:  []uint32{first(1, 3), 1, 2},
			op:    1,
		},
		{
			name:  "short",
			words: []uint32{first(1, 3)},
			err:   io.ErrUnexpectedEOF,
		},
		{
			name:  "zerolen",
			words: []uint32{first(1, 0), 1, 2},
			err:   &spirv.ZeroInstructionLengthError{Opcode: 1},
		},
		{
			name: "empty",
			err:  io.EOF,
		},
	}
	orders := []binary.ByteOrder{binary.LittleEndian, binary.BigEndian}

	for _, order := range orders {
		for _, c := range cases {
			t.Run(c.name+order.String(), func(t *testing.T) {
				var b bytes.Buffer
				var scr spirv.Scratch
				binary.Write(&b, order, c.words)
				d, op, err := scr.ReadInst(&b, order)
				if !reflect.DeepEqual(d, c.want) {
					t.Errorf("wrong decoded data: want %08x, got %08x", c.want, d)
				}
				if op != c.op {
					t.Errorf("wrong opcode: want %d, got %d", c.op, op)
				}
				if !eqerrs(t, err, c.err) {
					t.Errorf("wrong error: want %#v, got %#v", c.err, err)
				}
			})
		}
	}
}

func TestScanInst(t *testing.T) {
	first := func(op, len uint32) uint32 { return op | len<<16 }
	cases := []struct {
		name  string
		inst  spirv.Instruction
		words []uint32
		want  spirv.Instruction
		err   error
	}{
		{
			name:  "empty",
			inst:  new(spirv.OpNop),
			words: []uint32{first(0, 1)},
			want:  &spirv.OpNop{},
		},
		{
			name:  "operands",
			inst:  new(spirv.OpUndef),
			words: []uint32{first(1, 3), 1, 12},
			want: &spirv.OpUndef{
				ResultType: spirv.TypeRef(1),
				Result:     12,
			},
		},
		{
			name:  "valenum",
			inst:  new(spirv.OpTypePointer),
			words: []uint32{first(32, 4), 16, 1, 3},
			want: &spirv.OpTypePointer{
				Result:   16,
				Operand1: spirv.StorageClassInput,
				Type:     spirv.Ref(3),
			},
		},
		{
			name:  "valenumParams",
			inst:  new(spirv.OpDecorate),
			words: []uint32{first(71, 4), 19, 39, 1},
			want: &spirv.OpDecorate{
				Target:   spirv.Ref(19),
				Operand1: &spirv.DecorationFPRoundingMode{Param0: spirv.FPRoundingModeRTZ},
			},
		},
		{
			name:  "bitenum",
			inst:  new(spirv.OpLoopMerge),
			words: []uint32{first(246, 4), 17, 18, 0x1 | 0x4},
			want: &spirv.OpLoopMerge{
				MergeBlock:     spirv.Ref(17),
				ContinueTarget: spirv.Ref(18),
				Operand2: *(&spirv.LoopControl{}).SetBits(
					spirv.LoopControlUnroll,
					spirv.LoopControlDependencyInfinite,
				),
			},
		},
		{
			name:  "operandsMissing",
			inst:  new(spirv.OpUndef),
			words: []uint32{first(1, 3)},
			want:  &spirv.OpUndef{},
			err: &spirv.ShortInstructionError{
				Inst:    new(spirv.OpUndef),
				Operand: "<id>",
			},
		},
		{
			name:  "optionalAbsent",
			inst:  new(spirv.OpLoad),
			words: []uint32{first(61, 4), 1, 14, 13},
			want: &spirv.OpLoad{
				ResultType: spirv.TypeRef(1),
				Result:     14,
				Pointer:    spirv.Ref(13),
			},
		},
		{
			name:  "optionalPresent",
			inst:  new(spirv.OpLoad),
			words: []uint32{first(61, 5), 1, 14, 13, 1},
			want: &spirv.OpLoad{
				ResultType: spirv.TypeRef(1),
				Result:     14,
				Pointer:    spirv.Ref(13),
				Operand3:   new(spirv.MemoryAccess).SetBits(spirv.MemoryAccessVolatile),
			},
		},
		{
			name:  "optionalWithParams",
			inst:  new(spirv.OpLoad),
			words: []uint32{first(61, 7), 1, 14, 13, 0x2 | 0x8 | 0x20, 4, 15},
			want: &spirv.OpLoad{
				ResultType: spirv.TypeRef(1),
				Result:     14,
				Pointer:    spirv.Ref(13),
				Operand3: new(spirv.MemoryAccess).SetBits(
					&spirv.MemoryAccessAligned{Param0: 4},
					&spirv.MemoryAccessMakePointerAvailable{Param0: spirv.Ref(15)},
					spirv.MemoryAccessNonPrivatePointer,
				),
			},
		},
		{
			name:  "optionalWithMissingParams",
			inst:  new(spirv.OpLoad),
			words: []uint32{first(61, 6), 1, 14, 13, 0x2},
			want: &spirv.OpLoad{
				ResultType: spirv.TypeRef(1),
				Result:     14,
				Pointer:    spirv.Ref(13),
				Operand3:   new(spirv.MemoryAccess).SetBits(&spirv.MemoryAccessAligned{}),
			},
			err: &spirv.ShortInstructionError{
				Inst:    new(spirv.OpLoad),
				Operand: "LiteralInteger",
			},
		},
		{
			name:  "repeatedEmpty",
			inst:  new(spirv.OpTypeStruct),
			words: []uint32{first(30, 2), 15},
			want: &spirv.OpTypeStruct{
				Result:   15,
				Operand1: []spirv.IdRef{},
			},
		},
		{
			name:  "repeatedSeveral",
			inst:  new(spirv.OpTypeStruct),
			words: []uint32{first(30, 5), 15, 3, 2, 1},
			want: &spirv.OpTypeStruct{
				Result:   15,
				Operand1: spirv.Refs(3, 2, 1),
			},
		},
	}
	m := moduleWithTypes()

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			var scr spirv.Scratch
			err := scr.ScanInst(m, c.inst, c.words)
			if !reflect.DeepEqual(c.inst, c.want) {
				t.Errorf("inst decoded wrong: want %#v, got %#v", c.want, c.inst)
			}
			if !eqerrs(t, err, c.err) {
				t.Errorf("wrong error: want %#v, got %#v", c.err, err)
			}
		})
	}
}

// TestReadErrors tests that errors which can be produced while decoding
// instructions in a module are correct.
func TestReadErrors(t *testing.T) {
	const fakeop = 65000
	if spirv.InstForOpcode(fakeop) != nil {
		t.Logf("opcode %d isn't fake anymore!!", fakeop)
	}
	hdr := []uint32{spirv.Magic, spirv.VersionWord, 0, 100, 0}
	cases := []struct {
		name string
		data []uint32
		err  error
	}{
		{
			name: "ZeroInstructionLength",
			data: []uint32{
				spirvop.OpMemoryModel | 0<<16, 0, 0,
			},
			err: &spirv.ZeroInstructionLengthError{
				InstPos: 1,
				WordPos: 5,
				Opcode:  spirvop.OpMemoryModel,
			},
		},
		{
			name: "ZeroInstructionLength-position",
			data: []uint32{
				spirvop.OpMemoryModel | 3<<16, 0, 0,
				spirvop.OpTypeFloat | 0<<16, 10, 32,
			},
			err: &spirv.ZeroInstructionLengthError{
				InstPos: 2,
				WordPos: 8,
				Opcode:  spirvop.OpTypeFloat,
			},
		},
		{
			name: "UnknownOpcode",
			data: []uint32{
				fakeop | 1<<16,
			},
			err: &spirv.UnknownOpcodeError{
				InstPos: 1,
				WordPos: 5,
				Opcode:  fakeop,
				Data:    []uint32{fakeop | 1<<16},
			},
		},
		{
			name: "UnknownOpcode-position",
			data: []uint32{
				spirvop.OpMemoryModel | 3<<16, 0, 0,
				fakeop | 1<<16,
			},
			err: &spirv.UnknownOpcodeError{
				InstPos: 2,
				WordPos: 8,
				Opcode:  fakeop,
				Data:    []uint32{fakeop | 1<<16},
			},
		},
		{
			name: "ShortInstruction",
			data: []uint32{
				spirvop.OpMemoryModel | 2<<16, 0, 0,
			},
			err: &spirv.ShortInstructionError{
				InstPos: 1,
				WordPos: 5,
				Inst:    new(spirv.OpMemoryModel),
				Operand: "Operand1",
			},
		},
		{
			name: "ShortInstruction-position",
			data: []uint32{
				spirvop.OpMemoryModel | 3<<16, 0, 0,
				spirvop.OpTypeFloat | 2<<16, 10, 32,
			},
			err: &spirv.ShortInstructionError{
				InstPos: 2,
				WordPos: 8,
				Inst:    new(spirv.OpTypeFloat),
				Operand: "Width",
			},
		},
		// TODO(zeph): many more cases
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			d := append(hdr, c.data...)
			var b bytes.Buffer
			binary.Write(&b, binary.LittleEndian, d)
			m, err := spirv.Read(&b)
			if m != nil {
				t.Errorf("shouldn't have gotten module %+v", m)
			}
			if !eqerrs(t, err, c.err) {
				t.Errorf("wrong error: wanted %#v, got %#v", c.err, err)
			}
		})
	}
}

// moduleWithTypes initializes a module with a variety of types declared.
// <id>s %1 through %4 are uint8 through uint64. %5 through %8 are int8 through
// int64. %9 through %11 are float16 through float64.
func moduleWithTypes() *spirv.Module {
	var m spirv.Module
	setup := []spirv.Instruction{
		&spirv.OpCapability{Capability: spirv.CapabilityInt8},
		&spirv.OpCapability{Capability: spirv.CapabilityInt16},
		&spirv.OpCapability{Capability: spirv.CapabilityInt64},
		&spirv.OpCapability{Capability: spirv.CapabilityFloat16},
		&spirv.OpCapability{Capability: spirv.CapabilityFloat64},
		&spirv.OpMemoryModel{Operand0: spirv.AddressingModelLogical, Operand1: spirv.MemoryModelSimple},

		&spirv.OpTypeInt{Result: 1, Width: 8, Signedness: 0},  // %1 = uint8
		&spirv.OpTypeInt{Result: 2, Width: 16, Signedness: 0}, // %2 = uint16
		&spirv.OpTypeInt{Result: 3, Width: 32, Signedness: 0}, // %3 = uint32
		&spirv.OpTypeInt{Result: 4, Width: 64, Signedness: 0}, // %4 = uint64
		&spirv.OpTypeInt{Result: 5, Width: 8, Signedness: 1},  // %5 = int8
		&spirv.OpTypeInt{Result: 6, Width: 16, Signedness: 1}, // %6 = int16
		&spirv.OpTypeInt{Result: 7, Width: 32, Signedness: 1}, // %7 = int32
		&spirv.OpTypeInt{Result: 8, Width: 64, Signedness: 1}, // %8 = int64
		&spirv.OpTypeFloat{Result: 9, Width: 16},              // %9 = float16
		&spirv.OpTypeFloat{Result: 10, Width: 32},             // %10 = float32
		&spirv.OpTypeFloat{Result: 11, Width: 64},             // %11 = float64
	}
	for _, n := range setup {
		m.Add(n)
	}
	return &m
}
