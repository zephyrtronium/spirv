package spirv

import "errors"

// A Validator observes each instruction in a SPIR-V module.
type Validator interface {
	// Visit receives a SPIR-V instruction. If the returned error is non-nil,
	// validation halts. Visit must not modify a module undergoing validation,
	// nor instructions passed to it.
	Visit(inst Instruction) error
}

// ValidateOptions controls validation. A nil ValidateOptions is equivalent to
// the zero value for Validate.
type ValidateOptions struct {
	// SkipCapabilities indicates to skip all OpCapability instructions.
	SkipCapabilities bool
	// SkipExtensions indicates to skip all OpExtension instructions.
	SkipExtensions bool
	// SkipExtInstImports indicates to skip all OpExtInstImport instructions.
	SkipExtInstImports bool
	// SkipMemoryModel indicates to skip the single required OpMemoryModel
	// instruction.
	SkipMemoryModel bool
	// SkipEntryPoints indicates to skip all OpEntryPoint instructions.
	SkipEntryPoints bool
	// SkipExecutionModes indicates to skip all OpExecutionMode and
	// OpExecutionModeId instructions.
	SkipExecutionModes bool
	// SkipStrings indicates to skip all OpString, OpSource, OpSourceContinued,
	// and OpSourceExtension instructions.
	SkipStrings bool
	// SkipNames indicates to skip all OpName and OpMemberName instructions.
	SkipNames bool
	// SkipModuleProcessed indicates to skip all OpModuleProcessed
	// instructions.
	SkipModuleProcessed bool
	// SkipDebug is equivalent to setting SkipStrings, SkipNames, and
	// SkipModuleProcessed.
	SkipDebug bool
	// SkipAnnotations indicates to skip all annotation instructions. These
	// include OpDecorate and its variants.
	SkipAnnotations bool
	// SkipDeclarations indicates to skip all type, constant, and global
	// variable declarations, as well as all OpLine, OpNoLine, and nonsemantic
	// OpExtInst instructions pertaining to such declarations.
	SkipDeclarations bool
	// SkipFuncDecls indicates to skip all function declarations, i.e.
	// functions with no bodies, consisting of OpFunction, any number of
	// OpFunctionParameter, and OpFunctionEnd.
	SkipFuncDecls bool
	// SkipFunctions indicates to skip all function definitions.
	SkipFunctions bool
}

// Validate visits each instruction in the module. Instructions are visited
// first in the order of the logical module layout proscribed in Section 2.4 of
// the SPIR-V specification, and subsequently in the order they were added to
// the module. Validation ends when the validator returns a non-nil error, and
// that error is returned from Validate unless it is StopValidate. Validate
// panics if the module does not have a memory model set, either through
// SetMemoryModel or by adding an OpMemoryModel, even if SkipMemoryModel is
// set in options.
func (m *Module) Validate(v Validator, options *ValidateOptions) error {
	if m.memModel == nil {
		panic("spirv: no memory model on validation")
	}
	var opts ValidateOptions
	if options != nil {
		opts = *options
	}

	if !opts.SkipCapabilities {
		for _, c := range m.capabilities {
			inst := OpCapability{Capability: c}
			switch err := v.Visit(&inst); err {
			case nil: // do nothing
			case StopValidate:
				return nil
			default:
				return err
			}
		}
	}
	if !opts.SkipExtensions {
		for _, e := range m.extensions {
			inst := OpExtension{Name: e}
			switch err := v.Visit(&inst); err {
			case nil: // do nothing
			case StopValidate:
				return nil
			default:
				return err
			}
		}
	}
	if !opts.SkipExtInstImports {
		for _, inst := range m.imports {
			switch err := v.Visit(inst); err {
			case nil: // do nothing
			case StopValidate:
				return nil
			default:
				return err
			}
		}
	}
	if !opts.SkipMemoryModel {
		switch err := v.Visit(m.memModel); err {
		case nil: // do nothing
		case StopValidate:
			return nil
		default:
			return err
		}
	}
	if !opts.SkipEntryPoints {
		for _, inst := range m.entryPoints {
			switch err := v.Visit(inst); err {
			case nil: // do nothing
			case StopValidate:
				return nil
			default:
				return err
			}
		}
	}
	if !opts.SkipExecutionModes {
		for _, inst := range m.execModes {
			switch err := v.Visit(inst); err {
			case nil: // do nothing
			case StopValidate:
				return nil
			default:
				return err
			}
		}
	}
	if !opts.SkipStrings && !opts.SkipDebug {
		for _, inst := range m.strings {
			switch err := v.Visit(inst); err {
			case nil: // do nothing
			case StopValidate:
				return nil
			default:
				return err
			}
		}
	}
	if !opts.SkipNames && !opts.SkipDebug {
		for _, inst := range m.names {
			switch err := v.Visit(inst); err {
			case nil: // do nothing
			case StopValidate:
				return nil
			default:
				return err
			}
		}
	}
	if !opts.SkipModuleProcessed && !opts.SkipDebug {
		for _, p := range m.processes {
			inst := OpModuleProcessed{Process: p}
			switch err := v.Visit(&inst); err {
			case nil: // do nothing
			case StopValidate:
				return nil
			default:
				return err
			}
		}
	}
	if !opts.SkipAnnotations {
		for _, inst := range m.annotations {
			switch err := v.Visit(inst); err {
			case nil: // do nothing
			case StopValidate:
				return nil
			default:
				return err
			}
		}
	}
	if !opts.SkipDeclarations {
		for _, inst := range m.globl {
			switch err := v.Visit(inst); err {
			case nil: // do nothing
			case StopValidate:
				return nil
			default:
				return err
			}
		}
	}
	if !opts.SkipFuncDecls {
		for _, inst := range m.funcDecls {
			switch err := v.Visit(inst); err {
			case nil: // do nothing
			case StopValidate:
				return nil
			default:
				return err
			}
		}
	}
	if !opts.SkipFunctions {
		for _, inst := range m.funcs {
			switch err := v.Visit(inst); err {
			case nil: // do nothing
			case StopValidate:
				return nil
			default:
				return err
			}
		}
	}

	return nil
}

// ValidatorFunc implements Validator with a function value.
type ValidatorFunc func(Instruction) error

func (v ValidatorFunc) Visit(inst Instruction) error {
	return v(inst)
}

var _ Validator = ValidatorFunc(nil)

// StopValidate is a sentinel error which can be returned by a validator to
// stop validation such that Validate returns nil.
var StopValidate = errors.New("stop")
