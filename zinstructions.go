// Copyright (c) 2014-2020 The Khronos Group Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and/or associated documentation files (the "Materials"),
// to deal in the Materials without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Materials, and to permit persons to whom the
// Materials are furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Materials.
//
// MODIFICATIONS TO THIS FILE MAY MEAN IT NO LONGER ACCURATELY REFLECTS KHRONOS
// STANDARDS. THE UNMODIFIED, NORMATIVE VERSIONS OF KHRONOS SPECIFICATIONS AND
// HEADER INFORMATION ARE LOCATED AT https://www.khronos.org/registry/
//
// THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM,OUT OF OR IN CONNECTION WITH THE MATERIALS OR THE USE OR OTHER DEALINGS
// IN THE MATERIALS.

package spirv

// Code generated by spirv/generate; DO NOT EDIT

func instForOpcode(opcode uint32) Instruction {
	switch opcode {
	case 0:
		return new(OpNop)
	case 1:
		return new(OpUndef)
	case 2:
		return new(OpSourceContinued)
	case 3:
		return new(OpSource)
	case 4:
		return new(OpSourceExtension)
	case 5:
		return new(OpName)
	case 6:
		return new(OpMemberName)
	case 7:
		return new(OpString)
	case 8:
		return new(OpLine)
	case 10:
		return new(OpExtension)
	case 11:
		return new(OpExtInstImport)
	case 12:
		return new(OpExtInst)
	case 14:
		return new(OpMemoryModel)
	case 15:
		return new(OpEntryPoint)
	case 16:
		return new(OpExecutionMode)
	case 17:
		return new(OpCapability)
	case 19:
		return new(OpTypeVoid)
	case 20:
		return new(OpTypeBool)
	case 21:
		return new(OpTypeInt)
	case 22:
		return new(OpTypeFloat)
	case 23:
		return new(OpTypeVector)
	case 24:
		return new(OpTypeMatrix)
	case 25:
		return new(OpTypeImage)
	case 26:
		return new(OpTypeSampler)
	case 27:
		return new(OpTypeSampledImage)
	case 28:
		return new(OpTypeArray)
	case 29:
		return new(OpTypeRuntimeArray)
	case 30:
		return new(OpTypeStruct)
	case 31:
		return new(OpTypeOpaque)
	case 32:
		return new(OpTypePointer)
	case 33:
		return new(OpTypeFunction)
	case 34:
		return new(OpTypeEvent)
	case 35:
		return new(OpTypeDeviceEvent)
	case 36:
		return new(OpTypeReserveId)
	case 37:
		return new(OpTypeQueue)
	case 38:
		return new(OpTypePipe)
	case 39:
		return new(OpTypeForwardPointer)
	case 41:
		return new(OpConstantTrue)
	case 42:
		return new(OpConstantFalse)
	case 43:
		return new(OpConstant)
	case 44:
		return new(OpConstantComposite)
	case 45:
		return new(OpConstantSampler)
	case 46:
		return new(OpConstantNull)
	case 48:
		return new(OpSpecConstantTrue)
	case 49:
		return new(OpSpecConstantFalse)
	case 50:
		return new(OpSpecConstant)
	case 51:
		return new(OpSpecConstantComposite)
	case 52:
		return new(OpSpecConstantOp)
	case 54:
		return new(OpFunction)
	case 55:
		return new(OpFunctionParameter)
	case 56:
		return new(OpFunctionEnd)
	case 57:
		return new(OpFunctionCall)
	case 59:
		return new(OpVariable)
	case 60:
		return new(OpImageTexelPointer)
	case 61:
		return new(OpLoad)
	case 62:
		return new(OpStore)
	case 63:
		return new(OpCopyMemory)
	case 64:
		return new(OpCopyMemorySized)
	case 65:
		return new(OpAccessChain)
	case 66:
		return new(OpInBoundsAccessChain)
	case 67:
		return new(OpPtrAccessChain)
	case 68:
		return new(OpArrayLength)
	case 69:
		return new(OpGenericPtrMemSemantics)
	case 70:
		return new(OpInBoundsPtrAccessChain)
	case 71:
		return new(OpDecorate)
	case 72:
		return new(OpMemberDecorate)
	case 73:
		return new(OpDecorationGroup)
	case 74:
		return new(OpGroupDecorate)
	case 75:
		return new(OpGroupMemberDecorate)
	case 77:
		return new(OpVectorExtractDynamic)
	case 78:
		return new(OpVectorInsertDynamic)
	case 79:
		return new(OpVectorShuffle)
	case 80:
		return new(OpCompositeConstruct)
	case 81:
		return new(OpCompositeExtract)
	case 82:
		return new(OpCompositeInsert)
	case 83:
		return new(OpCopyObject)
	case 84:
		return new(OpTranspose)
	case 86:
		return new(OpSampledImage)
	case 87:
		return new(OpImageSampleImplicitLod)
	case 88:
		return new(OpImageSampleExplicitLod)
	case 89:
		return new(OpImageSampleDrefImplicitLod)
	case 90:
		return new(OpImageSampleDrefExplicitLod)
	case 91:
		return new(OpImageSampleProjImplicitLod)
	case 92:
		return new(OpImageSampleProjExplicitLod)
	case 93:
		return new(OpImageSampleProjDrefImplicitLod)
	case 94:
		return new(OpImageSampleProjDrefExplicitLod)
	case 95:
		return new(OpImageFetch)
	case 96:
		return new(OpImageGather)
	case 97:
		return new(OpImageDrefGather)
	case 98:
		return new(OpImageRead)
	case 99:
		return new(OpImageWrite)
	case 100:
		return new(OpImage)
	case 101:
		return new(OpImageQueryFormat)
	case 102:
		return new(OpImageQueryOrder)
	case 103:
		return new(OpImageQuerySizeLod)
	case 104:
		return new(OpImageQuerySize)
	case 105:
		return new(OpImageQueryLod)
	case 106:
		return new(OpImageQueryLevels)
	case 107:
		return new(OpImageQuerySamples)
	case 109:
		return new(OpConvertFToU)
	case 110:
		return new(OpConvertFToS)
	case 111:
		return new(OpConvertSToF)
	case 112:
		return new(OpConvertUToF)
	case 113:
		return new(OpUConvert)
	case 114:
		return new(OpSConvert)
	case 115:
		return new(OpFConvert)
	case 116:
		return new(OpQuantizeToF16)
	case 117:
		return new(OpConvertPtrToU)
	case 118:
		return new(OpSatConvertSToU)
	case 119:
		return new(OpSatConvertUToS)
	case 120:
		return new(OpConvertUToPtr)
	case 121:
		return new(OpPtrCastToGeneric)
	case 122:
		return new(OpGenericCastToPtr)
	case 123:
		return new(OpGenericCastToPtrExplicit)
	case 124:
		return new(OpBitcast)
	case 126:
		return new(OpSNegate)
	case 127:
		return new(OpFNegate)
	case 128:
		return new(OpIAdd)
	case 129:
		return new(OpFAdd)
	case 130:
		return new(OpISub)
	case 131:
		return new(OpFSub)
	case 132:
		return new(OpIMul)
	case 133:
		return new(OpFMul)
	case 134:
		return new(OpUDiv)
	case 135:
		return new(OpSDiv)
	case 136:
		return new(OpFDiv)
	case 137:
		return new(OpUMod)
	case 138:
		return new(OpSRem)
	case 139:
		return new(OpSMod)
	case 140:
		return new(OpFRem)
	case 141:
		return new(OpFMod)
	case 142:
		return new(OpVectorTimesScalar)
	case 143:
		return new(OpMatrixTimesScalar)
	case 144:
		return new(OpVectorTimesMatrix)
	case 145:
		return new(OpMatrixTimesVector)
	case 146:
		return new(OpMatrixTimesMatrix)
	case 147:
		return new(OpOuterProduct)
	case 148:
		return new(OpDot)
	case 149:
		return new(OpIAddCarry)
	case 150:
		return new(OpISubBorrow)
	case 151:
		return new(OpUMulExtended)
	case 152:
		return new(OpSMulExtended)
	case 154:
		return new(OpAny)
	case 155:
		return new(OpAll)
	case 156:
		return new(OpIsNan)
	case 157:
		return new(OpIsInf)
	case 158:
		return new(OpIsFinite)
	case 159:
		return new(OpIsNormal)
	case 160:
		return new(OpSignBitSet)
	case 161:
		return new(OpLessOrGreater)
	case 162:
		return new(OpOrdered)
	case 163:
		return new(OpUnordered)
	case 164:
		return new(OpLogicalEqual)
	case 165:
		return new(OpLogicalNotEqual)
	case 166:
		return new(OpLogicalOr)
	case 167:
		return new(OpLogicalAnd)
	case 168:
		return new(OpLogicalNot)
	case 169:
		return new(OpSelect)
	case 170:
		return new(OpIEqual)
	case 171:
		return new(OpINotEqual)
	case 172:
		return new(OpUGreaterThan)
	case 173:
		return new(OpSGreaterThan)
	case 174:
		return new(OpUGreaterThanEqual)
	case 175:
		return new(OpSGreaterThanEqual)
	case 176:
		return new(OpULessThan)
	case 177:
		return new(OpSLessThan)
	case 178:
		return new(OpULessThanEqual)
	case 179:
		return new(OpSLessThanEqual)
	case 180:
		return new(OpFOrdEqual)
	case 181:
		return new(OpFUnordEqual)
	case 182:
		return new(OpFOrdNotEqual)
	case 183:
		return new(OpFUnordNotEqual)
	case 184:
		return new(OpFOrdLessThan)
	case 185:
		return new(OpFUnordLessThan)
	case 186:
		return new(OpFOrdGreaterThan)
	case 187:
		return new(OpFUnordGreaterThan)
	case 188:
		return new(OpFOrdLessThanEqual)
	case 189:
		return new(OpFUnordLessThanEqual)
	case 190:
		return new(OpFOrdGreaterThanEqual)
	case 191:
		return new(OpFUnordGreaterThanEqual)
	case 194:
		return new(OpShiftRightLogical)
	case 195:
		return new(OpShiftRightArithmetic)
	case 196:
		return new(OpShiftLeftLogical)
	case 197:
		return new(OpBitwiseOr)
	case 198:
		return new(OpBitwiseXor)
	case 199:
		return new(OpBitwiseAnd)
	case 200:
		return new(OpNot)
	case 201:
		return new(OpBitFieldInsert)
	case 202:
		return new(OpBitFieldSExtract)
	case 203:
		return new(OpBitFieldUExtract)
	case 204:
		return new(OpBitReverse)
	case 205:
		return new(OpBitCount)
	case 207:
		return new(OpDPdx)
	case 208:
		return new(OpDPdy)
	case 209:
		return new(OpFwidth)
	case 210:
		return new(OpDPdxFine)
	case 211:
		return new(OpDPdyFine)
	case 212:
		return new(OpFwidthFine)
	case 213:
		return new(OpDPdxCoarse)
	case 214:
		return new(OpDPdyCoarse)
	case 215:
		return new(OpFwidthCoarse)
	case 218:
		return new(OpEmitVertex)
	case 219:
		return new(OpEndPrimitive)
	case 220:
		return new(OpEmitStreamVertex)
	case 221:
		return new(OpEndStreamPrimitive)
	case 224:
		return new(OpControlBarrier)
	case 225:
		return new(OpMemoryBarrier)
	case 227:
		return new(OpAtomicLoad)
	case 228:
		return new(OpAtomicStore)
	case 229:
		return new(OpAtomicExchange)
	case 230:
		return new(OpAtomicCompareExchange)
	case 231:
		return new(OpAtomicCompareExchangeWeak)
	case 232:
		return new(OpAtomicIIncrement)
	case 233:
		return new(OpAtomicIDecrement)
	case 234:
		return new(OpAtomicIAdd)
	case 235:
		return new(OpAtomicISub)
	case 236:
		return new(OpAtomicSMin)
	case 237:
		return new(OpAtomicUMin)
	case 238:
		return new(OpAtomicSMax)
	case 239:
		return new(OpAtomicUMax)
	case 240:
		return new(OpAtomicAnd)
	case 241:
		return new(OpAtomicOr)
	case 242:
		return new(OpAtomicXor)
	case 245:
		return new(OpPhi)
	case 246:
		return new(OpLoopMerge)
	case 247:
		return new(OpSelectionMerge)
	case 248:
		return new(OpLabel)
	case 249:
		return new(OpBranch)
	case 250:
		return new(OpBranchConditional)
	case 251:
		return new(OpSwitch)
	case 252:
		return new(OpKill)
	case 253:
		return new(OpReturn)
	case 254:
		return new(OpReturnValue)
	case 255:
		return new(OpUnreachable)
	case 256:
		return new(OpLifetimeStart)
	case 257:
		return new(OpLifetimeStop)
	case 259:
		return new(OpGroupAsyncCopy)
	case 260:
		return new(OpGroupWaitEvents)
	case 261:
		return new(OpGroupAll)
	case 262:
		return new(OpGroupAny)
	case 263:
		return new(OpGroupBroadcast)
	case 264:
		return new(OpGroupIAdd)
	case 265:
		return new(OpGroupFAdd)
	case 266:
		return new(OpGroupFMin)
	case 267:
		return new(OpGroupUMin)
	case 268:
		return new(OpGroupSMin)
	case 269:
		return new(OpGroupFMax)
	case 270:
		return new(OpGroupUMax)
	case 271:
		return new(OpGroupSMax)
	case 274:
		return new(OpReadPipe)
	case 275:
		return new(OpWritePipe)
	case 276:
		return new(OpReservedReadPipe)
	case 277:
		return new(OpReservedWritePipe)
	case 278:
		return new(OpReserveReadPipePackets)
	case 279:
		return new(OpReserveWritePipePackets)
	case 280:
		return new(OpCommitReadPipe)
	case 281:
		return new(OpCommitWritePipe)
	case 282:
		return new(OpIsValidReserveId)
	case 283:
		return new(OpGetNumPipePackets)
	case 284:
		return new(OpGetMaxPipePackets)
	case 285:
		return new(OpGroupReserveReadPipePackets)
	case 286:
		return new(OpGroupReserveWritePipePackets)
	case 287:
		return new(OpGroupCommitReadPipe)
	case 288:
		return new(OpGroupCommitWritePipe)
	case 291:
		return new(OpEnqueueMarker)
	case 292:
		return new(OpEnqueueKernel)
	case 293:
		return new(OpGetKernelNDrangeSubGroupCount)
	case 294:
		return new(OpGetKernelNDrangeMaxSubGroupSize)
	case 295:
		return new(OpGetKernelWorkGroupSize)
	case 296:
		return new(OpGetKernelPreferredWorkGroupSizeMultiple)
	case 297:
		return new(OpRetainEvent)
	case 298:
		return new(OpReleaseEvent)
	case 299:
		return new(OpCreateUserEvent)
	case 300:
		return new(OpIsValidEvent)
	case 301:
		return new(OpSetUserEventStatus)
	case 302:
		return new(OpCaptureEventProfilingInfo)
	case 303:
		return new(OpGetDefaultQueue)
	case 304:
		return new(OpBuildNDRange)
	case 305:
		return new(OpImageSparseSampleImplicitLod)
	case 306:
		return new(OpImageSparseSampleExplicitLod)
	case 307:
		return new(OpImageSparseSampleDrefImplicitLod)
	case 308:
		return new(OpImageSparseSampleDrefExplicitLod)
	case 309:
		return new(OpImageSparseSampleProjImplicitLod)
	case 310:
		return new(OpImageSparseSampleProjExplicitLod)
	case 311:
		return new(OpImageSparseSampleProjDrefImplicitLod)
	case 312:
		return new(OpImageSparseSampleProjDrefExplicitLod)
	case 313:
		return new(OpImageSparseFetch)
	case 314:
		return new(OpImageSparseGather)
	case 315:
		return new(OpImageSparseDrefGather)
	case 316:
		return new(OpImageSparseTexelsResident)
	case 317:
		return new(OpNoLine)
	case 318:
		return new(OpAtomicFlagTestAndSet)
	case 319:
		return new(OpAtomicFlagClear)
	case 320:
		return new(OpImageSparseRead)
	case 321:
		return new(OpSizeOf)
	case 322:
		return new(OpTypePipeStorage)
	case 323:
		return new(OpConstantPipeStorage)
	case 324:
		return new(OpCreatePipeFromPipeStorage)
	case 325:
		return new(OpGetKernelLocalSizeForSubgroupCount)
	case 326:
		return new(OpGetKernelMaxNumSubgroups)
	case 327:
		return new(OpTypeNamedBarrier)
	case 328:
		return new(OpNamedBarrierInitialize)
	case 329:
		return new(OpMemoryNamedBarrier)
	case 330:
		return new(OpModuleProcessed)
	case 331:
		return new(OpExecutionModeId)
	case 332:
		return new(OpDecorateId)
	case 333:
		return new(OpGroupNonUniformElect)
	case 334:
		return new(OpGroupNonUniformAll)
	case 335:
		return new(OpGroupNonUniformAny)
	case 336:
		return new(OpGroupNonUniformAllEqual)
	case 337:
		return new(OpGroupNonUniformBroadcast)
	case 338:
		return new(OpGroupNonUniformBroadcastFirst)
	case 339:
		return new(OpGroupNonUniformBallot)
	case 340:
		return new(OpGroupNonUniformInverseBallot)
	case 341:
		return new(OpGroupNonUniformBallotBitExtract)
	case 342:
		return new(OpGroupNonUniformBallotBitCount)
	case 343:
		return new(OpGroupNonUniformBallotFindLSB)
	case 344:
		return new(OpGroupNonUniformBallotFindMSB)
	case 345:
		return new(OpGroupNonUniformShuffle)
	case 346:
		return new(OpGroupNonUniformShuffleXor)
	case 347:
		return new(OpGroupNonUniformShuffleUp)
	case 348:
		return new(OpGroupNonUniformShuffleDown)
	case 349:
		return new(OpGroupNonUniformIAdd)
	case 350:
		return new(OpGroupNonUniformFAdd)
	case 351:
		return new(OpGroupNonUniformIMul)
	case 352:
		return new(OpGroupNonUniformFMul)
	case 353:
		return new(OpGroupNonUniformSMin)
	case 354:
		return new(OpGroupNonUniformUMin)
	case 355:
		return new(OpGroupNonUniformFMin)
	case 356:
		return new(OpGroupNonUniformSMax)
	case 357:
		return new(OpGroupNonUniformUMax)
	case 358:
		return new(OpGroupNonUniformFMax)
	case 359:
		return new(OpGroupNonUniformBitwiseAnd)
	case 360:
		return new(OpGroupNonUniformBitwiseOr)
	case 361:
		return new(OpGroupNonUniformBitwiseXor)
	case 362:
		return new(OpGroupNonUniformLogicalAnd)
	case 363:
		return new(OpGroupNonUniformLogicalOr)
	case 364:
		return new(OpGroupNonUniformLogicalXor)
	case 365:
		return new(OpGroupNonUniformQuadBroadcast)
	case 366:
		return new(OpGroupNonUniformQuadSwap)
	case 400:
		return new(OpCopyLogical)
	case 401:
		return new(OpPtrEqual)
	case 402:
		return new(OpPtrNotEqual)
	case 403:
		return new(OpPtrDiff)
	case 4416:
		return new(OpTerminateInvocation)
	case 4421:
		return new(OpSubgroupBallotKHR)
	case 4422:
		return new(OpSubgroupFirstInvocationKHR)
	case 4428:
		return new(OpSubgroupAllKHR)
	case 4429:
		return new(OpSubgroupAnyKHR)
	case 4430:
		return new(OpSubgroupAllEqualKHR)
	case 4431:
		return new(OpGroupNonUniformRotateKHR)
	case 4432:
		return new(OpSubgroupReadInvocationKHR)
	case 4445:
		return new(OpTraceRayKHR)
	case 4446:
		return new(OpExecuteCallableKHR)
	case 4447:
		return new(OpConvertUToAccelerationStructureKHR)
	case 4448:
		return new(OpIgnoreIntersectionKHR)
	case 4449:
		return new(OpTerminateRayKHR)
	case 4450:
		return new(OpSDot)
	case 4451:
		return new(OpUDot)
	case 4452:
		return new(OpSUDot)
	case 4453:
		return new(OpSDotAccSat)
	case 4454:
		return new(OpUDotAccSat)
	case 4455:
		return new(OpSUDotAccSat)
	case 4472:
		return new(OpTypeRayQueryKHR)
	case 4473:
		return new(OpRayQueryInitializeKHR)
	case 4474:
		return new(OpRayQueryTerminateKHR)
	case 4475:
		return new(OpRayQueryGenerateIntersectionKHR)
	case 4476:
		return new(OpRayQueryConfirmIntersectionKHR)
	case 4477:
		return new(OpRayQueryProceedKHR)
	case 4479:
		return new(OpRayQueryGetIntersectionTypeKHR)
	case 5000:
		return new(OpGroupIAddNonUniformAMD)
	case 5001:
		return new(OpGroupFAddNonUniformAMD)
	case 5002:
		return new(OpGroupFMinNonUniformAMD)
	case 5003:
		return new(OpGroupUMinNonUniformAMD)
	case 5004:
		return new(OpGroupSMinNonUniformAMD)
	case 5005:
		return new(OpGroupFMaxNonUniformAMD)
	case 5006:
		return new(OpGroupUMaxNonUniformAMD)
	case 5007:
		return new(OpGroupSMaxNonUniformAMD)
	case 5011:
		return new(OpFragmentMaskFetchAMD)
	case 5012:
		return new(OpFragmentFetchAMD)
	case 5056:
		return new(OpReadClockKHR)
	case 5283:
		return new(OpImageSampleFootprintNV)
	case 5296:
		return new(OpGroupNonUniformPartitionNV)
	case 5299:
		return new(OpWritePackedPrimitiveIndices4x8NV)
	case 5334:
		return new(OpReportIntersectionNV)
	case 5335:
		return new(OpIgnoreIntersectionNV)
	case 5336:
		return new(OpTerminateRayNV)
	case 5337:
		return new(OpTraceNV)
	case 5338:
		return new(OpTraceMotionNV)
	case 5339:
		return new(OpTraceRayMotionNV)
	case 5341:
		return new(OpTypeAccelerationStructureNV)
	case 5344:
		return new(OpExecuteCallableNV)
	case 5358:
		return new(OpTypeCooperativeMatrixNV)
	case 5359:
		return new(OpCooperativeMatrixLoadNV)
	case 5360:
		return new(OpCooperativeMatrixStoreNV)
	case 5361:
		return new(OpCooperativeMatrixMulAddNV)
	case 5362:
		return new(OpCooperativeMatrixLengthNV)
	case 5364:
		return new(OpBeginInvocationInterlockEXT)
	case 5365:
		return new(OpEndInvocationInterlockEXT)
	case 5380:
		return new(OpDemoteToHelperInvocation)
	case 5381:
		return new(OpIsHelperInvocationEXT)
	case 5391:
		return new(OpConvertUToImageNV)
	case 5392:
		return new(OpConvertUToSamplerNV)
	case 5393:
		return new(OpConvertImageToUNV)
	case 5394:
		return new(OpConvertSamplerToUNV)
	case 5395:
		return new(OpConvertUToSampledImageNV)
	case 5396:
		return new(OpConvertSampledImageToUNV)
	case 5397:
		return new(OpSamplerImageAddressingModeNV)
	case 5571:
		return new(OpSubgroupShuffleINTEL)
	case 5572:
		return new(OpSubgroupShuffleDownINTEL)
	case 5573:
		return new(OpSubgroupShuffleUpINTEL)
	case 5574:
		return new(OpSubgroupShuffleXorINTEL)
	case 5575:
		return new(OpSubgroupBlockReadINTEL)
	case 5576:
		return new(OpSubgroupBlockWriteINTEL)
	case 5577:
		return new(OpSubgroupImageBlockReadINTEL)
	case 5578:
		return new(OpSubgroupImageBlockWriteINTEL)
	case 5580:
		return new(OpSubgroupImageMediaBlockReadINTEL)
	case 5581:
		return new(OpSubgroupImageMediaBlockWriteINTEL)
	case 5585:
		return new(OpUCountLeadingZerosINTEL)
	case 5586:
		return new(OpUCountTrailingZerosINTEL)
	case 5587:
		return new(OpAbsISubINTEL)
	case 5588:
		return new(OpAbsUSubINTEL)
	case 5589:
		return new(OpIAddSatINTEL)
	case 5590:
		return new(OpUAddSatINTEL)
	case 5591:
		return new(OpIAverageINTEL)
	case 5592:
		return new(OpUAverageINTEL)
	case 5593:
		return new(OpIAverageRoundedINTEL)
	case 5594:
		return new(OpUAverageRoundedINTEL)
	case 5595:
		return new(OpISubSatINTEL)
	case 5596:
		return new(OpUSubSatINTEL)
	case 5597:
		return new(OpIMul32x16INTEL)
	case 5598:
		return new(OpUMul32x16INTEL)
	case 5614:
		return new(OpAtomicFMinEXT)
	case 5615:
		return new(OpAtomicFMaxEXT)
	case 5630:
		return new(OpAssumeTrueKHR)
	case 5631:
		return new(OpExpectKHR)
	case 5632:
		return new(OpDecorateString)
	case 5633:
		return new(OpMemberDecorateString)
	case 5887:
		return new(OpLoopControlINTEL)
	case 5946:
		return new(OpReadPipeBlockingINTEL)
	case 5947:
		return new(OpWritePipeBlockingINTEL)
	case 5949:
		return new(OpFPGARegINTEL)
	case 6016:
		return new(OpRayQueryGetRayTMinKHR)
	case 6017:
		return new(OpRayQueryGetRayFlagsKHR)
	case 6018:
		return new(OpRayQueryGetIntersectionTKHR)
	case 6019:
		return new(OpRayQueryGetIntersectionInstanceCustomIndexKHR)
	case 6020:
		return new(OpRayQueryGetIntersectionInstanceIdKHR)
	case 6021:
		return new(OpRayQueryGetIntersectionInstanceShaderBindingTableRecordOffsetKHR)
	case 6022:
		return new(OpRayQueryGetIntersectionGeometryIndexKHR)
	case 6023:
		return new(OpRayQueryGetIntersectionPrimitiveIndexKHR)
	case 6024:
		return new(OpRayQueryGetIntersectionBarycentricsKHR)
	case 6025:
		return new(OpRayQueryGetIntersectionFrontFaceKHR)
	case 6026:
		return new(OpRayQueryGetIntersectionCandidateAABBOpaqueKHR)
	case 6027:
		return new(OpRayQueryGetIntersectionObjectRayDirectionKHR)
	case 6028:
		return new(OpRayQueryGetIntersectionObjectRayOriginKHR)
	case 6029:
		return new(OpRayQueryGetWorldRayDirectionKHR)
	case 6030:
		return new(OpRayQueryGetWorldRayOriginKHR)
	case 6031:
		return new(OpRayQueryGetIntersectionObjectToWorldKHR)
	case 6032:
		return new(OpRayQueryGetIntersectionWorldToObjectKHR)
	case 6035:
		return new(OpAtomicFAddEXT)
	case 6086:
		return new(OpTypeBufferSurfaceINTEL)
	case 6090:
		return new(OpTypeStructContinuedINTEL)
	case 6091:
		return new(OpConstantCompositeContinuedINTEL)
	case 6092:
		return new(OpSpecConstantCompositeContinuedINTEL)
	case 6142:
		return new(OpControlBarrierArriveINTEL)
	case 6143:
		return new(OpControlBarrierWaitINTEL)
	case 6401:
		return new(OpGroupIMulKHR)
	case 6402:
		return new(OpGroupFMulKHR)
	case 6403:
		return new(OpGroupBitwiseAndKHR)
	case 6404:
		return new(OpGroupBitwiseOrKHR)
	case 6405:
		return new(OpGroupBitwiseXorKHR)
	case 6406:
		return new(OpGroupLogicalAndKHR)
	case 6407:
		return new(OpGroupLogicalOrKHR)
	case 6408:
		return new(OpGroupLogicalXorKHR)
	default:
		return nil
	}
}
