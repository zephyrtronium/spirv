package spirv

import (
	"encoding/binary"
	"errors"
	"io"

	"gitlab.com/zephyrtronium/spirv/spirvop"
)

// Read deserializes a SPIR-V module.
func Read(r io.Reader) (*Module, error) {
	var (
		m   Module
		scr scratch
	)
	order, err := scr.ReadHeader(r, &m)
	if err != nil {
		return nil, err
	}
	// The header is exactly 5 words.
	scr.W += 5
	for {
		d, op, err := scr.ReadInst(r, order)
		switch err {
		case nil: // do nothing
		case io.EOF:
			return &m, nil
		default:
			var serr Error
			if errors.As(err, &serr) {
				serr.setPosition(scr.I, scr.W)
			}
			return nil, err
		}
		if op == spirvop.OpExtInst {
			// We special-case OpExtInst decoding so that we can connect to
			// OpExtInstImport definitions we've seen.
			inst := new(OpExtInst)
			if err := scr.ScanExtInst(&m, inst, d); err != nil {
				var serr Error
				if errors.As(err, &serr) {
					serr.setPosition(scr.I, scr.W)
				}
				return nil, err
			}
			m.Add(inst)
			continue
		}
		inst := instForOpcode(op)
		if inst == nil {
			err := UnknownOpcodeError{
				InstPos: scr.I,
				WordPos: scr.W,
				Opcode:  op,
				Data:    append(([]uint32)(nil), d...),
			}
			return nil, &err
		}
		if err := scr.ScanInst(&m, inst, d); err != nil {
			var serr Error
			if errors.As(err, &serr) {
				serr.setPosition(scr.I, scr.W)
			}
			return nil, err
		}
		m.Add(inst)
		scr.W += len(d)
	}
}

// ReadHeader reads the SPIR-V header. It sets some fields of the module and
// returns the byte order that should be used for decoding.
func (scr *scratch) ReadHeader(r io.Reader, m *Module) (order binary.ByteOrder, err error) {
	scr.B = make([]byte, 4*5)
	if _, err := io.ReadFull(r, scr.B); err != nil {
		return nil, err
	}
	switch Magic {
	case binary.LittleEndian.Uint32(scr.B):
		order = binary.LittleEndian
	case binary.BigEndian.Uint32(scr.B):
		order = binary.BigEndian
	default:
		err := InvalidHeaderError{}
		copy(err.Magic[:], scr.B)
		return nil, &err
	}

	d := scr.Decode(order)
	ver := d[1]
	gen := d[2]
	bnd := d[3]
	res := d[4]
	maj, min := splitVersion(ver)
	switch {
	case maj == 0 && min == 0, bnd == 0, res != 0:
		err := InvalidHeaderError{
			Version:        ver,
			GeneratorMagic: gen,
			Bound:          bnd,
			Reserved:       res,
		}
		copy(err.Magic[:], scr.B[:4])
		return order, &err
	}

	m.maj, m.min = maj, min
	m.maxid = Id(bnd - 1)
	m.Magic = gen
	return order, nil
}

// ReadInst reads the data describing a single instruction and returns its
// opcode. The error is EOF only if no data could be read.
func (scr *scratch) ReadInst(r io.Reader, order binary.ByteOrder) ([]uint32, uint32, error) {
	scr.B = append(scr.B[:0], 0, 0, 0, 0)
	if _, err := io.ReadFull(r, scr.B); err != nil {
		return nil, 0, err
	}
	op, l := scr.Head(order)
	switch l {
	case 0:
		return nil, 0, &ZeroInstructionLengthError{Opcode: op}
	case 1:
		// Return early if there is nothing more to read so we don't get EOF
		// when we don't mean it.
		return scr.D, op, nil
	}
	b := scr.WordsBuf(l - 1)
	if _, err := io.ReadFull(r, b); err != nil {
		// TODO: wrap with info about what inst we were decoding
		if err == io.EOF {
			err = io.ErrUnexpectedEOF
		}
		return nil, 0, err
	}
	return scr.Decode(order), op, nil
}

// ScanInst initializes an instruction and its operands.
func (scr *scratch) ScanInst(m *Module, inst Instruction, p []uint32) error {
	if err := inst.Setup(p); err != nil {
		return err
	}
	scr.Ops = inst.Operands(scr.Ops[:0])
	return scr.ScanOperands(m, inst, p[1:])
}

// ScanExtInst initializes an OpExtInst and its operands, including the
// extension instruction.
func (scr *scratch) ScanExtInst(m *Module, inst *OpExtInst, p []uint32) error {
	// Set up the OpExtInst directly. We don't want to call inst.Setup because
	// that will create a potentially garbage RawExtInst.
	switch len(p) {
	case 0, 1, 2, 3:
		return &ShortInstructionError{Inst: inst, Operand: "Set"}
	case 4:
		return &ShortInstructionError{Inst: inst, Operand: "Instruction"}
	}
	inst.ResultType = IdResultType{Id(p[1])}
	inst.Result = Id(p[2])
	inst.Set = IdRef{Id(p[3])}
	// Check for an imported extension instruction set matching the inst.
	name, ok := m.extnames[inst.Set]
	if !ok {
		// No extension has been imported with this <id>.
		return &NoExtensionError{Inst: inst, Set: inst.Set}
	}
	set := extensions[string(name)]
	if set == nil {
		// The extension exists, but it hasn't been registered with the
		// package. To round-trip, we want a RawExtInst.
		e := RawExtInst{
			ExtName: name,
			Op:      LiteralExtInstInteger(p[4]),
			Words:   make([]Id, len(p)-5),
		}
		for i, v := range p[5:] {
			e.Words[i] = Id(v)
		}
		inst.Instruction = &e
		return nil
	}
	inst.Instruction = set(LiteralExtInstInteger(p[4]))
	if inst.Instruction == nil {
		// The extension instruction set doesn't recognize the opcode.
		return &NoExtensionInstructionError{
			Inst:   inst,
			Set:    name,
			Opcode: LiteralExtInstInteger(p[4]),
		}
	}
	// Scan extension instruction operands.
	if err := inst.Instruction.Setup(p[5:]); err != nil {
		return err
	}
	scr.Ops = inst.Instruction.Parameters(scr.Ops[:0])
	return scr.ScanOperands(m, inst, p[5:])
}

// ScanOperands initializes the operands of an instruction.
func (scr *scratch) ScanOperands(m *Module, inst Instruction, p []uint32) error {
	n := 0
	// Once scanned, each operand can add parameters. Loop with a counter
	// instead of a range to account for the changing size.
	for i := 0; i < len(scr.Ops); i++ {
		p = p[n:]
		// LiteralString operands are scanned during instruction setup so the
		// instruction can track its length while determining other operands.
		// We don't need to scan them twice, especially considering it may be
		// quite expensive to do so, so check for them and skip.
		if s, ok := scr.Ops[i].(*LiteralString); ok {
			n = int(s.Size())
			continue
		}
		var err error
		n, err = scr.Ops[i].Scan(m, inst, p)
		if err != nil {
			var serr Error
			if errors.As(err, &serr) {
				serr.setPosition(scr.I, scr.W)
			}
			return err
		}
		scr.Ops = scr.Ops[i].Parameters(scr.Ops)
	}
	return nil
}

// splitVersion splits a version value into major and minor components. If it
// is an invalid version, the results are both 0.
func splitVersion(ver uint32) (maj, min uint8) {
	if ver&0xff0000ff != 0 {
		return 0, 0
	}
	return uint8(ver >> 16), uint8(ver >> 8)
}

// Head decodes the first word of an instruction held in B, sets D to the slice
// containing that word, empties B, and returns the decoded instruction opcode
// and length.
func (scr *scratch) Head(order binary.ByteOrder) (op, l uint32) {
	// Increment the instruction counter first so the positions we report are
	// automatically 1-based.
	scr.I++
	var w uint32
	// Switch on order instead of using the interface so the method call can
	// be inlined and compiled as an intrinsic.
	switch order {
	case binary.LittleEndian:
		w = binary.LittleEndian.Uint32(scr.B)
	case binary.BigEndian:
		w = binary.BigEndian.Uint32(scr.B)
	default:
		w = order.Uint32(scr.B)
	}
	scr.B = scr.B[:0]
	scr.D = append(scr.D[:0], w)
	op, l = w&0xffff, w>>16
	return op, l
}

// Decode decodes and empties B and returns the result of appending it to D.
// Panics if the size of B is not a multiple of 4.
func (scr *scratch) Decode(order binary.ByteOrder) []uint32 {
	if len(scr.B)%4 != 0 {
		panic("spirv: bad scratch space size")
	}
	switch order {
	case binary.LittleEndian:
		for i := 0; i < len(scr.B); i += 4 {
			w := binary.LittleEndian.Uint32(scr.B[i:])
			scr.D = append(scr.D, w)
		}
	case binary.BigEndian:
		for i := 0; i < len(scr.B); i += 4 {
			w := binary.BigEndian.Uint32(scr.B[i:])
			scr.D = append(scr.D, w)
		}
	default:
		for i := 0; i < len(scr.B); i += 4 {
			w := order.Uint32(scr.B[i:])
			scr.D = append(scr.D, w)
		}
	}
	scr.B = scr.B[:0]
	return scr.D
}

// WordsBuf sets the length of B to hold n words, grows the capacity of D to
// hold n additional words, and returns B. The contents of B may or may not be
// overwritten.
func (scr *scratch) WordsBuf(n uint32) []byte {
	m := 4 * int(n)
	if cap(scr.B) >= m {
		scr.B = scr.B[:m]
	} else {
		scr.B = make([]byte, m)
	}
	if cap(scr.D)-len(scr.D) < int(n) {
		d := make([]uint32, len(scr.D), len(scr.D)+int(n))
		copy(d, scr.D)
		scr.D = d
	}
	return scr.B
}
