package spirv

import (
	"encoding/binary"
	"errors"
	"math"
	"strings"
)

// Literal types are written manually.

// LiteralInteger is a literal 32-bit integer.
type LiteralInteger int32

func (l *LiteralInteger) Encode(m *Module, n Instruction, p []uint32) []uint32 {
	return append(p, uint32(*l))
}

func (l *LiteralInteger) Scan(m *Module, n Instruction, p []uint32) (int, error) {
	if len(p) == 0 {
		err := ShortInstructionError{Inst: n, Operand: "LiteralInteger"}
		return 0, &err
	}
	*l = LiteralInteger(p[0])
	return 1, nil
}

func (*LiteralInteger) Parameters(p []Operand) []Operand {
	return p
}

// LiteralString is a string literal.
type LiteralString string

// Size returns the number of words required to serialize the string. Panics if
// the result overflows uint32.
func (l *LiteralString) Size() uint32 {
	sz := uint64(len(*l))/4 + 1
	if sz >= 1<<32 {
		panic("spirv: absurdly large string operand")
	}
	return uint32(sz)
}

func (l *LiteralString) Encode(m *Module, n Instruction, p []uint32) []uint32 {
	s := *l
	for len(s) >= 4 {
		x := binary.LittleEndian.Uint32([]byte(s[:4]))
		p = append(p, x)
		s = s[4:]
	}
	b := make([]byte, 4)
	copy(b, s)
	x := binary.LittleEndian.Uint32(b)
	return append(p, x)
}

// Scan decodes the value of the string from the sequence of words p.
//
// Unlike most operands, LiteralString skips scanning if it is not the empty
// string.
func (l *LiteralString) Scan(m *Module, n Instruction, p []uint32) (int, error) {
	if *l != "" {
		return int(l.Size()), nil
	}
	var b strings.Builder
	b.Grow(4 * len(p))
	for n, v := range p {
		for i := 0; i < 4; i++ {
			if v&0xff == 0 {
				*l = LiteralString(b.String())
				return n + 1, nil
			}
			b.WriteByte(byte(v))
			v >>= 8
		}
	}
	err := ShortInstructionError{Inst: n, Operand: "LiteralString"}
	return len(p), &err
}

func (*LiteralString) Parameters(p []Operand) []Operand {
	return p
}

// LiteralContextDependentNumber is a numeric literal the size of which is
// determined by the result type given in the instruction.
type LiteralContextDependentNumber struct {
	b uint64
}

// UnsignedConstant creates a LiteralContextDependentNumber with the value in
// x assuming the size of the integer is b bits. Panics if b is not 8, 16, 32,
// or 64.
func UnsignedConstant(x uint64, b int) LiteralContextDependentNumber {
	switch b {
	case 8:
		return LiteralContextDependentNumber{b: x & 0xff}
	case 16:
		return LiteralContextDependentNumber{b: x & 0xffff}
	case 32:
		return LiteralContextDependentNumber{b: x & 0xffffffff}
	case 64:
		return LiteralContextDependentNumber{b: x}
	default:
		panic(errors.New("spirv: invalid bit size for LiteralContextDependentNumber"))
	}
}

// SignedConstant creates a LiteralContextDependentNumber with the value in
// x assuming the size of the integer is b bits. Panics if b is not 8, 16, 32,
// or 64.
func SignedConstant(x int64, b int) LiteralContextDependentNumber {
	switch b {
	case 8:
		return LiteralContextDependentNumber{b: uint64(int64(int8(x)))}
	case 16:
		return LiteralContextDependentNumber{b: uint64(int64(int16(x)))}
	case 32:
		return LiteralContextDependentNumber{b: uint64(int64(int32(x)))}
	case 64:
		return LiteralContextDependentNumber{b: uint64(x)}
	default:
		panic(errors.New("spirv: invalid bit size for LiteralContextDependentNumber"))
	}
}

// FloatConstant creates a LiteralContextDependentNumber with the value in x
// assuming the size of the float is b bits. Panics if b is not 16, 32, or 64.
//
// When b is 16, FloatConstant converts x to a IEEE 754 binary16 format
// according to the following rules (where x^y represents exponentiation):
//
//  - If x is smaller in magnitude than 2^-14, it is treated as +0.
//  - If x is larger in magnitude than 65504, it is treated as an infinity of
//    the same sign.
//  - If x is NaN, it is converted to a qNaN.
//  - Otherwise, x is rounded toward zero to the precision of binary16.
//
// If these rules are not sufficient, one may instead generate the desired bit
// pattern and pass it to UnsignedConstant.
func FloatConstant(x float64, b int) LiteralContextDependentNumber {
	switch b {
	case 16:
		switch {
		case x >= 0 && x < 0x1p-14, x <= 0 && x > -0x1p-14:
			return LiteralContextDependentNumber{}
		case x > 65504:
			return LiteralContextDependentNumber{b: 0b0_11111_0000000000}
		case x < -65504:
			return LiteralContextDependentNumber{b: 0b1_11111_0000000000}
		case x != x:
			return LiteralContextDependentNumber{b: 0b0_11111_0111111111}
		}
		u := math.Float64bits(x)
		s := u >> 63
		e := u>>52&(1<<11-1) - 1023 + 15
		m := u >> 42 & (1<<10 - 1)
		return LiteralContextDependentNumber{b: (s << 15) | (e & 0b11111 << 10) | m}
	case 32:
		return LiteralContextDependentNumber{b: uint64(math.Float32bits(float32(x)))}
	case 64:
		return LiteralContextDependentNumber{b: math.Float64bits(x)}
	default:
		panic(errors.New("spirv: invalid bit size for LiteralContextDependentNumber"))
	}
}

func (l *LiteralContextDependentNumber) Encode(m *Module, n Instruction, p []uint32) []uint32 {
	var s int32
	switch n := n.(type) {
	case *OpConstant:
		s = m.litSize[n.ResultType.id]
	case *OpSpecConstant:
		s = m.litSize[n.ResultType.id]
	default:
		panic(errors.New("spirv: LiteralContextDependentNumber with unknown Instruction type"))
	}
	p = append(p, uint32(l.b))
	if s == 64 {
		p = append(p, uint32(l.b>>32))
	}
	return p
}

func (l *LiteralContextDependentNumber) Scan(m *Module, n Instruction, p []uint32) (int, error) {
	s := 1
	switch n := n.(type) {
	case *OpConstant:
		if m.litSize[n.ResultType.id] == 64 {
			s = 2
		}
	case *OpSpecConstant:
		if m.litSize[n.ResultType.id] == 64 {
			s = 2
		}
	default:
		panic(errors.New("spirv: LiteralContextDependentNumber with unknown Instruction type"))
	}
	if len(p) < s {
		err := ShortInstructionError{Inst: n, Operand: "LiteralContextDependentNumber"}
		return 0, &err
	}
	l.b = uint64(p[0])
	if s == 2 {
		l.b |= uint64(p[1]) << 32
	}
	return s, nil
}

func (*LiteralContextDependentNumber) Parameters(p []Operand) []Operand {
	return p
}

// LiteralExtInstInteger is a numeric literal indicating an extension
// instruction opcode for OpExtInst.
type LiteralExtInstInteger uint32

func (l *LiteralExtInstInteger) Encode(m *Module, n Instruction, p []uint32) []uint32 {
	return append(p, uint32(*l))
}

func (l *LiteralExtInstInteger) Scan(m *Module, n Instruction, p []uint32) (int, error) {
	if len(p) == 0 {
		err := ShortInstructionError{Inst: n, Operand: "LiteralExtInstInteger"}
		return 0, &err
	}
	*l = LiteralExtInstInteger(p[0])
	return 1, nil
}

func (*LiteralExtInstInteger) Parameters(p []Operand) []Operand {
	return p
}

// LiteralSpecConstantOpInteger is the opcode of a constant instruction used as
// the argument to OpSpecConstantOp.
type LiteralSpecConstantOpInteger uint32

func (l *LiteralSpecConstantOpInteger) Encode(m *Module, n Instruction, p []uint32) []uint32 {
	return append(p, uint32(*l))
}

func (l *LiteralSpecConstantOpInteger) Scan(m *Module, n Instruction, p []uint32) (int, error) {
	if len(p) == 0 {
		err := ShortInstructionError{Inst: n, Operand: "LiteralSpecConstantOpInteger"}
		return 0, &err
	}
	*l = LiteralSpecConstantOpInteger(p[0])
	return 1, nil
}

func (*LiteralSpecConstantOpInteger) Parameters(p []Operand) []Operand {
	return p
}

var (
	_ Operand = (*LiteralInteger)(nil)
	_ Operand = (*LiteralString)(nil)
	_ Operand = (*LiteralContextDependentNumber)(nil)
	_ Operand = (*LiteralExtInstInteger)(nil)
	_ Operand = (*LiteralSpecConstantOpInteger)(nil)
)
