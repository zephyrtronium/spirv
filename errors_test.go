package spirv_test

import (
	"errors"
	"reflect"
	"testing"

	"gitlab.com/zephyrtronium/spirv"
)

// eqerrs returns true if e1 == e2 or if e1 and e2 are errors from package
// spirv with equal content. Instructions in errors need only be the same type.
func eqerrs(t *testing.T, e1, e2 error) bool {
	t.Helper()
	if e1 == e2 {
		return true
	}
	if reflect.TypeOf(e1) != reflect.TypeOf(e2) {
		return false
	}
	var s1, s2 spirv.Error
	if errors.As(e1, &s1) {
		errors.As(e2, &s2)
		i1, w1 := s1.Position()
		i2, w2 := s2.Position()
		if i1 != i2 || w1 != w2 {
			return false
		}
	}
	switch e1 := e1.(type) {
	case *spirv.InstructionTooLargeError:
		e2 := e2.(*spirv.InstructionTooLargeError)
		return reflect.TypeOf(e1.Inst) == reflect.TypeOf(e2.Inst)
	case *spirv.InvalidEnumerantError:
		e2 := e2.(*spirv.InvalidEnumerantError)
		return reflect.TypeOf(e1.Inst) == reflect.TypeOf(e2.Inst) &&
			e1.Enum == e2.Enum &&
			e1.Value == e2.Value
	case *spirv.InvalidHeaderError:
		e2 := e2.(*spirv.InvalidHeaderError)
		return *e1 == *e2
	case *spirv.ShortInstructionError:
		e2 := e2.(*spirv.ShortInstructionError)
		return reflect.TypeOf(e1.Inst) == reflect.TypeOf(e2.Inst) &&
			e1.Operand == e2.Operand
	case *spirv.UnknownOpcodeError:
		e2 := e2.(*spirv.UnknownOpcodeError)
		return reflect.DeepEqual(e1, e2)
	case *spirv.ZeroInstructionLengthError:
		e2 := e2.(*spirv.ZeroInstructionLengthError)
		return *e1 == *e2
	case *spirv.NoExtensionError:
		e2 := e2.(*spirv.NoExtensionError)
		return e1.Set == e2.Set
	case *spirv.NoExtensionInstructionError:
		e2 := e2.(*spirv.NoExtensionInstructionError)
		return e1.Set == e2.Set && e1.Opcode == e2.Opcode
	}
	return false
}
