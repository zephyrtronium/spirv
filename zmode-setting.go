// Copyright (c) 2014-2020 The Khronos Group Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and/or associated documentation files (the "Materials"),
// to deal in the Materials without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Materials, and to permit persons to whom the
// Materials are furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Materials.
//
// MODIFICATIONS TO THIS FILE MAY MEAN IT NO LONGER ACCURATELY REFLECTS KHRONOS
// STANDARDS. THE UNMODIFIED, NORMATIVE VERSIONS OF KHRONOS SPECIFICATIONS AND
// HEADER INFORMATION ARE LOCATED AT https://www.khronos.org/registry/
//
// THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM,OUT OF OR IN CONNECTION WITH THE MATERIALS OR THE USE OR OTHER DEALINGS
// IN THE MATERIALS.

package spirv

// Code generated by spirv/generate; DO NOT EDIT

// Mode-Setting Instructions

// OpMemoryModel is in class Mode-Setting.
type OpMemoryModel struct {
	Operand0 AddressingModel
	Operand1 MemoryModel
}

var _ Instruction = (*OpMemoryModel)(nil)

// String returns "OpMemoryModel".
func (op *OpMemoryModel) String() string {
	return "OpMemoryModel"
}

// Opcode returns 14.
func (op *OpMemoryModel) Opcode() uint32 {
	return 14
}

func (op *OpMemoryModel) ResultID() (id IdRef) {
	return id
}

func (op *OpMemoryModel) Setup(p []uint32) error {
	k := 1
	if k >= len(p) {
		return &ShortInstructionError{
			Inst:    op,
			Operand: "Operand0",
		}
	}
	op.Operand0 = AddressingModelEnumerant(p[k])
	if op.Operand0 == nil {
		return &InvalidEnumerantError{
			Inst:  op,
			Enum:  "AddressingModel",
			Value: p[k],
		}
	}
	k++
	if k >= len(p) {
		return &ShortInstructionError{
			Inst:    op,
			Operand: "Operand1",
		}
	}
	op.Operand1 = MemoryModelEnumerant(p[k])
	if op.Operand1 == nil {
		return &InvalidEnumerantError{
			Inst:  op,
			Enum:  "MemoryModel",
			Value: p[k],
		}
	}
	k++
	return nil
}

func (op *OpMemoryModel) Operands(p []Operand) []Operand {
	p = append(p, op.Operand0)
	p = append(p, op.Operand1)
	return p
}

// OpEntryPoint is in class Mode-Setting.
type OpEntryPoint struct {
	Operand0   ExecutionModel
	EntryPoint IdRef
	Name       LiteralString
	Interface  []IdRef
}

var _ Instruction = (*OpEntryPoint)(nil)

// String returns "OpEntryPoint".
func (op *OpEntryPoint) String() string {
	return "OpEntryPoint"
}

// Opcode returns 15.
func (op *OpEntryPoint) Opcode() uint32 {
	return 15
}

func (op *OpEntryPoint) ResultID() (id IdRef) {
	return id
}

func (op *OpEntryPoint) Setup(p []uint32) error {
	k := 1
	if k >= len(p) {
		return &ShortInstructionError{
			Inst:    op,
			Operand: "Operand0",
		}
	}
	op.Operand0 = ExecutionModelEnumerant(p[k])
	if op.Operand0 == nil {
		return &InvalidEnumerantError{
			Inst:  op,
			Enum:  "ExecutionModel",
			Value: p[k],
		}
	}
	k++
	if k >= len(p) {
		return &ShortInstructionError{
			Inst:    op,
			Operand: "EntryPoint",
		}
	}
	k++
	if k >= len(p) {
		return &ShortInstructionError{
			Inst:    op,
			Operand: "Name",
		}
	}
	{
		n, err := op.Name.Scan(nil, op, p[k:])
		if err != nil {
			return err
		}
		k += n
	}
	if k > len(p) {
		return &ShortInstructionError{
			Inst:    op,
			Operand: "Interface",
		}
	}
	op.Interface = make([]IdRef, len(p)-k)
	return nil
}

func (op *OpEntryPoint) Operands(p []Operand) []Operand {
	p = append(p, op.Operand0)
	p = append(p, &op.EntryPoint)
	p = append(p, &op.Name)
	for i := range op.Interface {
		p = append(p, &op.Interface[i])
	}
	return p
}

// OpExecutionMode is in class Mode-Setting.
type OpExecutionMode struct {
	EntryPoint IdRef
	Mode       ExecutionMode
}

var _ Instruction = (*OpExecutionMode)(nil)

// String returns "OpExecutionMode".
func (op *OpExecutionMode) String() string {
	return "OpExecutionMode"
}

// Opcode returns 16.
func (op *OpExecutionMode) Opcode() uint32 {
	return 16
}

func (op *OpExecutionMode) ResultID() (id IdRef) {
	return id
}

func (op *OpExecutionMode) Setup(p []uint32) error {
	k := 1
	if k >= len(p) {
		return &ShortInstructionError{
			Inst:    op,
			Operand: "EntryPoint",
		}
	}
	k++
	if k >= len(p) {
		return &ShortInstructionError{
			Inst:    op,
			Operand: "Mode",
		}
	}
	op.Mode = ExecutionModeEnumerant(p[k])
	if op.Mode == nil {
		return &InvalidEnumerantError{
			Inst:  op,
			Enum:  "ExecutionMode",
			Value: p[k],
		}
	}
	k++
	return nil
}

func (op *OpExecutionMode) Operands(p []Operand) []Operand {
	p = append(p, &op.EntryPoint)
	p = append(p, op.Mode)
	return p
}

// OpCapability is in class Mode-Setting.
type OpCapability struct {
	Capability Capability
}

var _ Instruction = (*OpCapability)(nil)

// String returns "OpCapability".
func (op *OpCapability) String() string {
	return "OpCapability"
}

// Opcode returns 17.
func (op *OpCapability) Opcode() uint32 {
	return 17
}

func (op *OpCapability) ResultID() (id IdRef) {
	return id
}

func (op *OpCapability) Setup(p []uint32) error {
	k := 1
	if k >= len(p) {
		return &ShortInstructionError{
			Inst:    op,
			Operand: "Capability",
		}
	}
	op.Capability = CapabilityEnumerant(p[k])
	if op.Capability == nil {
		return &InvalidEnumerantError{
			Inst:  op,
			Enum:  "Capability",
			Value: p[k],
		}
	}
	k++
	return nil
}

func (op *OpCapability) Operands(p []Operand) []Operand {
	p = append(p, op.Capability)
	return p
}

// OpExecutionModeId is in class Mode-Setting.
//
// Missing before SPIR-V 1.2
type OpExecutionModeId struct {
	EntryPoint IdRef
	Mode       ExecutionMode
}

var _ Instruction = (*OpExecutionModeId)(nil)

// String returns "OpExecutionModeId".
func (op *OpExecutionModeId) String() string {
	return "OpExecutionModeId"
}

// Opcode returns 331.
func (op *OpExecutionModeId) Opcode() uint32 {
	return 331
}

func (op *OpExecutionModeId) ResultID() (id IdRef) {
	return id
}

func (op *OpExecutionModeId) Setup(p []uint32) error {
	k := 1
	if k >= len(p) {
		return &ShortInstructionError{
			Inst:    op,
			Operand: "EntryPoint",
		}
	}
	k++
	if k >= len(p) {
		return &ShortInstructionError{
			Inst:    op,
			Operand: "Mode",
		}
	}
	op.Mode = ExecutionModeEnumerant(p[k])
	if op.Mode == nil {
		return &InvalidEnumerantError{
			Inst:  op,
			Enum:  "ExecutionMode",
			Value: p[k],
		}
	}
	k++
	return nil
}

func (op *OpExecutionModeId) Operands(p []Operand) []Operand {
	p = append(p, &op.EntryPoint)
	p = append(p, op.Mode)
	return p
}
