package spirv_test

import (
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/zephyrtronium/spirv"
)

func TestValidate(t *testing.T) {
	// complete is roughly a complete and mostly-correct SPIR-V module.
	complete := []spirv.Instruction{
		&spirv.OpCapability{Capability: spirv.CapabilityKernel},
		&spirv.OpExtension{Name: "test"},
		&spirv.OpExtInstImport{Result: 1, Name: "Validate.test.100"},
		&spirv.OpMemoryModel{Operand0: spirv.AddressingModelLogical, Operand1: spirv.MemoryModelSimple},
		&spirv.OpEntryPoint{Operand0: spirv.ExecutionModelKernel, EntryPoint: spirv.Ref(2), Name: "test", Interface: spirv.Refs(3)},
		&spirv.OpExecutionMode{EntryPoint: spirv.Ref(2), Mode: &spirv.ExecutionModeLocalSize{Param0: 1, Param1: 1, Param2: 1}},
		&spirv.OpString{Result: 4, Str: "module_test_validate.go"},
		&spirv.OpName{Target: spirv.Ref(3), Name: "invocation"},
		&spirv.OpModuleProcessed{Process: "manually writing out test cases"},
		&spirv.OpDecorate{Target: spirv.Ref(3), Operand1: &spirv.DecorationBuiltIn{Param0: spirv.BuiltInGlobalLinearId}},
		&spirv.OpTypeVoid{Result: 9},
		&spirv.OpTypeInt{Result: 5, Width: 32, Signedness: 0},
		&spirv.OpTypeFunction{Result: 8, ReturnType: spirv.Ref(9)},
		&spirv.OpConstant{ResultType: spirv.TypeRef(5), Result: 6, Value: spirv.UnsignedConstant(0, 32)},
		&spirv.OpLine{File: spirv.Ref(4), Line: 1, Column: 1},
		&spirv.OpVariable{ResultType: spirv.TypeRef(5), Result: 3, Operand2: spirv.StorageClassInput},
		&spirv.OpNoLine{},
		&spirv.OpFunction{ResultType: spirv.TypeRef(9), Result: 7, Operand2: spirv.FunctionControl{}, FunctionType: spirv.Ref(8)},
		&spirv.OpFunctionEnd{},
		&spirv.OpLine{File: spirv.Ref(4), Line: 2, Column: 1},
		&spirv.OpFunction{ResultType: spirv.TypeRef(9), Result: 10, Operand2: spirv.FunctionControl{}, FunctionType: spirv.Ref(8)},
		&spirv.OpLabel{Result: 11},
		&spirv.OpLine{File: spirv.Ref(4), Line: 3, Column: 1},
		&spirv.OpExtInst{ResultType: spirv.TypeRef(9), Result: 12, Set: spirv.Ref(1), Instruction: &spirv.RawExtInst{ExtName: "Validate.test.100", Op: 1, Words: []spirv.Id{3}}},
		&spirv.OpFunctionEnd{},
	}
	cases := []struct {
		name string
		opts *spirv.ValidateOptions
		in   []spirv.Instruction
		out  []spirv.Instruction
	}{
		{
			name: "nil",
			opts: nil,
			in:   complete,
			out:  complete,
		},
		{
			name: "zero",
			opts: new(spirv.ValidateOptions),
			in:   complete,
			out:  complete,
		},
		{
			name: "capabilities",
			opts: &spirv.ValidateOptions{SkipCapabilities: true},
			in:   complete,
			out:  filterTypes(complete, new(spirv.OpCapability)),
		},
		{
			name: "extensions",
			opts: &spirv.ValidateOptions{SkipExtensions: true},
			in:   complete,
			out:  filterTypes(complete, new(spirv.OpExtension)),
		},
		{
			name: "imports",
			opts: &spirv.ValidateOptions{SkipExtInstImports: true},
			in:   complete,
			out:  filterTypes(complete, new(spirv.OpExtInstImport)),
		},
		{
			name: "mem-model",
			opts: &spirv.ValidateOptions{SkipMemoryModel: true},
			in:   complete,
			out:  filterTypes(complete, new(spirv.OpMemoryModel)),
		},
		{
			name: "entry-points",
			opts: &spirv.ValidateOptions{SkipEntryPoints: true},
			in:   complete,
			out:  filterTypes(complete, new(spirv.OpEntryPoint)),
		},
		{
			name: "exec-mode",
			opts: &spirv.ValidateOptions{SkipExecutionModes: true},
			in:   complete,
			out:  filterTypes(complete, new(spirv.OpExecutionMode)),
		},
		{
			name: "strings",
			opts: &spirv.ValidateOptions{SkipStrings: true},
			in:   complete,
			out:  filterTypes(complete, new(spirv.OpString), new(spirv.OpSource), new(spirv.OpSourceContinued), new(spirv.OpSourceExtension)),
		},
		{
			name: "names",
			opts: &spirv.ValidateOptions{SkipNames: true},
			in:   complete,
			out:  filterTypes(complete, new(spirv.OpName), new(spirv.OpMemberName)),
		},
		{
			name: "processes",
			opts: &spirv.ValidateOptions{SkipModuleProcessed: true},
			in:   complete,
			out:  filterTypes(complete, new(spirv.OpModuleProcessed)),
		},
		{
			name: "debug",
			opts: &spirv.ValidateOptions{SkipDebug: true},
			in:   complete,
			out: filterTypes(complete,
				new(spirv.OpString), new(spirv.OpSource), new(spirv.OpSourceContinued), new(spirv.OpSourceExtension),
				new(spirv.OpName), new(spirv.OpMemberName),
				new(spirv.OpModuleProcessed),
			),
		},
		{
			name: "annotation",
			opts: &spirv.ValidateOptions{SkipAnnotations: true},
			in:   complete,
			out: filterTypes(complete,
				new(spirv.OpDecorate), new(spirv.OpMemberDecorate), new(spirv.OpDecorateId),
				new(spirv.OpDecorationGroup), new(spirv.OpGroupDecorate), new(spirv.OpGroupMemberDecorate),
				new(spirv.OpDecorateString), new(spirv.OpDecorateStringGOOGLE),
				new(spirv.OpMemberDecorateString), new(spirv.OpMemberDecorateStringGOOGLE),
			),
		},
		{
			name: "decls",
			opts: &spirv.ValidateOptions{SkipDeclarations: true},
			in:   complete,
			out:  filterRange(complete, new(spirv.OpTypeVoid), new(spirv.OpFunction)),
		},
		{
			name: "func-decls",
			opts: &spirv.ValidateOptions{SkipFuncDecls: true},
			in:   complete,
			out:  filterRange(complete, new(spirv.OpFunction), new(spirv.OpLine)),
		},
		{
			name: "func-defs",
			opts: &spirv.ValidateOptions{SkipFunctions: true},
			in:   complete,
			out:  complete[:19],
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			var m spirv.Module
			for _, inst := range c.in {
				m.Add(inst)
			}
			var seen []spirv.Instruction
			v := func(inst spirv.Instruction) error {
				seen = append(seen, inst)
				return nil
			}
			err := m.Validate(spirv.ValidatorFunc(v), c.opts)
			if err != nil {
				t.Error(err)
			}
			if !reflect.DeepEqual(seen, c.out) {
				t.Errorf("wrong output:\nwant %#v\ngot  %#v", c.out, seen)
			}
		})
	}
}

func filterResults(insts []spirv.Instruction, defining ...spirv.Id) []spirv.Instruction {
	var r []spirv.Instruction
	contains := func(inst spirv.Instruction, ids []spirv.Id) bool {
		id := inst.ResultID()
		for _, v := range ids {
			if id == v.Ref() {
				return true
			}
		}
		return false
	}
	for _, inst := range insts {
		if !contains(inst, defining) {
			r = append(r, inst)
		}
	}
	return r
}

func filterTypes(insts []spirv.Instruction, types ...spirv.Instruction) []spirv.Instruction {
	var r []spirv.Instruction
	contains := func(inst spirv.Instruction, types []spirv.Instruction) bool {
		t := reflect.TypeOf(inst)
		for _, v := range types {
			if t == reflect.TypeOf(v) {
				return true
			}
		}
		return false
	}
	for _, inst := range insts {
		if !contains(inst, types) {
			r = append(r, inst)
		}
	}
	return r
}

// filterRange removes instructions from insts starting with the first
// instruction with the same type as from and continuing until and not
// including the first subsequent instruction with the same type as to. Panics
// if no instruction matches either operand.
func filterRange(insts []spirv.Instruction, from spirv.Instruction, to spirv.Instruction) []spirv.Instruction {
	var r []spirv.Instruction
	k := -1
	for i, inst := range insts {
		if reflect.TypeOf(inst) == reflect.TypeOf(from) {
			k = i
			break
		}
	}
	r = append(r, insts[:k]...)
	for i, inst := range insts[k:] {
		if reflect.TypeOf(inst) == reflect.TypeOf(to) {
			r = append(r, insts[k+i:]...)
			return r
		}
	}
	panic(fmt.Errorf("spirv_test: no inst in %+v matches %+v", insts, to))
}
