package main

import (
	"context"
	"flag"
	"log"

	// import for module requirement
	_ "golang.org/x/sync/errgroup"
)

func main() {
	var clean bool
	flag.BoolVar(&clean, "clean", false, "remove files that would be generated")
	flag.Parse()

	ctx := context.Background()
	spirs := spirlist()
	m := make(map[pkg]*SPIRV, len(spirs))
	for _, name := range spirs {
		pkg := pkgname(name)
		spir, err := loadspir(name)
		if err != nil {
			log.Fatal(err)
		}
		m[pkg] = spir
	}
	r := newRender(m, clean)
	for pkg, spir := range m {
		if err := r.grammar(ctx, spir, pkg); err != nil {
			log.Fatal(err)
		}
	}
	if err := r.close(); err != nil {
		log.Fatal(err)
	}
}
