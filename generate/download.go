//go:build ignore
// +build ignore

// download downloads the SPIR-V grammar files.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"

	"golang.org/x/sync/errgroup"
)

const kgbase = "https://github.com/KhronosGroup/SPIRV-Headers/raw/master/include/spirv/unified1/"
const libase = "https://github.com/KhronosGroup/SPIRV-Headers/raw/master/LICENSE"

func main() {
	var base, to, list, liurl string
	flag.StringVar(&base, "base", kgbase, "base URL of directory with API specifications")
	flag.StringVar(&to, "to", "grammar", "output directory, created if needed and CLEARED")
	flag.StringVar(&list, "list", "list.json", "grammar list")
	flag.StringVar(&liurl, "license", libase, "URL of license to retrieve")
	flag.Parse()
	grammars := flag.Args()

	if err := os.MkdirAll(to, 0644); err != nil {
		log.Fatalf("could not create directory %s: %v", to, err)
	}

	var eg errgroup.Group
	for _, g := range grammars {
		out := filepath.Join(to, g)
		url := base + "/" + g
		eg.Go(func() error { return wget(out, url) })
	}
	{
		out := filepath.Join(to, path.Base(liurl))
		eg.Go(func() error { return wget(out, liurl) })
	}
	err := eg.Wait()
	if err != nil {
		log.Fatal(err)
	}

	b, err := json.Marshal(grammars)
	if err != nil {
		log.Fatal(err)
	}
	if err := os.WriteFile(filepath.Join(to, list), b, 0644); err != nil {
		log.Fatal(err)
	}
}

func wget(out, url string) error {
	f, err := os.Create(out)
	if err != nil {
		return fmt.Errorf("error creating output file %s: %w", out, err)
	}
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("error getting %s: %w", url, err)
	}
	defer resp.Body.Close()
	_, err = io.Copy(f, resp.Body)
	return err
}
