package main

import (
	"embed"
	"encoding/json"
	"fmt"
	"path"
)

//go:generate go run download.go spirv.core.grammar.json extinst.debuginfo.grammar.json extinst.glsl.std.450.grammar.json extinst.nonsemantic.clspvreflection.grammar.json extinst.nonsemantic.debugprintf.grammar.json extinst.nonsemantic.shader.debuginfo.100.grammar.json extinst.opencl.debuginfo.100.grammar.json extinst.opencl.std.100.grammar.json extinst.spv-amd-gcn-shader.grammar.json extinst.spv-amd-shader-ballot.grammar.json extinst.spv-amd-shader-explicit-vertex-parameter.grammar.json extinst.spv-amd-shader-trinary-minmax.grammar.json

// Types generated by https://mholt.github.io/json-to-go/ plus adjustments.

type SPIRV struct {
	Copyright                []string        `json:"copyright"`
	MagicNumber              string          `json:"magic_number"`
	MajorVersion             int             `json:"major_version"`
	MinorVersion             int             `json:"minor_version"`
	Version                  int             `json:"version"` // used by extensions
	Revision                 int             `json:"revision"`
	InstructionPrintingClass []PrintingClass `json:"instruction_printing_class"`
	Instructions             []Instruction   `json:"instructions"`
	OperandKinds             []OperandKind   `json:"operand_kinds"`
}

type PrintingClass struct {
	Tag     string `json:"tag"`
	Heading string `json:"heading,omitempty"`
}

type Operand struct {
	Kind       string `json:"kind"`
	Name       string `json:"name"`
	Quantifier string `json:"quantifier"`
}

type Instruction struct {
	Opname       string    `json:"opname"`
	Class        string    `json:"class"`
	Opcode       int       `json:"opcode"`
	Operands     []Operand `json:"operands,omitempty"`
	Capabilities []string  `json:"capabilities,omitempty"`
	Capability   string    `json:"capability,omitempty"`
	LastVersion  string    `json:"lastVersion,omitempty"`
	Version      string    `json:"version,omitempty"`
	Extensions   []string  `json:"extensions,omitempty"`
}

type Enumerant struct {
	Enumerant    string      `json:"enumerant"`
	Value        interface{} `json:"value"`
	Capabilities []string    `json:"capabilities,omitempty"`
	Parameters   []Operand   `json:"parameters,omitempty"`
	Version      string      `json:"version,omitempty"`
	LastVersion  string      `json:"lastVersion,omitempty"`
	Extensions   []string    `json:"extensions,omitempty"`
}

type OperandKind struct {
	Category   string      `json:"category"`
	Kind       string      `json:"kind"`
	Enumerants []Enumerant `json:"enumerants,omitempty"`
	Doc        string      `json:"doc,omitempty"`
	Bases      []string    `json:"bases,omitempty"`
}

//go:embed grammar/*.json
var grammars embed.FS

func spirlist() []string {
	var r []string
	b, err := grammars.ReadFile("grammar/list.json")
	if err != nil {
		panic(fmt.Errorf("couldn't read grammar list: %w", err))
	}
	if err := json.Unmarshal(b, &r); err != nil {
		panic(fmt.Errorf("couldn't decode grammar list: %w", err))
	}
	return r
}

func loadspir(name string) (*SPIRV, error) {
	var sp SPIRV
	g, err := grammars.Open(path.Join("grammar", name))
	if err != nil {
		return nil, fmt.Errorf("couldn't open grammar %s: %w", name, err)
	}
	defer g.Close()
	dec := json.NewDecoder(g)
	dec.DisallowUnknownFields()
	if err := dec.Decode(&sp); err != nil {
		return nil, fmt.Errorf("couldn't decode grammar %s: %w", name, err)
	}
	return &sp, nil
}
