{{define "extinst"}}{{with .Inst}}{{$name := instname .}}
// {{$name}} is an extension instruction.{{with .Capabilities}}
//
// Enabling capabilities: {{printf "%s " .}}{{end}}{{with .Extensions}}
//
// Requires extensions: {{printf "%s " .}}{{end}}
type {{$name}} struct {
	{{- range $i, $op := .Operands}}
	{{- $mn := mbname $op.Name}}
	{{- if eq $op.Kind "IdResultType"}}
	ResultType spirv.IdResultType {{- with $op.Name}} // {{dename .}}{{end}}
	{{- else if eq $op.Kind "IdResult"}}
	Result spirv.Id {{- with $op.Name}} // {{dename .}}{{end}}
	{{- else if eq $op.Quantifier ""}}
	{{- if $mn}}
	{{$mn}} {{if index $.CoreTypes $op.Kind}}spirv.{{end}}{{enname $op}}
	{{- else}}
	Operand{{$i}} {{if index $.CoreTypes $op.Kind}}spirv.{{end}}{{enname $op}} {{- with $op.Name}} // {{dename .}}{{end}}
	{{- end}}
	{{- else if eq $op.Quantifier "?"}}
	{{- if $mn}}
	{{$mn}} {{if not (anyelem $op.Kind $.ValueEnums $.CoreValues)}}*{{end}}{{if index $.CoreTypes $op.Kind}}spirv.{{end}}{{enname $op}} `spirv:"?"`
	{{- else}}
	Operand{{$i}} {{if not (anyelem $op.Kind $.ValueEnums $.CoreValues)}}*{{end}}{{if index $.CoreTypes $op.Kind}}spirv.{{end}}{{enname $op}} `spirv:"?"` {{- with $op.Name}} // {{dename .}}{{end}}
	{{- end}}
	{{- else if eq $op.Quantifier "*"}}
	{{- if $mn}}
	{{$mn}} []{{if index $.CoreTypes $op.Kind}}spirv.{{end}}{{enname $op}}
	{{- else}}
	Operand{{$i}} []{{if index $.CoreTypes $op.Kind}}spirv.{{end}}{{enname $op}} {{- with $op.Name}} // {{dename .}}{{end}}
	{{- end}}
	{{- else}}
	$$ unknown quantifier {{$op.Quantifier}} $$
	{{- end}}
	{{- end}}
}

{{/* Ensure the inst implements ExtInst */ -}}
var _ spirv.ExtInst = (*{{$name}})(nil)

func (*{{$name}}) String() string {
    return {{printf "%q" .Opname}}
}

func (*{{$name}}) Extension() string {
    return {{printf "%q" $.Extension}}
}

func (*{{$name}}) Opcode() spirv.LiteralExtInstInteger {
    return {{.Opcode}}
}

func (op *{{$name}}) Setup(p []uint32) error {
	{{- with .Operands}}
	k := 0
	{{- range $i, $op := .}}
	{{- $mn := or (mbname $op.Name) (printf "Operand%d" $i)}}
	{{- if eq $op.Kind "IdResultType" "IdResult"}}
		{{- /* There shouldn't be any of these, since they are in the */}}
		{{- /* enclosing OpExtInst instead. I am largely copying this code */}}
		{{- /* from the inst template, though, and it's easy to include. */}}
	k++
	{{- else if eq $op.Kind "LiteralString"}}
		{{- /* See the LiteralString case in the core inst template. */}}
	if k >= len(p) {
		return &spirv.ShortInstructionError{
			{{- /* TODO(zeph): there's no reasonable instruction available */}}
			Operand: {{printf "%q" $mn}},
		}
	}
		{{- if eq $op.Quantifier "?"}}
	op.{{$mn}} = new(spirv.LiteralString)
		{{- end}}
	{
		n, err := op.{{$mn}}.Scan(nil, nil, p[k:])
		if err != nil {
			return nil
		}
		k += n
	}
	{{- else if eq $op.Quantifier "" "?"}}
	if k >= len(p) {
		{{- if eq $op.Quantifier ""}}
		return &spirv.ShortInstructionError{
			Operand: {{printf "%q" $mn}},
		}
		{{- else}}
		return nil
		{{- end}}
	}
		{{- if index $.ValueEnums $op.Kind}}
	op.{{$mn}} = {{enname $op.Kind}}Enumerant(p[k])
	if op.{{$mn}} == nil {
		return &spirv.InvalidEnumerantError{
			Enum:  {{printf "%q" $op.Kind}},
			Value: p[k],
		}
	}
		{{- else if index $.CoreValues $op.Kind}}
	op.{{$mn}} = spirv.{{enname $op.Kind}}Enumerant(p[k])
	if op.{{$mn}} == nil {
		return &spirv.InvalidEnumerantError{
			Enum:  {{printf "%q" $op.Kind}},
			Value: p[k],
		}
	}
		{{- else if eq $op.Quantifier "?"}}
	op.{{$mn}} = new({{if index $.CoreTypes $op.Kind}}spirv.{{end}}{{enname $op}})
		{{- end}}
	k++
	{{- else if eq $op.Quantifier "*"}}
	if k > len(p) {
		return &spirv.ShortInstructionError{
			Operand: {{printf "%q" $mn}},
		}
	}
	op.{{$mn}} = make([]{{if index $.CoreTypes $op.Kind}}spirv.{{end}}{{enname $op}}, len(p)-k)
	{{- end}}
	{{- end}}
	{{- end}}
    return nil
}

func (op *{{$name}}) Encode(m *spirv.Module, n spirv.Instruction, p []uint32) []uint32 {
    return append(p, {{.Opcode}})
}

func (op *{{$name}}) Scan(m *spirv.Module, n spirv.Instruction, p []uint32) (int, error) {
	return 0, nil
}

func (op *{{$name}}) Parameters(p []spirv.Operand) []spirv.Operand {
    {{- range $i, $op := .Operands}}
	{{- $mn := or (mbname $op.Name) (printf "Operand%d" $i)}}
	{{- if eq $op.Quantifier ""}}
	p = append(p, {{if not (anyelem $op.Kind $.ValueEnums $.CoreValues)}}&{{end}}op.{{$mn}})
	{{- else if eq $op.Quantifier "?"}}
	if op.{{$mn}} != nil {
		p = append(p, op.{{$mn}})
	}
	{{- else if eq $op.Quantifier "*"}}
	for i := range op.{{$mn}} {
		p = append(p, {{if not (anyelem $op.Kind $.ValueEnums $.CoreValues)}}&{{end}}op.{{$mn}}[i])
	}
	{{- end}}
	{{- end}}
	return p
}
{{end}}
{{- end}}

{{define "extinstlist"}}
{{range .Copyright -}}
// {{.}}
{{end}}
package {{.Package}}

// Code generated by spirv/generate; DO NOT EDIT

import "gitlab.com/zephyrtronium/spirv"

func instForOpcode(opcode spirv.LiteralExtInstInteger) spirv.ExtInst {
    switch opcode {
    {{- range $inst := $.Insts}}
    case {{$inst.Opcode}}:
        return new({{instname $inst}})
    {{- end}}
    default:
        return nil
    }
}

func init() {
    spirv.Register({{printf "%q" $.Extension}}, instForOpcode)
}
{{- end}}
