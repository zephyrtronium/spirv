package main

import (
	"context"
	"embed"
	"errors"
	"fmt"
	"go/token"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"text/template"
	"unicode"
	"unicode/utf8"
)

type render struct {
	// base is the output directory for files, which is also package spirv.
	base string
	// insts is a per-package mapping of instruction printing classes to
	// output writers.
	insts map[string]map[string]io.WriteCloser
	// comps is the per-package output location for composite types.
	comps map[string]io.WriteCloser
	// valueEnums is the per-package set of value enumeration types.
	valueEnums map[string]map[string]bool
	// bitEnums is the per-package set of bit enumeration types.
	bitEnums map[string]map[string]bool
	// enumerants is the per-package list of all enumerants.
	enumerants map[string]map[string][]Enumerant
	// coretypes is the set of all types in the core SPIR-V grammar.
	coretypes map[string]bool
	// corevals is the set of all value enumerations in the core.
	corevals map[string]bool
	// clean is whether to remove files that would be generated instead of
	// creating them.
	clean bool
}

func newRender(spirs map[pkg]*SPIRV, clean bool) render {
	v := make(map[string]map[string]bool)
	b := make(map[string]map[string]bool)
	e := make(map[string]map[string][]Enumerant)
	c := make(map[string]bool)
	var cv map[string]bool
	// As a first pass, categorize enums and record their enumerants.
	for pkg, spir := range spirs {
		v[pkg.File] = make(map[string]bool)
		b[pkg.File] = make(map[string]bool)
		e[pkg.File] = make(map[string][]Enumerant)
		for _, opkind := range spir.OperandKinds {
			if pkg.Ext == "" {
				c[opkind.Kind] = true
			}
			switch opkind.Category {
			case "ValueEnum":
				v[pkg.File][opkind.Kind] = true
				e[pkg.File][opkind.Kind] = deduped(opkind.Enumerants)
			case "BitEnum":
				b[pkg.File][opkind.Kind] = true
				e[pkg.File][opkind.Kind] = deduped(opkind.Enumerants)
			}
		}
		if pkg.Ext == "" {
			cv = v[pkg.File]
		}
	}
	return render{
		base:       ".",
		insts:      make(map[string]map[string]io.WriteCloser),
		comps:      make(map[string]io.WriteCloser),
		valueEnums: v,
		bitEnums:   b,
		enumerants: e,
		coretypes:  c,
		corevals:   cv,
		clean:      clean,
	}
}

func (r *render) grammar(ctx context.Context, spir *SPIRV, pkg pkg) error {
	if err := r.initPkg(ctx, pkg); err != nil {
		return err
	}
	if err := r.version(ctx, spir, pkg); err != nil {
		return err
	}
	if len(spir.InstructionPrintingClass) == 0 {
		// Extension instruction sets usually don't list printing classes.
		// Make a fake one.
		spir.InstructionPrintingClass = []PrintingClass{{}}
	}
	for _, cls := range spir.InstructionPrintingClass {
		if err := r.class(ctx, spir, pkg, cls); err != nil {
			return err
		}
	}
	for _, inst := range spir.Instructions {
		if err := r.inst(ctx, pkg, inst); err != nil {
			return err
		}
	}
	if err := r.instlist(ctx, spir, pkg, dedupedInsts(spir.Instructions)); err != nil {
		return err
	}
	if err := r.opspkg(ctx, spir, pkg, spir.Instructions); err != nil {
		return err
	}
	for _, opkind := range spir.OperandKinds {
		switch opkind.Category {
		case "ValueEnum":
			if err := r.valenum(ctx, spir, pkg, opkind); err != nil {
				return err
			}
		case "BitEnum":
			if err := r.bitenum(ctx, spir, pkg, opkind); err != nil {
				return err
			}
		case "Composite":
			if err := r.comp(ctx, spir, pkg, opkind); err != nil {
				return err
			}
		case "Literal", "Id":
			// Literals and <id>s are implemented manually. Do nothing.
		default:
			return fmt.Errorf("unhandled operand category in %+v", opkind)
		}
	}
	return nil
}

func (r *render) initPkg(ctx context.Context, pkg pkg) error {
	if pkg.Ext == "" {
		return nil
	}
	dir := filepath.Join(r.base, "ext", pkg.Name)
	if r.clean {
		return nil
	}
	err := os.MkdirAll(dir, 0777)
	if err != nil {
		return fmt.Errorf("couldn't create directory %s for package %q: %w", dir, pkg, err)
	}
	return nil
}

func (r *render) version(ctx context.Context, spir *SPIRV, pkg pkg) error {
	// The versionfile template both sets up the file and completes its
	// contents, unlike the templates for most other output types.
	var nm string
	if pkg.Ext != "" {
		nm = filepath.Join(r.base, "ext", pkg.Name, "zversion.go")
	} else {
		nm = filepath.Join(r.base, "zversion.go")
	}
	if r.clean {
		err := os.Remove(nm)
		if err != nil && !errors.Is(err, fs.ErrNotExist) {
			return err
		}
		return nil
	}
	f, err := os.Create(nm)
	if err != nil {
		return fmt.Errorf("couldn't create %s for package %q: %w", nm, pkg, err)
	}
	data := struct {
		Package      string
		Copyright    []string
		MajorVersion int
		MinorVersion int
		Version      int
		Revision     int
		MagicNumber  string
	}{
		pkg.Name,
		spir.Copyright,
		spir.MajorVersion,
		spir.MinorVersion,
		spir.Version,
		spir.Revision,
		spir.MagicNumber,
	}
	if err := templates.ExecuteTemplate(f, "versionfile", &data); err != nil {
		return fmt.Errorf("couldn't run template versionfile for package %q: %w", pkg, err)
	}
	return nil
}

func (r *render) class(ctx context.Context, spir *SPIRV, pkg pkg, cls PrintingClass) error {
	if cls.Tag == "@exclude" {
		return nil
	}
	// Ensure the package has a map.
	p := r.insts[pkg.File]
	if p == nil {
		p = make(map[string]io.WriteCloser)
		r.insts[pkg.File] = p
	}
	// Create an output file.
	nm := "z" + strings.ToLower(cls.Tag)
	if pkg.Ext != "" {
		nm = filepath.Join(r.base, "ext", pkg.Name, nm+".go")
	} else {
		nm = filepath.Join(r.base, nm+".go")
	}
	// Or, if we're cleaning, remove it instead.
	if r.clean {
		err := os.Remove(nm)
		if err != nil && !errors.Is(err, fs.ErrNotExist) {
			return err
		}
		return nil
	}
	f, err := os.Create(nm)
	if err != nil {
		return fmt.Errorf("couldn't create %s for package %q class %q: %w", nm, pkg, cls, err)
	}
	p[cls.Tag] = f
	// If the context has closed, don't bother running templates.
	if ctx.Err() != nil {
		return ctx.Err()
	}
	// Run the initializer template.
	data := struct {
		Package   string
		Heading   string
		Copyright []string
	}{
		pkg.Name,
		cls.Heading,
		spir.Copyright,
	}
	if err := templates.ExecuteTemplate(f, "instfile", &data); err != nil {
		return fmt.Errorf("couldn't run template instfile for package %q class %q: %w", pkg, cls, err)
	}
	return nil
}

func (r *render) inst(ctx context.Context, pkg pkg, inst Instruction) error {
	if r.clean {
		// We don't have anything to do if we're cleaning.
		return nil
	}
	if inst.Class == "@exclude" {
		return nil
	}
	if inst.Opname == "OpExtInst" {
		// OpExtInst is implemented manually because it has a special operand.
		return nil
	}
	// Some extensions use a "capability" field rather than "capabilities" to
	// list an enabling capability.
	if inst.Capability != "" {
		inst.Capabilities = append(inst.Capabilities, inst.Capability)
	}
	tmpl := "inst"
	if pkg.Ext != "" {
		tmpl = "extinst"
	}
	if ctx.Err() != nil {
		return ctx.Err()
	}
	w := r.insts[pkg.File][inst.Class]
	data := struct {
		Extension  string
		Inst       Instruction
		ValueEnums map[string]bool
		BitEnums   map[string]bool
		Enumerants map[string][]Enumerant
		CoreTypes  map[string]bool
		CoreValues map[string]bool
	}{
		Extension:  pkg.Ext,
		Inst:       inst,
		ValueEnums: r.valueEnums[pkg.File],
		BitEnums:   r.bitEnums[pkg.File],
		Enumerants: r.enumerants[pkg.File],
		CoreTypes:  r.coretypes,
		CoreValues: r.corevals,
	}
	if err := templates.ExecuteTemplate(w, tmpl, data); err != nil {
		return fmt.Errorf("couldn't run template %s for package %q inst %+v: %w", tmpl, pkg, inst, err)
	}
	return nil
}

func (r *render) instlist(ctx context.Context, spir *SPIRV, pkg pkg, insts []Instruction) error {
	// The instlist template both sets up the file and completes its contents,
	// unlike the templates for most other output types.
	var nm string
	if pkg.Ext != "" {
		nm = filepath.Join(r.base, "ext", pkg.Name, "zinstructions.go")
	} else {
		nm = filepath.Join(r.base, "zinstructions.go")
	}
	if r.clean {
		err := os.Remove(nm)
		if err != nil && !errors.Is(err, fs.ErrNotExist) {
			return err
		}
		return nil
	}
	f, err := os.Create(nm)
	if err != nil {
		return fmt.Errorf("couldn't create %s for package %q: %w", nm, pkg, err)
	}
	tmpl := "instlist"
	if pkg.Ext != "" {
		tmpl = "extinstlist"
	}
	data := struct {
		Package   string
		Extension string
		Copyright []string
		Insts     []Instruction
	}{
		pkg.Name,
		pkg.Ext,
		spir.Copyright,
		insts,
	}
	if err := templates.ExecuteTemplate(f, tmpl, &data); err != nil {
		return fmt.Errorf("couldn't run template %s for package %q: %w", tmpl, pkg, err)
	}
	return nil
}

func (r *render) opspkg(ctx context.Context, spir *SPIRV, pkg pkg, insts []Instruction) error {
	// The opspkg template both sets up the file and completes its contents.
	var dir string
	if pkg.Ext != "" {
		dir = filepath.Join(r.base, "ext", pkg.Name, pkg.Name+"op")
	} else {
		dir = filepath.Join(r.base, pkg.Name+"op")
	}
	nm := filepath.Join(dir, "zopcode.go")
	if r.clean {
		err := os.Remove(nm)
		return err
	}
	err := os.MkdirAll(dir, 0777)
	if err != nil {
		return fmt.Errorf("couldn't create directory %s for package %q: %w", dir, pkg, err)
	}
	f, err := os.Create(nm)
	if err != nil {
		return fmt.Errorf("couldn't create zopcode.go for package %q: %w", pkg, err)
	}
	data := struct {
		Package   string
		Copyright []string
		Insts     []Instruction
	}{
		pkg.Name,
		spir.Copyright,
		insts,
	}
	if err := templates.ExecuteTemplate(f, "opspkg", &data); err != nil {
		return fmt.Errorf("couldn't run template opspkg for package %q: %w", pkg, err)
	}
	return nil
}

func (r *render) initEnum(ctx context.Context, spir *SPIRV, pkg pkg, kind string) (io.WriteCloser, error) {
	var nm string
	kind = strings.ToLower(kind)
	if pkg.Ext != "" {
		nm = filepath.Join(r.base, "ext", pkg.Name, "zenum_"+kind+".go")
	} else {
		nm = filepath.Join(r.base, "zenum_"+kind+".go")
	}
	if r.clean {
		err := os.Remove(nm)
		if err != nil && !errors.Is(err, fs.ErrNotExist) {
			return nil, err
		}
		return nil, nil
	}
	f, err := os.Create(nm)
	if err != nil {
		return nil, fmt.Errorf("couldn't create %s for package %q: %w", nm, pkg, err)
	}
	data := struct {
		Package   string
		Copyright []string
	}{
		pkg.Name,
		spir.Copyright,
	}
	if err := templates.ExecuteTemplate(f, "enumfile", &data); err != nil {
		return nil, fmt.Errorf("couldn't run template enumfile for package %q: %w", pkg, err)
	}
	return f, nil
}

func (r *render) valenum(ctx context.Context, spir *SPIRV, pkg pkg, enum OperandKind) error {
	w, err := r.initEnum(ctx, spir, pkg, enum.Kind)
	if err != nil {
		return err
	}
	if r.clean {
		return nil
	}
	if ctx.Err() != nil {
		return ctx.Err()
	}
	data := struct {
		Ext        string
		Enum       OperandKind
		Values     []Enumerant
		ValueEnums map[string]bool
		Enumerants map[string][]Enumerant
		CoreTypes  map[string]bool
		CoreValues map[string]bool
	}{
		Ext:        pkg.Ext,
		Enum:       enum,
		Values:     deduped(enum.Enumerants),
		ValueEnums: r.valueEnums[pkg.File],
		Enumerants: r.enumerants[pkg.File],
		CoreTypes:  r.coretypes,
		CoreValues: r.corevals,
	}
	if err := templates.ExecuteTemplate(w, "valenum", data); err != nil {
		return fmt.Errorf("couldn't run template valenum for package %q enum %q: %w", pkg, enum.Kind, err)
	}
	return nil
}

func (r *render) bitenum(ctx context.Context, spir *SPIRV, pkg pkg, enum OperandKind) error {
	w, err := r.initEnum(ctx, spir, pkg, enum.Kind)
	if err != nil {
		return err
	}
	if r.clean {
		return nil
	}
	if ctx.Err() != nil {
		return ctx.Err()
	}
	data := struct {
		Ext        string
		Enum       OperandKind
		ValueEnums map[string]bool
		CoreTypes  map[string]bool
		CoreValues map[string]bool
	}{
		Ext:        pkg.Ext,
		Enum:       enum,
		ValueEnums: r.valueEnums[pkg.File],
		CoreTypes:  r.coretypes,
		CoreValues: r.corevals,
	}
	if err := templates.ExecuteTemplate(w, "bitenum", data); err != nil {
		return fmt.Errorf("couldn't run template bitenum for package %q enum %q: %w", pkg, enum.Kind, err)
	}
	return nil
}

func (r *render) initComp(ctx context.Context, spir *SPIRV, pkg pkg) (io.WriteCloser, error) {
	var nm string
	if pkg.Ext != "" {
		nm = filepath.Join(r.base, "ext", pkg.Name, "zopcomp.go")
	} else {
		nm = filepath.Join(r.base, "zopcomp.go")
	}
	if r.clean {
		err := os.Remove(nm)
		if err != nil && !errors.Is(err, fs.ErrNotExist) {
			return nil, err
		}
		return nil, nil
	}
	f, err := os.Create(nm)
	if err != nil {
		return nil, fmt.Errorf("couldn't create %s for package %q: %w", nm, pkg, err)
	}
	r.comps[pkg.File] = f
	data := struct {
		Package   string
		Copyright []string
	}{
		pkg.Name,
		spir.Copyright,
	}
	if err := templates.ExecuteTemplate(f, "compfile", &data); err != nil {
		return nil, fmt.Errorf("couldn't run template compfile for package %q: %w", pkg, err)
	}
	return f, nil
}

func (r *render) comp(ctx context.Context, spir *SPIRV, pkg pkg, comp OperandKind) error {
	w := r.comps[pkg.File]
	if w == nil {
		var err error
		w, err = r.initComp(ctx, spir, pkg)
		if err != nil {
			return err
		}
	}
	if r.clean {
		return nil
	}
	if ctx.Err() != nil {
		return ctx.Err()
	}
	data := struct {
		Comp       OperandKind
		ValueEnums map[string]bool
	}{
		Comp:       comp,
		ValueEnums: r.valueEnums[pkg.File],
	}
	if err := templates.ExecuteTemplate(w, "comp", data); err != nil {
		return fmt.Errorf("couldn't run template comp for package %q type %q: %w", pkg, comp.Kind, err)
	}
	return nil
}

func (r *render) close() error {
	for pkg, p := range r.insts {
		for f, w := range p {
			if err := w.Close(); err != nil {
				return fmt.Errorf("couldn't close %s in package %s: %w", f, pkg, err)
			}
		}
	}
	for pkg, w := range r.comps {
		if err := w.Close(); err != nil {
			return fmt.Errorf("couldn't close comps for package %s: %w", pkg, err)
		}
	}
	return nil
}

// deduped makes a copy of an enumerants list with consecutive duplicates
// removed.
func deduped(en []Enumerant) []Enumerant {
	if len(en) == 0 {
		return nil
	}
	r := make([]Enumerant, 0, len(en))
	r = append(r, en[0])
	for i := 1; i < len(en); i++ {
		if en[i].Value != en[i-1].Value {
			r = append(r, en[i])
		}
	}
	return r
}

// dedupedInsts makes a copy of an instructions list with consecutive duplicate
// opcodes removed. Also removes instructions with @exclude class.
func dedupedInsts(ops []Instruction) []Instruction {
	if len(ops) == 0 {
		return nil
	}
	r := make([]Instruction, 0, len(ops))
	r = append(r, ops[0])
	for i := 1; i < len(ops); i++ {
		if ops[i].Opcode != ops[i-1].Opcode && ops[i].Class != "@exclude" {
			r = append(r, ops[i])
		}
	}
	return r
}

// pkg is a package name.
type pkg struct {
	// name is the package name, like "spirv" or "glsl".
	Name string
	// file is the file name from which the package was created, like
	// "spirv.core.grammar.json" or "extinst.glsl.std.450.grammar.json".
	File string
	// ext is the extension name. For the core grammar, it is the empty
	// string. Otherwise, it is like "glsl.std.450".
	Ext string
	// nonsem is true if the package is an extension instruction set with
	// "nonsemantic." in the name.
	Nonsem bool
}

// pkgname gets a reasonable package name based on the name of a SPIR-V
// grammar JSON.
func pkgname(name string) pkg {
	pkg := pkg{File: filepath.Base(name)}
	// Grammar names are written like name.spec.100.grammar.json.
	fields := strings.Split(strings.ToLower(name), ".")
	nm := fields[0]
	// Skip the words "extinst" and "nonsemantic." They're informative, but
	// not appropriate for a package name.
	for {
		switch nm {
		case "nonsemantic":
			pkg.Nonsem = true
			fallthrough
		case "extinst":
			fields = fields[1:]
			nm = fields[0]
			continue
		}
		break
	}
	// If this isn't the core grammar, then the extension name is the remaining
	// fields less ".grammar.json".
	if nm != "spirv" {
		ext := strings.Join(fields, ".")
		ext = strings.TrimSuffix(ext, ".json")
		ext = strings.TrimSuffix(ext, ".grammar")
		pkg.Ext = ext
	}
	// A number of grammars are named like spv-amd-gcn-shader. Skip the "spv"
	// part (vendor is ok) and get rid of the hyphens.
	if strings.HasPrefix(nm, "spv-") {
		nm = nm[4:]
	}
	nm = strings.ReplaceAll(nm, "-", "")
	// If the subsequent word doesn't indicate a default spec, or no spec,
	// include it in the package name. This distinguishes e.g. opencl.std.100
	// from opencl.debuginfo.100.
	// Also, as a special case, shorten "debuginfo" to just "dbg". Note that
	// there is an extinst.debuginfo.grammar.json; that will still come out to
	// "debuginfo" because we skip "extinst" before this.
	switch s := fields[1]; s {
	case "std", "core", "grammar":
		// do nothing
	case "debuginfo":
		nm += "dbg"
	default:
		if 'a' <= s[0] && s[0] <= 'z' {
			nm += s
		}
	}
	// Done.
	pkg.Name = nm
	return pkg
}

// String returns the extension name if the package is an extension, or
// "SPIR-V" if it is the SPIR-V core grammar.
func (pkg pkg) String() string {
	if pkg.Ext != "" {
		return pkg.Ext
	}
	return "SPIR-V"
}

//go:embed templates
var templateFS embed.FS
var templates = func() *template.Template {
	t := template.New("spirv")
	funcs := template.FuncMap{
		"enname":   enname,
		"instname": instname,
		"dename":   dename,
		"mbname":   mbname,
		"anyelem":  anyelem,
	}
	t, err := t.Funcs(funcs).ParseFS(templateFS, "templates/*.go.tmpl")
	if err != nil {
		panic(err)
	}
	return t
}()

// enname gets the name to use for the type of an Operand or OperandKind.
func enname(en interface{}) string {
	// TODO(zeph): need to detect value enums and distinguish core vs ext types
	var s string
	switch en := en.(type) {
	case Operand:
		s = en.Kind
	case OperandKind:
		s = en.Kind
	case string:
		s = en
	default:
		panic(fmt.Errorf("generate: enname called on bad type: %#v", en))
	}
	return s
}

// anyelem returns whether the given name is an element of any of the sets.
func anyelem(name string, sets ...map[string]bool) bool {
	for _, set := range sets {
		if set[name] {
			return true
		}
	}
	return false
}

// instname gets the name to use for the type of an Instruction.
func instname(in Instruction) string {
	s := in.Opname
	// Some extension instruction sets use snake_case, which breaks exporting
	// and occasionally conflicts with Go keywords. Even though we'll prepend
	// Op in all these cases (currently), it will look nicer if the name is
	// CamelCase.
	words := strings.Split(s, "_")
	for i, word := range words {
		r, n := utf8.DecodeRuneInString(word)
		word = string(unicode.ToUpper(r)) + word[n:]
		words[i] = word
	}
	s = strings.Join(words, "")
	// Extension instructions generally don't start with Op like core
	// instructions do, which causes naming conflicts in several extensions.
	// As an easy approach to fix this, just prepend Op if it's missing.
	if !strings.HasPrefix(s, "Op") {
		return "Op" + s
	}
	return s
}

// dename rewrites operand names to be more suitable for comments.
func dename(s string) string {
	s = strings.ReplaceAll(s, "'", "")
	s = strings.ReplaceAll(s, "\n", "")
	return s
}

// mbname returns the operand name made suitable for use as a field name, or
// the empty string if no such transformation is possible.
func mbname(s string) string {
	s = dename(s)
	switch s {
	case "D~ref~":
		return "Dref"
	case "Opcode":
		return "Op"
	case "String":
		return "Str"
	case "Result":
		return "Res"
	case "Parameters":
		return "Params"
	case "The name of the opaque type.":
		return "Name"
	}
	s = strings.ReplaceAll(s, " ", "")
	if len(s) == 0 {
		return ""
	}
	r, n := utf8.DecodeRuneInString(s)
	if n == len(s) {
		s = string(unicode.ToUpper(r))
	}
	if !token.IsIdentifier(s) || !token.IsExported(s) {
		return ""
	}
	return s
}
