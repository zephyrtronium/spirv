package spirv_test

import (
	"bytes"
	"embed"
	"testing"

	"gitlab.com/zephyrtronium/spirv"
)

func TestLabelMovesDecl(t *testing.T) {
	var m spirv.Module
	ops := []spirv.Instruction{
		&spirv.OpFunction{
			ResultType:   spirv.TypeRef(1),
			Result:       3,
			Operand2:     spirv.FunctionControl{},
			FunctionType: spirv.Ref(2),
		},
		&spirv.OpFunctionEnd{},
		&spirv.OpFunction{
			ResultType:   spirv.TypeRef(1),
			Result:       4,
			Operand2:     spirv.FunctionControl{},
			FunctionType: spirv.Ref(2),
		},
	}
	for _, op := range ops {
		m.Add(op)
	}
	if len(m.Funcs()) != 0 {
		t.Errorf("instructions prematurely added to funcs: %v", m.Funcs())
	}
	op := &spirv.OpLabel{Result: 5}
	m.Add(op)
	if len(m.Funcs()) != 2 {
		t.Errorf("wrong instructions moved to funcs: %v", m.Funcs())
	}
	if len(m.FuncDecls()) != 2 {
		t.Errorf("wrong instructions moved from funcDecls: %v", m.FuncDecls())
	}
}

//go:embed testdata
var testmodules embed.FS

func TestModuleRoundTrip(t *testing.T) {
	infos, err := testmodules.ReadDir("testdata/valid")
	if err != nil {
		t.Fatal(err)
	}
	for _, info := range infos {
		t.Run(info.Name(), func(t *testing.T) {
			b, err := testmodules.ReadFile("testdata/valid/" + info.Name())
			if err != nil {
				t.Fatal(err)
			}
			m, err := spirv.Read(bytes.NewReader(b))
			if err != nil {
				t.Fatal("couldn't read module:", err)
			}
			var o bytes.Buffer
			_, err = m.WriteTo(&o)
			if err != nil {
				t.Error("couldn't write module:", err)
			}
			if !bytes.Equal(b, o.Bytes()) {
				t.Error("round-trip failed")
			}
		})
	}
}

func TestTypeInst(t *testing.T) {
	var m spirv.Module
	ops := []spirv.Instruction{
		&spirv.OpString{
			Result: 1,
			Str:    "",
		},
		&spirv.OpTypeInt{
			Result:     2,
			Width:      32,
			Signedness: 0,
		},
		&spirv.OpConstant{
			ResultType: spirv.TypeRef(1),
			Result:     3,
			Value:      spirv.UnsignedConstant(0, 32),
		},
	}
	for _, op := range ops {
		m.Add(op)
	}

	cases := []struct {
		name string
		id   spirv.Id
		want spirv.Instruction
	}{
		{"non-global", 1, nil},
		{"type", 2, ops[1]},
		{"const", 3, nil},
		{"nowhere", 4, nil},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			got := m.TypeInst(spirv.TypeRef(c.id))
			if c.want != got {
				t.Errorf("found wrong inst: want %#v, got %#v", c.want, got)
			}
		})
	}
}
