package spirv

import (
	"encoding/binary"
	"errors"
	"io"

	"gitlab.com/zephyrtronium/spirv/spirvop"
)

// encodeStep is an encoding portion for a module.
type encodeStep func(io.Writer, *Module, *scratch) (int64, error)

// WriteTo serializes the module and writes it to w.
func (m *Module) WriteTo(w io.Writer) (n int64, err error) {
	if err := m.canSerialize(); err != nil {
		return 0, err
	}
	steps := []encodeStep{
		encodeHeader,
		encodeCaps,
		encodeExts,
		encodeImports,
		encodeMemModel,
		encodeEntryPoints,
		encodeInsts(m.execModes),
		encodeInsts(m.strings),
		encodeInsts(m.names),
		encodeProcesses,
		encodeInsts(m.annotations),
		encodeInsts(m.globl),
		encodeInsts(m.funcDecls),
		encodeInsts(m.funcs),
	}
	var scr scratch
	for _, step := range steps {
		r, err := step(w, m, &scr)
		n += r
		if err != nil {
			// Set the error position, if applicable.
			var serr Error
			if errors.As(err, &serr) {
				serr.setPosition(scr.I, scr.W)
			}
			return n, err
		}
	}
	return n, nil
}

// canSerialize ensures the module has enough information to serialize.
func (m *Module) canSerialize() error {
	if m.memModel == nil {
		return errors.New("no memory model")
	}
	return nil
}

func encodeHeader(w io.Writer, m *Module, scr *scratch) (n int64, err error) {
	// Physical layout of a SPIR-V module begins:
	//  1. Magic number 0x07230203. Endianism is inferred from this.
	//  2. Version number in the layout 0x00MMmm00, where M is major version.
	//  3. Generator magic number.
	//  4. Bound on <id>s.
	//  5. 0.
	maj, min := m.Version()
	scr.D = []uint32{
		Magic,
		uint32(maj)<<16 | uint32(min)<<8,
		m.Magic,
		uint32(m.Bound()),
		0,
	}
	r, err := w.Write(scr.Convert())
	return int64(r), err
}

func encodeCaps(w io.Writer, m *Module, scr *scratch) (n int64, err error) {
	// OpCapability. No enumerants have parameters, so we know the size is 2.
	var v uint32 = spirvop.OpCapability | 2<<16
	scr.D = append(scr.D[:0], v)
	// We can cheat a bit here to avoid allocations: we know that only
	// LiteralContextDependentNumber actually uses the Instruction parameter
	// to Encode, and we know that only OpConstant and OpSpecConstant use
	// LiteralContextDependentNumber. So, if we aren't in a place where those
	// instructions can appear, we can pass nil for the instruction whenever
	// there isn't an already-allocated value.
	for _, cap := range m.capabilities {
		scr.D = cap.Encode(m, nil, scr.D[:1])
		r, err := w.Write(scr.Convert())
		n += int64(r)
		if err != nil {
			return n, err
		}
	}
	return n, nil
}

func encodeExts(w io.Writer, m *Module, scr *scratch) (n int64, err error) {
	for _, ext := range m.extensions {
		sz := ext.Size()
		if sz >= 0xffff {
			err := &InstructionTooLargeError{Inst: &OpExtension{Name: ext}}
			return n, err
		}
		sz++
		v := spirvop.OpExtension | sz<<16
		scr.D = append(scr.D[:0], v)
		scr.D = ext.Encode(m, nil, scr.D)
		r, err := w.Write(scr.Convert())
		n += int64(r)
		if err != nil {
			return n, err
		}
	}
	return n, nil
}

func encodeImports(w io.Writer, m *Module, scr *scratch) (n int64, err error) {
	for _, op := range m.imports {
		err = scr.Encodele(m, op)
		if err != nil {
			return n, err
		}
		r, err := w.Write(scr.Convert())
		n += int64(r)
		if err != nil {
			return n, err
		}
	}
	return n, nil
}

func encodeMemModel(w io.Writer, m *Module, scr *scratch) (n int64, err error) {
	err = scr.Encodele(m, m.memModel)
	if err != nil {
		return 0, err
	}
	r, err := w.Write(scr.Convert())
	n += int64(r)
	if err != nil {
		return n, err
	}
	return n, nil
}

func encodeEntryPoints(w io.Writer, m *Module, scr *scratch) (n int64, err error) {
	for _, op := range m.entryPoints {
		err = scr.Encodele(m, op)
		if err != nil {
			return n, err
		}
		r, err := w.Write(scr.Convert())
		n += int64(r)
		if err != nil {
			return n, err
		}
	}
	return n, nil
}

func encodeProcesses(w io.Writer, m *Module, scr *scratch) (n int64, err error) {
	for _, p := range m.processes {
		sz := p.Size()
		if sz >= 0xffff {
			err := &InstructionTooLargeError{Inst: &OpModuleProcessed{Process: p}}
			return n, err
		}
		sz++
		v := spirvop.OpModuleProcessed | sz<<16
		scr.D = append(scr.D[:0], v)
		scr.D = p.Encode(m, nil, scr.D)
		r, err := w.Write(scr.Convert())
		n += int64(r)
		if err != nil {
			return n, err
		}
	}
	return n, nil
}

func encodeInsts(insts []Instruction) encodeStep {
	return func(w io.Writer, m *Module, scr *scratch) (n int64, err error) {
		for _, inst := range insts {
			if err := scr.Encodele(m, inst); err != nil {
				return n, err
			}
			r, err := w.Write(scr.Convert())
			n += int64(r)
			if err != nil {
				return n, err
			}
		}
		return n, nil
	}
}

// scratch holds scratch space for encoding and decoding SPIR-V modules.
type scratch struct {
	B   []byte
	D   []uint32
	Ops []Operand
	I   int
	W   int
}

// Encodele encodes an instruction from m.
func (scr *scratch) Encodele(m *Module, inst Instruction) error {
	// Increment the instruction counter first so that positions we report are
	// automatically 1-based.
	scr.I++
	scr.D = append(scr.D[:0], 0)
	scr.Ops = inst.Operands(scr.Ops[:0])
	// Operand parameters are encoded in FIFO order after all operands.
	// Use a plain for loop instead of a range to account for changing size.
	for i := 0; i < len(scr.Ops); i++ {
		scr.Ops = scr.Ops[i].Parameters(scr.Ops)
	}
	for _, p := range scr.Ops {
		scr.D = p.Encode(m, inst, scr.D)
	}
	if len(scr.D) > 0xffff {
		err := &InstructionTooLargeError{Inst: inst}
		return err
	}
	scr.D[0] = inst.Opcode() | uint32(len(scr.D))<<16
	return nil
}

// Convert encodes scr.D into scr.B and returns scr.B.
func (scr *scratch) Convert() []byte {
	scr.B = scr.B[:0]
	for _, x := range scr.D {
		scr.B = append(scr.B, 0, 0, 0, 0)
		binary.LittleEndian.PutUint32(scr.B[len(scr.B)-4:], x)
	}
	scr.W += len(scr.D)
	return scr.B
}
