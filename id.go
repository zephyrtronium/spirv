package spirv

// Id is a SPIR-V <id>.
type Id uint32

func (id *Id) Encode(m *Module, n Instruction, p []uint32) []uint32 {
	return append(p, uint32(*id))
}

func (id *Id) Scan(m *Module, n Instruction, p []uint32) (int, error) {
	if len(p) == 0 {
		err := ShortInstructionError{Inst: n, Operand: "<id>"}
		return 0, &err
	}
	*id = Id(p[0])
	return 1, nil
}

func (*Id) Parameters(p []Operand) []Operand {
	return p
}

// Ref returns an IdRef to this <id>.
func (id *Id) Ref() IdRef {
	if id == nil {
		return IdRef{}
	}
	return IdRef{*id}
}

// IdRef is a reference to an <id>. The zero value is an invalid reference.
type IdRef struct {
	id Id
}

func (id *IdRef) Encode(m *Module, n Instruction, p []uint32) []uint32 {
	return id.id.Encode(m, n, p)
}

func (id *IdRef) Scan(m *Module, n Instruction, p []uint32) (int, error) {
	return id.id.Scan(m, n, p)
}

func (*IdRef) Parameters(p []Operand) []Operand {
	return p
}

// IsValid returns whether the reference is to a valid <id>.
func (id *IdRef) IsValid() bool {
	return id.id != 0
}

// IdResultType is a reference to the <id> of a type.
type IdResultType struct {
	id Id
}

func (id *IdResultType) Encode(m *Module, n Instruction, p []uint32) []uint32 {
	return id.id.Encode(m, n, p)
}

func (id *IdResultType) Scan(m *Module, n Instruction, p []uint32) (int, error) {
	return id.id.Scan(m, n, p)
}

func (*IdResultType) Parameters(p []Operand) []Operand {
	return p
}

// AsRef converts the reference to an IdRef.
func (id IdResultType) AsRef() IdRef {
	return IdRef(id)
}

// IdResult is a reference to the <id> of the result of an instruction other
// than a type-declaration instruction.
type IdResult struct {
	id Id
}

func (id *IdResult) Encode(m *Module, n Instruction, p []uint32) []uint32 {
	return id.id.Encode(m, n, p)
}

func (id *IdResult) Scan(m *Module, n Instruction, p []uint32) (int, error) {
	return id.id.Scan(m, n, p)
}

func (*IdResult) Parameters(p []Operand) []Operand {
	return p
}

// AsRef converts the reference to an IdRef.
func (id IdResult) AsRef() IdRef {
	return IdRef(id)
}

type (
	IdMemorySemantics = IdRef
	IdScope           = IdRef
)
