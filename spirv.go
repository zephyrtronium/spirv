package spirv

//go:generate go run ./generate
//go:generate go fmt ./...

// Instruction is a SPIR-V instruction.
type Instruction interface {
	// String returns the name of the instruction.
	String() string
	// Opcode returns the opcode of the instruction as a uint32.
	Opcode() uint32
	// ResultID returns a reference to the instruction's result <id>. If the
	// instruction has no result, the reference is invalid.
	ResultID() IdRef
	// Setup initializes the instruction's enumeration operands according to
	// the values in p. A subsequent call to Operands allows each operand to be
	// scanned separately.
	Setup(p []uint32) error
	// Operands appends the instruction's operands to p.
	Operands(p []Operand) []Operand
}

// TypeDecl is a type declaration instruction.
type TypeDecl interface {
	Instruction
	// TypeRef returns a type reference to the <id> this instruction defines.
	TypeRef() IdResultType
}

// ConstInstruction is a constant instruction.
type ConstInstruction interface {
	Instruction
	CreatesConstant()
}

// Operand is an operand to a SPIR-V instruction or enumerant.
type Operand interface {
	// Encode appends the physical layout of the operand to p, not including
	// its parameters.
	Encode(m *Module, n Instruction, p []uint32) []uint32
	// Scan sets the value of the operand from the words in p. It returns the
	// number of words scanned.
	Scan(m *Module, n Instruction, p []uint32) (int, error)
	// Parameters appends the parameters of the operand to p in the order that
	// they should be encoded.
	Parameters(p []Operand) []Operand
}

// ValueEnum is a value enumeration. Enumerants of a ValueEnum are mutually
// exclusive.
type ValueEnum interface {
	Operand
	// EnumWord returns the enumerant value.
	EnumWord() uint32
	// String returns the name of the enumerant.
	String() string
}

// BitEnum is a bit enumeration. Enumerants of a BitEnum are composable.
type BitEnum interface {
	Operand
	// EnumBits returns a mask of the bits set in the enumeration.
	EnumBits() uint32
	// String returns a string representation of the bits set in the
	// enumeration, separating enumerants with | characters.
	String() string
}
