package spirv_test

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/zephyrtronium/spirv"
	"gitlab.com/zephyrtronium/spirv/spirvop"
)

func TestEncodeMinimal(t *testing.T) {
	var m spirv.Module
	var b bytes.Buffer
	// A module requires a memory model to serialize.
	m.SetMemoryModel(spirv.AddressingModelLogical, spirv.MemoryModelSimple)
	if _, err := m.WriteTo(&b); err != nil {
		t.Error(err)
	}
	ver, mag, bnd, err := readHeader(&b, binary.LittleEndian)
	if err != nil {
		t.Error(err)
	}
	if ver != spirv.VersionWord {
		t.Errorf("wrong version: want %#x, got %#x", spirv.VersionWord, ver)
	}
	if mag != 0 {
		t.Errorf("wrong generator magic number: want 0, got %d", mag)
	}
	if bnd != 1 {
		t.Errorf("wrong bound: want 1, got %d", bnd)
	}
	inst, err := readInstWords(&b, binary.LittleEndian)
	if err != nil {
		t.Error(err)
	}
	if inst[0]&0xffff != spirvop.OpMemoryModel {
		t.Errorf("wrong instruction: want opcode %x, got %x", spirvop.OpMemoryModel, inst[0]&0xffff)
	}
	if inst[0]>>16 != 3 || len(inst) != 3 {
		t.Errorf("wrong instruction length: want 3 words, got %x", inst)
	}
	p, err := io.ReadAll(&b)
	if err != nil {
		t.Error(err)
	}
	if len(p) != 0 {
		t.Errorf("unexpected module content: %x", p)
	}
}

func TestEncodeMultipleParams(t *testing.T) {
	var scr spirv.Scratch
	opr := spirv.DecorationLinkageAttributes{
		Name:        "",
		LinkageType: spirv.LinkageTypeImport,
	}
	inst := spirv.OpDecorate{
		Target:   spirv.Ref(1),
		Operand1: &opr,
	}
	want := []uint32{
		inst.Opcode() | 5<<16,              // opcode and size
		1,                                  // target
		opr.EnumWord(),                     // decoration
		0,                                  // name
		spirv.LinkageTypeImport.EnumWord(), // linkage type
	}
	scr.Encodele(nil, &inst)
	if !reflect.DeepEqual(scr.D, want) {
		t.Errorf("wrong instruction encoding: want %08x, got %08x", want, scr.D)
	}
}

func TestEncodeBitParams(t *testing.T) {
	var scr spirv.Scratch
	opr := new(spirv.ImageOperands).SetBits(
		&spirv.ImageOperandsLod{Param0: spirv.Ref(1)},
		&spirv.ImageOperandsConstOffset{Param0: spirv.Ref(2)},
		spirv.ImageOperandsSignExtend,
	)
	inst := spirv.OpImageSampleExplicitLod{
		ResultType:   spirv.TypeRef(3),
		Result:       4,
		SampledImage: spirv.Ref(5),
		Coordinate:   spirv.Ref(6),
		Operand4:     *opr,
	}
	want := []uint32{
		inst.Opcode() | 8<<16, // opcode and size
		3, 4,                  // result type and result
		5,              // sampled image
		6,              // coordinate
		opr.EnumBits(), // operands enum
		1,              // explicit lod
		2,              // const offset
	}
	scr.Encodele(nil, &inst)
	if !reflect.DeepEqual(scr.D, want) {
		t.Errorf("wrong instruction encoding: want %08x, got %08x", want, scr.D)
	}
}

// TestEncodeErrors tests that encoding a module containing invalid
// instructions returns an appropriate error.
func TestEncodeErrors(t *testing.T) {
	cases := []struct {
		name string
		ops  []spirv.Instruction
		err  error
	}{
		{
			name: "InstructionTooLarge",
			ops: []spirv.Instruction{
				&spirv.OpMemoryModel{Operand0: spirv.AddressingModelLogical, Operand1: spirv.MemoryModelSimple},
				&spirv.OpString{Result: 1, Str: spirv.LiteralString(strings.Repeat("AAAA", 65536))},
			},
			err: &spirv.InstructionTooLargeError{
				InstPos: 2,
				WordPos: 8,
				Inst:    new(spirv.OpString),
			},
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			var m spirv.Module
			for _, op := range c.ops {
				m.Add(op)
			}
			_, err := m.WriteTo(io.Discard)
			if !eqerrs(t, err, c.err) {
				t.Errorf("wrong error: want %#v, got %#v", c.err, err)
			}
		})
	}
}

func readHeader(b *bytes.Buffer, order binary.ByteOrder) (ver, mag, bnd uint32, err error) {
	p := make([]byte, 4*5)
	if _, err = io.ReadFull(b, p); err != nil {
		return
	}
	if x := order.Uint32(p); x != spirv.Magic {
		err = fmt.Errorf("wrong SPIR-V magic number: want %#x, got %#x", spirv.Magic, x)
		return
	}
	if x := order.Uint32(p[16:]); x != 0 {
		err = fmt.Errorf("nonzero reserved header value %#x", x)
		return
	}
	ver = order.Uint32(p[4:])
	if ver&0xff0000ff != 0 || ver>>16 != spirv.MajorVersion || ver>>8&0xff > spirv.MinorVersion {
		err = fmt.Errorf("unrecognized SPIR-V version number %#x", ver)
		return
	}
	mag = order.Uint32(p[8:])
	bnd = order.Uint32(p[12:])
	return
}

func readInstWords(b *bytes.Buffer, order binary.ByteOrder) ([]uint32, error) {
	p := make([]byte, 4)
	if _, err := io.ReadFull(b, p); err != nil {
		return nil, fmt.Errorf("couldn't read instruction opcode: %w", err)
	}
	h := order.Uint32(p)
	r := make([]uint32, h>>16)
	r[0] = h
	if len(r) == 1 {
		return r, nil
	}
	err := binary.Read(b, order, r[1:])
	if err != nil {
		return nil, fmt.Errorf("couldn't read instruction operands: %w", err)
	}
	return r, nil
}
