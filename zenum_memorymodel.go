// Copyright (c) 2014-2020 The Khronos Group Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and/or associated documentation files (the "Materials"),
// to deal in the Materials without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Materials, and to permit persons to whom the
// Materials are furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Materials.
//
// MODIFICATIONS TO THIS FILE MAY MEAN IT NO LONGER ACCURATELY REFLECTS KHRONOS
// STANDARDS. THE UNMODIFIED, NORMATIVE VERSIONS OF KHRONOS SPECIFICATIONS AND
// HEADER INFORMATION ARE LOCATED AT https://www.khronos.org/registry/
//
// THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM,OUT OF OR IN CONNECTION WITH THE MATERIALS OR THE USE OR OTHER DEALINGS
// IN THE MATERIALS.

package spirv

// Code generated by spirv/generate; DO NOT EDIT

// MemoryModel is a ValueEnum.
type MemoryModel interface {
	ValueEnum
	isMemoryModel()
}

// MemoryModelEnumerant creates a blank MemoryModel containing the enumerant
// corresponding to val. If there is no such enumerant, the result is nil.
func MemoryModelEnumerant(val uint32) MemoryModel {
	switch val {
	case 0:
		return MemoryModelSimple
	case 1:
		return MemoryModelGLSL450
	case 2:
		return MemoryModelOpenCL
	case 3:
		return MemoryModelVulkan
	default:
		return nil
	}
}

type _MemoryModelSimple struct{}

func (_MemoryModelSimple) Encode(m *Module, n Instruction, p []uint32) []uint32   { return append(p, 0) }
func (_MemoryModelSimple) Scan(m *Module, n Instruction, p []uint32) (int, error) { return 1, nil }
func (_MemoryModelSimple) Parameters(p []Operand) []Operand                       { return p }
func (_MemoryModelSimple) EnumWord() uint32                                       { return 0 }
func (_MemoryModelSimple) String() string                                         { return "Simple" }
func (_MemoryModelSimple) isMemoryModel()                                         {}

// MemoryModelSimple is an enumerant of MemoryModel.
//
// Enabling capabilities: [Shader]
var MemoryModelSimple MemoryModel = _MemoryModelSimple{}

type _MemoryModelGLSL450 struct{}

func (_MemoryModelGLSL450) Encode(m *Module, n Instruction, p []uint32) []uint32   { return append(p, 1) }
func (_MemoryModelGLSL450) Scan(m *Module, n Instruction, p []uint32) (int, error) { return 1, nil }
func (_MemoryModelGLSL450) Parameters(p []Operand) []Operand                       { return p }
func (_MemoryModelGLSL450) EnumWord() uint32                                       { return 1 }
func (_MemoryModelGLSL450) String() string                                         { return "GLSL450" }
func (_MemoryModelGLSL450) isMemoryModel()                                         {}

// MemoryModelGLSL450 is an enumerant of MemoryModel.
//
// Enabling capabilities: [Shader]
var MemoryModelGLSL450 MemoryModel = _MemoryModelGLSL450{}

type _MemoryModelOpenCL struct{}

func (_MemoryModelOpenCL) Encode(m *Module, n Instruction, p []uint32) []uint32   { return append(p, 2) }
func (_MemoryModelOpenCL) Scan(m *Module, n Instruction, p []uint32) (int, error) { return 1, nil }
func (_MemoryModelOpenCL) Parameters(p []Operand) []Operand                       { return p }
func (_MemoryModelOpenCL) EnumWord() uint32                                       { return 2 }
func (_MemoryModelOpenCL) String() string                                         { return "OpenCL" }
func (_MemoryModelOpenCL) isMemoryModel()                                         {}

// MemoryModelOpenCL is an enumerant of MemoryModel.
//
// Enabling capabilities: [Kernel]
var MemoryModelOpenCL MemoryModel = _MemoryModelOpenCL{}

type _MemoryModelVulkan struct{}

func (_MemoryModelVulkan) Encode(m *Module, n Instruction, p []uint32) []uint32   { return append(p, 3) }
func (_MemoryModelVulkan) Scan(m *Module, n Instruction, p []uint32) (int, error) { return 1, nil }
func (_MemoryModelVulkan) Parameters(p []Operand) []Operand                       { return p }
func (_MemoryModelVulkan) EnumWord() uint32                                       { return 3 }
func (_MemoryModelVulkan) String() string                                         { return "Vulkan" }
func (_MemoryModelVulkan) isMemoryModel()                                         {}

// MemoryModelVulkan is an enumerant of MemoryModel.
//
// Enabling capabilities: [VulkanMemoryModel]
//
// Missing before SPIR-V 1.5
var MemoryModelVulkan MemoryModel = _MemoryModelVulkan{}

type _MemoryModelVulkanKHR struct{}

func (_MemoryModelVulkanKHR) Encode(m *Module, n Instruction, p []uint32) []uint32 {
	return append(p, 3)
}
func (_MemoryModelVulkanKHR) Scan(m *Module, n Instruction, p []uint32) (int, error) { return 1, nil }
func (_MemoryModelVulkanKHR) Parameters(p []Operand) []Operand                       { return p }
func (_MemoryModelVulkanKHR) EnumWord() uint32                                       { return 3 }
func (_MemoryModelVulkanKHR) String() string                                         { return "VulkanKHR" }
func (_MemoryModelVulkanKHR) isMemoryModel()                                         {}

// MemoryModelVulkanKHR is an enumerant of MemoryModel.
//
// Enabling capabilities: [VulkanMemoryModel]
//
// Requires extensions: [SPV_KHR_vulkan_memory_model]
//
// Missing before SPIR-V 1.5
var MemoryModelVulkanKHR MemoryModel = _MemoryModelVulkanKHR{}
