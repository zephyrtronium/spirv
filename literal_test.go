package spirv_test

import (
	"math"
	"reflect"
	"testing"

	"gitlab.com/zephyrtronium/spirv"
)

func TestLiteralString(t *testing.T) {
	cases := []struct {
		name string
		s    spirv.LiteralString
		r    []uint32
	}{
		{"empty", "", []uint32{0}},
		{"one", "a", []uint32{'a'}},
		{"two", "ab", []uint32{'a' | 'b'<<8}},
		{"four", "abcd", []uint32{'a' | 'b'<<8 | 'c'<<16 | 'd'<<24, 0}},
		{"jp", "日本語", []uint32{0xe6a597e6, 0xaae8ac9c, 0x9e}},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			r := c.s.Encode(nil, nil, nil)
			if !reflect.DeepEqual(r, c.r) {
				t.Errorf("wrong encoding of %q: want %08x, got %08x", c.s, c.r, r)
			}
			if l := int(c.s.Size()); len(r) != l {
				t.Errorf("wrong length for %q: want %d, got %d", c.s, len(r), l)
			}
			var u spirv.LiteralString
			l, err := u.Scan(nil, nil, r)
			if l != len(c.r) {
				t.Errorf("scanning consumed wrong number of words: want %d, got %d", len(c.r), l)
			}
			if err != nil {
				t.Error("error during round-trip:", err)
			}
			if u != c.s {
				t.Errorf("round-trip failed: want %q, got %q", c.s, u)
			}
		})
	}
}

func TestLiteralContextDependentNumber(t *testing.T) {
	m := moduleWithTypes()
	cases := []struct {
		name string
		typ  spirv.Id
		l    spirv.LiteralContextDependentNumber
		r    []uint32
	}{
		{"u8", 1, spirv.UnsignedConstant(0x0102030405060708, 8), []uint32{0x08}},
		{"u16", 2, spirv.UnsignedConstant(0x0102030405060708, 16), []uint32{0x0708}},
		{"u32", 3, spirv.UnsignedConstant(0x0102030405060708, 32), []uint32{0x05060708}},
		{"u64", 4, spirv.UnsignedConstant(0x0102030405060708, 64), []uint32{0x05060708, 0x01020304}},

		{"i8", 5, spirv.SignedConstant(0x0102030405060708, 8), []uint32{0x08}},
		{"i16", 6, spirv.SignedConstant(0x0102030405060708, 16), []uint32{0x0708}},
		{"i32", 7, spirv.SignedConstant(0x0102030405060708, 32), []uint32{0x05060708}},
		{"i64", 8, spirv.SignedConstant(0x0102030405060708, 64), []uint32{0x05060708, 0x01020304}},
		{"n8", 5, spirv.SignedConstant(-1, 8), []uint32{0xffffffff}},
		{"n16", 6, spirv.SignedConstant(-1, 16), []uint32{0xffffffff}},
		{"n32", 7, spirv.SignedConstant(-1, 32), []uint32{0xffffffff}},
		{"n64", 8, spirv.SignedConstant(-1, 64), []uint32{0xffffffff, 0xffffffff}},

		{"f16", 9, spirv.FloatConstant(1.0/3.0, 16), []uint32{0b0_01101_0101010101}},
		{"f32", 10, spirv.FloatConstant(1.0/3.0, 32), []uint32{0b0_01111101_01010101010101010101011}},
		{"f64", 11, spirv.FloatConstant(1.0/3.0, 64), []uint32{0x55555555, 0b0_01111111101_01010101010101010101}},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			inst := spirv.OpConstant{ResultType: spirv.TypeRef(c.typ), Result: 12, Value: c.l}
			r := c.l.Encode(m, &inst, nil)
			if !reflect.DeepEqual(r, c.r) {
				t.Errorf("wrong encoding: want %08x, got %08x", c.r, r)
			}
		})
	}
}

func TestFloatConstant16(t *testing.T) {
	cases := []struct {
		name string
		x    float64
		r    uint16
	}{
		{"zero", 0, 0},
		{"positive subnormal", math.Nextafter(0x1p-14, 0), 0},
		{"smallest positive normal", 0x1p-14, 0b0_00001_0000000000},
		{"rounded positive normal", 0x1p-14 + 0x1p-25, 0b0_00001_0000000000},
		{"one", 1, 0b0_01111_0000000000},
		{"largest finite", 65504, 0b0_11110_1111111111},
		{"larger", math.Nextafter(65504, 65505), 0b0_11111_0000000000},
		{"inf", math.Inf(0), 0b0_11111_0000000000},
		{"nan", math.NaN(), 0b0_11111_0111111111},

		{"negative zero", math.Copysign(0, -1), 0},
		{"negative subnormal", math.Nextafter(-0x1p-14, 0), 0},
		{"smallest negative normal", -0x1p-14, 0b1_00001_0000000000},
		{"rounded negative normal", -0x1p-14 - 0x1p-25, 0b1_00001_0000000000},
		{"negative one", -1, 0b1_01111_0000000000},
		{"largest negative finite", -65504, 0b1_11110_1111111111},
		{"negativer", math.Nextafter(-65504, -65505), 0b1_11111_0000000000},
		{"negative inf", math.Inf(-1), 0b1_11111_0000000000},
		{"negative nan", math.Copysign(math.NaN(), -1), 0b0_11111_0111111111},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			l := spirv.FloatConstant(c.x, 16)
			if l.Raw() != uint64(c.r) {
				t.Errorf("wrong binary16 encoding for %f: want %#04x, got %#04x", c.x, c.r, l.Raw())
			}
		})
	}
}
