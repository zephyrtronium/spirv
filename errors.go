package spirv

import (
	"encoding/binary"
	"strconv"
)

// Error is an error specific to SPIR-V module encoding or decoding. The
// module encoder and decoder automatically set position information in such
// errors as available.
type Error interface {
	error
	// Position returns the number of instructions and words preceding the
	// error within the module containing the problematic instruction. The
	// number of words may refer to the start of the instruction or a specific
	// operand. For module encoding, this is based on the logical layout in
	// the SPIR-V specification rather than the order in which instructions
	// are added.
	Position() (insts, words int)
	// setPosition sets the position returned by Position.
	setPosition(insts, words int)
}

// InstructionTooLargeError is an error returned while serializing a module
// when an instruction would require more than 65535 words to encode.
type InstructionTooLargeError struct {
	// InstPos and WordPos are the number of instructions and words,
	// respectively, preceding the instruction.
	InstPos, WordPos int
	// Inst is the instruction which could not be encoded.
	Inst Instruction
}

func (err *InstructionTooLargeError) Error() string {
	i := strconv.Itoa(err.InstPos)
	w := strconv.Itoa(err.WordPos)
	return "inst " + i + " at word " + w + ": " + err.Inst.String() + " too large"
}

func (err *InstructionTooLargeError) Position() (insts, words int) {
	return err.InstPos, err.WordPos
}

func (err *InstructionTooLargeError) setPosition(insts, words int) {
	err.InstPos, err.WordPos = insts, words
}

var _ Error = (*InstructionTooLargeError)(nil)

// UnknownOpcodeError is an error returned while deserializing a module when an
// unknown instruction opcode is encountered.
type UnknownOpcodeError struct {
	// InstPos and WordPos are the number of instructions and words,
	// respectively, preceding the instruction.
	InstPos, WordPos int
	// Opcode is the unknown instruction opcode.
	Opcode uint32
	// Data is the words of the instruction, including the word encoding the
	// opcode and length.
	Data []uint32
}

func (err *UnknownOpcodeError) Error() string {
	i := strconv.Itoa(err.InstPos)
	w := strconv.Itoa(err.WordPos)
	return "inst " + i + " at word " + w + ": " + "unknown instruction opcode 0x" + strconv.FormatUint(uint64(err.Opcode), 16)
}

func (err *UnknownOpcodeError) Position() (insts, words int) {
	return err.InstPos, err.WordPos
}

func (err *UnknownOpcodeError) setPosition(insts, words int) {
	err.InstPos, err.WordPos = insts, words
}

var _ Error = (*UnknownOpcodeError)(nil)

// ShortInstructionError is an error returned while deserializing a module when
// there is insufficient data in an instruction to decode an operand.
type ShortInstructionError struct {
	// InstPos and WordPos are the number of instructions and words,
	// respectively, preceding the instruction.
	InstPos, WordPos int
	// Inst is the instruction that was partially decoded.
	Inst Instruction
	// Operand is the name of the operand that could not be decoded.
	Operand string
}

func (err *ShortInstructionError) Error() string {
	i := strconv.Itoa(err.InstPos)
	w := strconv.Itoa(err.WordPos)
	return "inst " + i + " at word " + w + ": " + err.Inst.String() + " missing data for " + err.Operand + " operand"
}

func (err *ShortInstructionError) Position() (insts, words int) {
	return err.InstPos, err.WordPos
}

func (err *ShortInstructionError) setPosition(insts, words int) {
	err.InstPos, err.WordPos = insts, words
}

var _ Error = (*ShortInstructionError)(nil)

// ZeroInstructionLengthError is an error returned while deserializing a module
// when an instruction has zero length.
type ZeroInstructionLengthError struct {
	// InstPos and WordPos are the number of instructions and words,
	// respectively, preceding the instruction.
	InstPos, WordPos int
	// Opcode is the opcode of the instruction which had zero length.
	Opcode uint32
}

func (err *ZeroInstructionLengthError) Error() string {
	i := strconv.Itoa(err.InstPos)
	w := strconv.Itoa(err.WordPos)
	return "inst " + i + " at word " + w + ": " + "instruction with opcode 0x" + strconv.FormatInt(int64(err.Opcode), 16) + " has zero length"
}

func (err *ZeroInstructionLengthError) Position() (insts, words int) {
	return err.InstPos, err.WordPos
}

func (err *ZeroInstructionLengthError) setPosition(insts, words int) {
	err.InstPos, err.WordPos = insts, words
}

var _ Error = (*ZeroInstructionLengthError)(nil)

// InvalidEnumerantError is an error returned while deserializing a module when
// a value or bit enumerant is unknown.
type InvalidEnumerantError struct {
	// InstPos and WordPos are the number of instructions and words,
	// respectively, preceding the instruction.
	InstPos, WordPos int
	// Inst is the instruction containing the invalid enumerant.
	Inst Instruction
	// Enum is the name of the enumeration.
	Enum string
	// Value is the unknown enumerant. For bit enums, all unknown bits in the
	// encoding are set.
	Value uint32
}

func (err *InvalidEnumerantError) Error() string {
	i := strconv.Itoa(err.InstPos)
	w := strconv.Itoa(err.WordPos)
	c := strconv.FormatUint(uint64(err.Value), 16)
	return "inst " + i + " at word " + w + ": " + err.Inst.String() + " has unknown " + err.Enum + " enumerant 0x" + c
}

func (err *InvalidEnumerantError) Position() (insts, words int) {
	return err.InstPos, err.WordPos
}

func (err *InvalidEnumerantError) setPosition(insts, words int) {
	err.InstPos, err.WordPos = insts, words
}

var _ Error = (*InvalidEnumerantError)(nil)

// InvalidHeaderError is an error returned while deserializing a module when
// the fixed SPIR-V header is invalid.
//
// InvalidHeaderError does not implement Error.
type InvalidHeaderError struct {
	// Magic is the SPIR-V magic number. Non-erroneous values are 0x07230203
	// or 0x03022307, determining the endianism of the module. If it is neither
	// of those, then the remaining fields of the error are zero.
	Magic [4]byte
	// Version is the SPIR-V version field, decoded according to the endianism
	// determined by the magic number. Non-erroneous values are those
	// corresponding to SPIR-V versions 1.0 through 1.6.
	Version uint32
	// GeneratorMagic is the generator magic number. It is included for
	// completeness; no value is erroneous.
	GeneratorMagic uint32
	// Bound is the bound on <id>s in the module. It must be nonzero.
	Bound uint32
	// Reserved is a reserved word. It is erroneous if it is nonzero.
	Reserved uint32
}

func (err *InvalidHeaderError) Error() string {
	switch {
	case err.IsInvalidMagic():
		return "invalid magic number"
	case err.IsInvalidVersion():
		return "invalid SPIR-V version"
	case err.IsInvalidBound():
		return "header indicates zero bound"
	case err.IsInvalidReserved():
		return "nonzero header reserved word"
	default:
		panic("spirv: InvalidHeaderError with no error condition")
	}
}

// IsInvalidMagic returns whether the SPIR-V magic number recorded in the error
// is invalid.
func (err *InvalidHeaderError) IsInvalidMagic() bool {
	switch Magic {
	case binary.LittleEndian.Uint32(err.Magic[:]):
		return false
	case binary.BigEndian.Uint32(err.Magic[:]):
		return false
	default:
		return true
	}
}

// IsInvalidVersion returns whether the encoded SPIR-V version number does not
// correspond to a known SPIR-V version.
func (err *InvalidHeaderError) IsInvalidVersion() bool {
	maj, min := splitVersion(err.Version)
	return maj == 0 && min == 0
}

// IsInvalidBound returns whether the bound is invalid.
func (err *InvalidHeaderError) IsInvalidBound() bool {
	return err.Bound == 0
}

// IsInvalidReserved returns whether the reserved word is invalid.
func (err *InvalidHeaderError) IsInvalidReserved() bool {
	return err.Reserved != 0
}
