package spirv

import (
	"errors"
	"strings"
)

// Module manages construction and serialization of a SPIR-V module.
//
// A Module does not enforce most requirements of SPIR-V modules.
// However, a Module always serializes according to the logical layout defined
// in the SPIR-V specification, section 2.4, regardless of the order in which
// instructions are added. Additionally, a Module always has exactly one
// OpMemoryModel instruction, defaulting to the Logical addressing model and
// the Simple memory model.
//
// Using Add to add each successive instruction of a valid SPIR-V module to a
// Module and then serializing the result should produce an identical binary.
type Module struct {
	// All OpCapability instructions.
	capabilities []Capability
	// Optional OpExtension instructions.
	extensions []LiteralString
	// Optional OpExtInstImport instructions.
	imports []*OpExtInstImport
	// The single required OpMemoryModel instruction.
	memModel *OpMemoryModel
	// All entry point declarations.
	entryPoints []*OpEntryPoint
	// All execution-mode declarations, using OpExecutionMode or
	// OpExecutionModeId.
	execModes []Instruction
	// Debug instructions, grouped in the following order:
	//  a. OpString, OpSourceExtension, OpSource, OpSourceContinued, with no
	//     forward references.
	strings []Instruction
	//  b. All OpName and OpMemberName.
	names []Instruction
	//  c. All OpModuleProcessed instructions.
	processes []LiteralString
	// All annotation instructions:
	// OpDecorate, OpMemberDecorate, OpDecorationGroup, OpGroupDecorate,
	// OpGroupMemberDecorate, OpDecorateId, OpDecorateString,
	// OpMemberDecorateString.
	annotations []Instruction
	// All type declarations, all constant instructions, and all global
	// variable instructions. No forward references. May also include OpLine
	// and OpNoLine.
	globl []Instruction
	// All function declarations (for functions without bodies). Each
	// declaration consists of OpFunction, any number of OpFunctionParameter,
	// and OpFunctionEnd.
	funcDecls []Instruction
	// All function definitions (with bodies). Each definition consists of
	// OpFunction, any number of OpFunctionParameter, at least one block
	// (OpLabel, other instructions, and a termination instruction), and an
	// OpFunctionEnd. Additionally, the OpFunction of and any OpLabel within a
	// function definition may be preceded by an OpLine instruction.
	funcs []Instruction

	// litSize maps <id> to size in bits for numeric literals with OpConstant
	// and OpSpecConstant.
	litSize map[Id]int32
	// maxid is the current maximum of <id>s in the module.
	maxid Id

	// extids maps extension names to the <id>s to which they are imported.
	extids map[LiteralString]Id
	// extnames maps <id>s used for importing extensions to the extension name.
	extnames map[IdRef]string

	// maj and min are the SPIR-V version associated with the module.
	maj, min uint8
	// Magic is the SPIR-V generator's magic number. It is allowed to be zero.
	// Nonzero generator magic numbers are reserved at
	// https://github.com/KhronosGroup/SPIRV-Headers.
	Magic uint32
}

// Add adds an instruction to the module in the correct logical location for
// that instruction. If an instruction is valid in multiple logical locations,
// it is added to the last such location that already contains instructions.
//
// Panics with ErrMemModelExists if an OpMemoryModel instruction is added when
// the module already has a memory model, whether set by adding an earlier
// OpMemoryModel instruction or by calling SetMemoryModel.
func (m *Module) Add(op Instruction) {
	switch op := op.(type) {
	case *OpCapability:
		m.capabilities = append(m.capabilities, op.Capability)
	case *OpExtension:
		m.extensions = append(m.extensions, op.Name)
	case *OpExtInstImport:
		if m.extids == nil {
			m.extids = make(map[LiteralString]Id)
			m.extnames = make(map[IdRef]string)
		}
		m.imports = append(m.imports, op)
		m.extids[op.Name] = op.Result
		// Extension instruction sets generally have names like "GLSL.std.450"
		// or "OpenCL.debuginfo.100", but there is no case-preserving mapping
		// between extension instruction set names and their grammars from
		// which we generate ExtInsts. The best we can do (for now?) is to
		// derive the extension instruction set name from the filename, which
		// is all lower case in the SPIRV-Headers repository. On the assumption
		// that there will never be a glsl.std.450 instruction set which is
		// different from GLSL.std.450, the module will use lower case names.
		m.extnames[op.Result.Ref()] = strings.ToLower(string(op.Name))
	case *OpMemoryModel:
		if m.memModel != nil {
			panic(ErrMemModelExists)
		}
		m.memModel = op
	case *OpEntryPoint:
		m.entryPoints = append(m.entryPoints, op)
	case *OpExecutionMode, *OpExecutionModeId:
		m.execModes = append(m.execModes, op)
	case *OpString, *OpSourceExtension, *OpSource, *OpSourceContinued:
		m.strings = append(m.strings, op)
	case *OpName, *OpMemberName:
		m.names = append(m.names, op)
	case *OpModuleProcessed:
		m.processes = append(m.processes, op.Process)
	case *OpDecorate, *OpMemberDecorate, *OpDecorationGroup, *OpGroupDecorate,
		*OpGroupMemberDecorate, *OpDecorateId, *OpDecorateString, *OpMemberDecorateString:
		m.annotations = append(m.annotations, op)
	case *OpTypeInt:
		if m.litSize == nil {
			m.litSize = make(map[Id]int32)
		}
		m.litSize[op.Result] = int32(op.Width)
		m.globl = append(m.globl, op)
	case *OpTypeFloat:
		if m.litSize == nil {
			m.litSize = make(map[Id]int32)
		}
		m.litSize[op.Result] = int32(op.Width)
		m.globl = append(m.globl, op)
	case TypeDecl, ConstInstruction:
		m.globl = append(m.globl, op)
	case *OpVariable:
		if op.Operand2 != StorageClassFunction {
			m.globl = append(m.globl, op)
		} else {
			m.funcs = append(m.funcs, op)
		}
	case *OpUndef:
		// OpUndef can be in globals or in function bodies. In order to
		// round-trip valid modules, add to funcs if anything has been added
		// to funcs, otherwise we are still in globals.
		if len(m.funcs) > 0 {
			m.funcs = append(m.funcs, op)
		} else {
			m.globl = append(m.globl, op)
		}
	case *OpLine, *OpNoLine:
		// OpLine and OpNoLine can be anywhere after annotation instructions,
		// i.e. in globals, function decls, or in function defs.
		switch {
		case len(m.funcs) > 0:
			m.funcs = append(m.funcs, op)
		case len(m.funcDecls) > 0:
			m.funcDecls = append(m.funcDecls, op)
		default:
			m.globl = append(m.globl, op)
		}
	case *OpFunction:
		// OpFunction may begin a declaration or a definition.
		// If we haven't already defined any function, add it to decls first;
		// once we find an OpLabel, we can move the last declaration-like group
		// of instructions to definitions.
		if len(m.funcs) != 0 {
			m.funcs = append(m.funcs, op)
		} else {
			// If the last inst in globals is an OpLine, it applies to this
			// declaration, so it's sensible to move it to decls.
			if len(m.globl) > 0 {
				switch p := m.globl[len(m.globl)-1].(type) {
				case *OpLine:
					m.globl[len(m.globl)-1] = nil
					m.globl = m.globl[:len(m.globl)-1]
					m.funcDecls = append(m.funcDecls, p)
				}
			}
			m.funcDecls = append(m.funcDecls, op)
		}
	case *OpFunctionParameter, *OpFunctionEnd:
		// These may be involved in function declarations or definitions.
		// See OpFunction.
		if len(m.funcs) != 0 {
			m.funcs = append(m.funcs, op)
		} else {
			m.funcDecls = append(m.funcDecls, op)
		}
	case *OpLabel:
		// OpLabel starts a block. If we haven't already started filling out
		// function definitions, we should have a partial function declaration
		// which is actually the first definition.
		if len(m.funcs) == 0 {
			m.moveDecl()
		}
		m.funcs = append(m.funcs, op)
	default:
		// Every other instruction should be a constituent of a function.
		m.funcs = append(m.funcs, op)
	}
	// Ensure the module bound is always up to date.
	if b := op.ResultID(); b.id > m.maxid {
		m.maxid = b.id
	}
}

// SetMemoryModel sets the module's memory and addressing models, regardless of
// whether they have already been set.
func (m *Module) SetMemoryModel(addr AddressingModel, mem MemoryModel) {
	m.memModel = &OpMemoryModel{Operand0: addr, Operand1: mem}
}

// Bound returns the current bound on <id>s. It is 1 if no <id>s are used.
func (m *Module) Bound() Id {
	return m.maxid + 1
}

// Version returns the major and minor version of SPIR-V associated with the
// module. If no version has been set, the default is the version with which
// this package was generated.
func (m *Module) Version() (major, minor uint8) {
	if m.maj == 0 && m.min == 0 {
		return MajorVersion, MinorVersion
	}
	return m.maj, m.min
}

// SetVersion sets the major and minor version of SPIR-V associated with the
// module. If both are zero, the module returns to the default.
func (m *Module) SetVersion(major, minor uint8) {
	m.maj, m.min = major, minor
}

// moveDecl moves the last function declaration header to the function
// definitions block.
func (m *Module) moveDecl() {
	// Search for the last OpFunctionEnd and move everything after it.
	for i := len(m.funcDecls) - 1; i >= 0; i-- {
		if _, ok := m.funcDecls[i].(*OpFunctionEnd); ok {
			m.funcs = append(m.funcs, m.funcDecls[i+1:]...)
			for j := i + 1; j < len(m.funcDecls); j++ {
				m.funcDecls[j] = nil
			}
			m.funcDecls = m.funcDecls[:i+1]
			return
		}
	}
	// No OpFunctionEnd. All of funcDecls is actually a function definition.
	m.funcs, m.funcDecls = m.funcDecls, nil
}

// TypeInst returns the instruction which has the result <id> which id
// references, if that instruction is a type-declaration instruction. If no
// instruction declares the given result type, the result is nil.
func (m *Module) TypeInst(id IdResultType) TypeDecl {
	for _, op := range m.globl {
		t, ok := op.(TypeDecl)
		if !ok {
			continue
		}
		if t.TypeRef().id == id.id {
			return t
		}
	}
	return nil
}

// ErrMemModelExists is an error indicating that an OpMemoryModel instruction
// was added to a module which already has a memory model.
var ErrMemModelExists = errors.New("memory model exists")
