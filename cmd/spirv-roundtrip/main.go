package main

import (
	"fmt"
	"io"
	"os"

	"gitlab.com/zephyrtronium/spirv"
)

func main() {
	var inname, outname string
	switch len(os.Args) {
	case 2:
		inname = os.Args[1]
		outname = inname + ".out"
	case 3:
		inname = os.Args[1]
		outname = os.Args[2]
	default:
		fmt.Println("spirv-roundtrip in.spv [out.spv]")
		os.Exit(1)
	}
	var (
		in  io.Reader
		m   *spirv.Module
		out io.WriteCloser
		err error
	)
	steps := []func() error{
		func() error {
			in, err = os.Open(inname)
			return err
		},
		func() error {
			m, err = spirv.Read(in)
			return err
		},
		func() error {
			out, err = os.Create(outname)
			return err
		},
		func() error {
			_, err = m.WriteTo(out)
			return err
		},
		func() error { return out.Close() },
	}
	for _, step := range steps {
		if err := step(); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}
}
